package pages.relime;

import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.*;
import cucumber.api.DataTable;
import helpers.CommonHelper;
import helpers.DBHelper;
import helpers.SystemHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Lazy;

import java.sql.ResultSet;
import java.util.List;
import java.util.Random;

public class DashboardPage extends RelimePage{

    public DashboardPage() {
        TypeFactory.containerInitHTMLElements(this);
    }

    public final String PROJECT_NAME = "./*[2]";
    public final String PROJECT_KEY = "./*[3]";
    public final String EDIT_BUTTON = ".//button[contains(@data-uib-tooltip, 'Edit project')]";
    public final String DELETE_BUTTON = ".//button[contains(@data-uib-tooltip, 'Delete project')]";
    public final String UNSUBSCRIBE_BUTTON = ".//button[contains(@data-uib-tooltip, 'Unsubscribe me')]";

    @Override
    protected WebElement elementForLoading() throws Exception {
        return addNewProjectButton.getWrappedElement();
    }

    @Lazy
    @FindBy(xpath = ".//div[@class='navbar-header']/a")
    public Button topPanelRelimeButton;

    @Lazy
    @FindBy(xpath = ".//*[contains(name(), 'rl-dashboard')]")
    public PlaceHolder dashboardElement;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'greeting-block')]/div/p")
    public PlaceHolder greetingBlockText;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'greeting-block')]//button[text()='Sign in']")
    public PlaceHolder greetingBlockSignInButton;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'greeting-block')]//button[text()='Create domain']")
    public PlaceHolder greetingBlockCreateDomainButton;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'greeting-block')]//a")
    public PlaceHolder greetingBlockCreateAnAccountLink;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'learn-more-block')]//a")
    public PlaceHolder learnMoreLink;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'greeting-block')]//p/a")
    public PlaceHolder profilePageLink;


    @Lazy
    @FindBy(xpath = ".//div[contains(@class,'tab-holder')]/ul/li")
    public List<Label> dashboardSubscribedUserTabs;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class,'no-projects-block-text')]")
    public Label noProjectsExistLabel;

    @Lazy
    @FindBy(xpath = ".//rl-projects-owner//div[contains(@class,'no-projects-block-text')]")
    public Label noProjectsResultsLabel;

    @Lazy
    @FindBy(xpath = ".//div[@class='nav-wrapper']/ul/li[1]/a")
    public Button topPanelMaximizeButton;

    @Lazy
    @FindBy(xpath = ".//div[@class='nav-wrapper']/ul/li[2]/a")
    public Button topPanelDashboardButton;

    @Lazy
    @FindBy(xpath = ".//button[@id='btn-append-to-single-button']")
    public Button topPanelCurrentProjectDropdown;

    @Lazy
    @FindBy(xpath = ".//rl-projects-list-dropdown/span")
    public Button topPanelCurrentProjectLabel;

    @Lazy
    @FindBy(xpath = ".//ul[@class='recent-list']")
    public PlaceHolder recentProjectsDropdown;

    @Lazy
    @FindBy(xpath = ".//nav[@sidebar-anyclick-close]//li/a")
    public List<Link> sidePanelElements;

    @Lazy
    @FindBy(xpath = ".//a[text()='View all projects']")
    public Link viewAllProjectsLink;

    @Lazy
    @FindBy(xpath = ".//*[@data-uib-tooltip='Create new project']")
    public Button addNewProjectButton;

    @Lazy
    @FindBy(xpath = ".//div[@class = 'panel-info']/div//div/ul/li[@class = 'ng-scope']")
    public List<Label> projectTableRows;

    @Lazy
    @FindBy(xpath = ".//label[@for='private']")
    public CheckBox projectTablePrivateCheckbox;

    @Lazy
    @FindBy(xpath = ".//label[@for='public']")
    public CheckBox projectTablePublicCheckbox;

    @Lazy
    @FindBy(xpath = ".//div[@class='two-columns']//div[contains(@class,'column')][1]//div[@class='input-block']//input")
    public TextInput projectTableSearchField;

    @Lazy
    @FindBy(xpath = ".//div[@class='two-columns']//div[contains(@class,'column')][1]//div[@class='input-block']//i")
    public TextInput projectTableSearchIcon;

    @Lazy
    @FindBy(xpath = ".//div[@class='two-columns']//div[contains(@class,'column')][1]//ul[contains(@class, 'pagination')][not(contains(@class, 'ng-hide'))]/li")
    public List<Label> projectTablePages;

    @Lazy
    @FindBy(xpath = ".//div[@class='two-columns']//div[contains(@class,'column')][2]//div[@class='head-line']/*")
    public List<Label> teamTableColumns;

    @Lazy
    @FindBy(xpath = ".//div[@class='two-columns']//div[contains(@class,'column')][2]//uib-accordion//div[contains(@id, 'team-accordion')]")
    public List<Label> teamTableRows;

    @Lazy
    @FindBy(xpath = ".//div[@class='two-columns']//div[contains(@class,'column')][2]//ul[contains(@class, 'pagination')][not(contains(@class, 'ng-hide'))]/li")
    public List<Label> teamTablePages;

    @Lazy
    @FindBy(xpath = ".//div[contains(@id, 'ui-select-choices-row')]")
    public List<PlaceHolder> rolesDropdownElements;

    @Lazy
    @FindBy(xpath = ".//*[contains(name(),'team')]")
    public PlaceHolder teamTable;


    @Lazy
    @FindBy(xpath = ".//div[@class='two-columns']//div[contains(@class,'column')][2]//div[@class='input-block']//input")
    public TextInput teamTableSearchField;

    @Lazy
    @FindBy(xpath = ".//div[@class='two-columns']//div[contains(@class,'column')][2]//div[@class='input-block']//i")
    public TextInput teamTableSearchIcon;


    @Lazy
    @FindBy(xpath = ".//div[not(contains(@class, 'ng-hide'))]/div[@class='no-team-text']/p")
    public Label noSubscribersLabel;

    @Lazy
    @FindBy(xpath = ".//div[@class='no-team-img-holder']/img")
    public Label noSubscribersImage;

    @Lazy
    @FindBy(xpath = ".//div[@class='no-results-found no-team']/a")
    public Label noSubscribersLink;

    @Lazy
    @FindBy(xpath = ".//rl-domain-team//ul[contains(@class, 'pagination')]/li")
    public List<PlaceHolder> subscribersTablePaginationElement;

    @Lazy
    @FindBy(xpath = ".//rl-domain-team//div[contains(@class, 'panel-open')]")
    public List<PlaceHolder> subscribersTableExpandedAccordions;

    @Lazy
    @FindBy(xpath = ".//rl-domain-team//div[@class='table-search']")
    public PlaceHolder subscribersTableSearchBlock;

    @Lazy
    @FindBy(xpath = ".//div[@class = 'panel-info']/div//div/ul/li[@class = 'head-line']/*")
    public List<Label> projectTableColumnLabels;

    public Element findRowInProjectPanelByProjectKey(String value) throws Exception {
        Thread.sleep(500);
        if(this.projectTablePages.size()>0&&this.projectTablePages.get(1).isDisplayed()&&!this.projectTablePages.get(1).getAttribute("class").contains("active")){
            this.projectTablePages.get(1).click();
            Thread.sleep(500);
        }
        if(this.projectTablePages.size()>3) {
            boolean firstPage = true;
            outer:
            while (!this.projectTablePages.get(this.projectTablePages.size() - 2).getAttribute("class").contains("active"))
            {
                String s = this.projectTablePages.get(this.projectTablePages.size() - 2).getAttribute("class");
                for (int i = 0; i < this.projectTablePages.size(); i++) {
                    Element e = this.projectTablePages.get(i);
                    if (e.getAttribute("class").contains("active") && !this.projectTablePages.get(i + 1).getText().equals("&gt;")) {
                        if (!firstPage) {
                            this.projectTablePages.get(i + 1).click();
                            Thread.sleep(500);
                        } else {
                            firstPage = false;
                            i--;
                        }
                        for (Element row : this.projectTableRows) {
                            String key = row.findElement(By.xpath(this.PROJECT_KEY)).getText();
                            if (key.equals(value)) {
                                return row;
                            }
                        }
                        break;
                    } else if (e.getAttribute("class").contains("active") && this.projectTablePages.get(i + 1).getText().equals("&gt;")) {
                        break outer;
                    }
                }
            }
        }
        else{
            for (Element row : this.projectTableRows) {
                String s = row.findElement(By.xpath(this.PROJECT_KEY)).getText();
                if (s.equals(value)) {
                    return row;
                }
            }
        }
        return null;
    }

    public boolean fillProjectNameTextboxOnNewProjectPopUp(String text) throws Exception {
        for (Element e : formFields) {
            if(e.findElement(By.xpath(FormFieldName)).getText().toLowerCase().contains("name")){
                e.findElement(By.xpath(AddProjectFormFieldInput)).click();
                e.findElement(By.xpath(AddProjectFormFieldInput)).clear();
                e.findElement(By.xpath(AddProjectFormFieldInput)).sendKeys(text);
                CommonHelper.testProjectName = text;
                return true;
            }
        }
        return false;
    }

    public boolean fillProjectKeyTextboxOnNewProjectPopUp(String text) throws Exception {
        if(text.equals("random")) {
            java.util.Random r = new Random();
            text = "F" + r.nextInt(999);
            while(text.length()<3)
                text = "F" + r.nextInt(999);
            ResultSet set = DBHelper.executeQuery("SELECT * FROM [RelimeDb].[dbo].[Project] WHERE ProjectKey = '" + text + "';");
            while (set.next()){
                text = "F" + r.nextInt(999);
                set = DBHelper.executeQuery("SELECT * FROM [RelimeDb].[dbo].[Project] WHERE ProjectKey = '" + text + "';");
            }
        }
        if(text.equals("sDefault"))
            text = SystemHelper.DEFAULTSMOKEPROJECT;
        if(text.equals("rDefaultOne"))
            text = SystemHelper.DEFAULTREGRESSIONPROJECTONE;
        if(text.equals("rDefaultTwo"))
            text = SystemHelper.DEFAULTREGRESSIONPROJECTTWO;
        for (Element e : formFields) {
            if(e.findElement(By.xpath(FormFieldName)).getText().toLowerCase().contains("project key")){
                e.findElement(By.xpath(AddProjectFormFieldInput)).click();
                e.findElement(By.xpath(AddProjectFormFieldInput)).clear();
                e.findElement(By.xpath(AddProjectFormFieldInput)).sendKeys(text);
                CommonHelper.testProjectKey = text;
                CommonHelper.createdProjectKeys.add(text);
                return true;
            }
        }
        return false;
    }

    public boolean fillDescriptionTextboxOnNewProjectPopUp(String text) throws Exception {
        for (Element e : formFields) {
            if(e.findElement(By.xpath(FormFieldName)).getText().toLowerCase().contains("description")){
                e.findElement(By.xpath(AddProjectFormFieldInput)).click();
                e.findElement(By.xpath(AddProjectFormFieldInput)).clear();
                e.findElement(By.xpath(AddProjectFormFieldInput)).sendKeys(text);
                CommonHelper.testProjectDescription = (text);
                return true;
            }
        }
        return false;
    }

    public boolean fillProjectTypeTextboxOnNewProjectPopUp(String text) throws Exception {
        for (Element e : formFields) {
            if(e.findElement(By.xpath(FormFieldName)).getText().contains("PROJECT TYPE")){
                e.findElement(By.xpath(AddProjectFormFieldInput)).click();
                for (WebElement li : e.findElements(By.xpath(FormFieldDropdownElements))) {
                    if(li.getText().toLowerCase().equals(text.toLowerCase())) {
                        li.click();
                        CommonHelper.testProjectType = text;
                        return true;
                    }
                }
                break;
            }
        }
        return false;
    }

    public void fillInDataForProject(DataTable table) throws Exception {
        List<List<String>> data = table.raw();
        for (String query : data.get(0)) {
            if(query.startsWith("projectName:")){
                fillProjectNameTextboxOnNewProjectPopUp(query.substring(12));
                ReportService.reportAction("'Project Name' textbox in pop-up 'New project' was filled with data.", true);
            }
            else if(query.startsWith("projectKey:")){
                fillProjectKeyTextboxOnNewProjectPopUp(query.substring(11));
                ReportService.reportAction("'Project Key' textbox in pop-up 'New project' was filled with data.", true);
            }
            else if(query.startsWith("description:")){
                fillDescriptionTextboxOnNewProjectPopUp(query.substring(12));
                ReportService.reportAction("'Description' textbox in pop-up 'New project' was filled with data.", true);
            }
            else if(query.startsWith("projectType:")){
                fillProjectTypeTextboxOnNewProjectPopUp(query.substring(12));
                ReportService.reportAction("'Project type' was selected in pop-up 'New project'.", true);
            }
            else
                ReportService.reportAction("Invalid input. Each cell should begin with one of the following phrases: 'featureName:', 'fileName:', 'description:' or 'tag:'.", false);
        }
    }

    public boolean openPopUpNewProject() throws Exception {
        Waiters.waitAppearanceOf(10, addNewProjectButton.getWrappedElement());
        addNewProjectButton.click();
        return popupIsOpened("New Project");
    }
}