package pages.relime;

import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.*;
import helpers.CommonHelper;
import helpers.SystemHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Lazy;
import pages.base.BasePage;

import java.util.List;
import java.util.regex.Pattern;

public class RelimePage extends BasePage {

    public RelimePage() {
        TypeFactory.containerInitHTMLElements(this);
    }

    @Override
    protected WebElement elementForLoading() throws Exception {
        return loadingElement;
    }

    public final String FormFieldName = "./*[1]";
    public final String AddAccountFormFieldInput = "./div/*[not(@ng-hide)]";
    public final String AddProjectFormFieldInput = "./*[2]";
    public final String FormFieldDropdownElements = ".//li/*";

    @Lazy
    @FindBy(id = "btn-append-to-single-button")
    public WebElement loadingElement;

    @Lazy
    @FindBy(xpath = ".//rl-projects-list-dropdown//a[@ui-sref='app.dashboard']")
    public WebElement viewAllProjectsLink;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'user-block')]/button[@class='btn dropdown-toggle']")
    public WebElement dashboardDropdownButton;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'user-block')]/ul/li/*")
    public List<WebElement> dashboardDropdownElements;

    @Lazy
    @FindBy(xpath = ".//button[contains(@class, 'user-btn')]")
    public WebElement profileDropdownButton;

    @Lazy
    @FindBy(xpath = ".//div[@id='profile-dropdown']//button")
    public List<WebElement> profileDropdownElements;

    @Lazy
    @FindBy(xpath = ".//div[contains(@id, 'ngdialog')]//div[@class='modal-header']/*")
    public Label formHeader;

    @Lazy
    @FindBy(xpath = ".//form//*[contains(@class, 'ask-message')]")
    public Label formMessage;

    @Lazy
    @FindBy(xpath = ".//div[contains(@id, 'ngdialog')]//div[@class='form-group']")
    public List<PlaceHolder> formFields;

    @Lazy
    @FindBy(xpath = ".//div[contains(@id, 'ngdialog')]//div[@class='checkbox c-checkbox']/label")
    public CheckBox formSelectAllCheckbox;

    @Lazy
    @FindBy(xpath = ".//button[contains(@class, 'btn-signin')]")
    public Button signInButton;

    @Lazy
    @FindBy(xpath = ".//md-dialog[contains(@class, 'sign-in-dialog')]")
    public PlaceHolder signInDialog;

    @Lazy
    @FindBy(xpath = ".//md-dialog[contains(@class, 'sign-in-dialog')]//input[@id='email']")
    public TextInput signInDialogEmailField;

    @Lazy
    @FindBy(xpath = ".//md-dialog[contains(@class, 'sign-in-dialog')]//input[@id='password']")
    public TextInput signInDialogPasswordField;

    @Lazy
    @FindBy(xpath = ".//md-dialog[contains(@class, 'sign-in-dialog')]//button[contains(@class, 'sign-in')]")
    public TextInput signInDialogSignInButton;

    @Lazy
    @FindBy(xpath = ".//md-dialog[contains(@aria-label, 'create account')]")
    public PlaceHolder createAnAccountDialog;

    @Lazy
    @FindBy(xpath = ".//md-dialog[contains(@aria-label, 'domain creation')]")
    public PlaceHolder createDomainDialog;

    @Lazy
    @FindBy(xpath = ".//header")
    public TextInput pageHeader;

    @Lazy
    @FindBy(xpath = ".//div[contains(@id, 'ngdialog')]//button")
    public List<Button> FormButtons;

    @Lazy
    @FindBy(xpath = ".//form//*[contains(@class, 'text-danger')]")
    public List<Label> formFieldWarnings;

    @Lazy
    @FindBy(xpath = ".//*[contains(@class, 'text-danger')]")
    public List<Label> EditorFieldWarnings;

    @Lazy
    @FindBy(xpath = "//form//div[@class='form-group'][2]//span[@class = 'text-danger']")
    public Label SecondFieldWarning;

    @Lazy
    @FindBy(xpath = ".//header")
    public Label headerLabel;

    @Lazy
    @FindBy(xpath = ".//div[@class = 'ui-notification ng-scope success']")
    public PlaceHolder successNotificationWindow;

    @Lazy
    @FindBy(xpath = ".//div[@class = 'ui-notification ng-scope error']")
    public PlaceHolder errorNotificationWindow;

    @Lazy
    @FindBy(xpath = ".//div[@class = 'ui-notification ng-scope wait']")
    public PlaceHolder infoNotificationWindow;

    @Lazy
    @FindBy(xpath = ".//div[@class = 'ui-notification ng-scope warning']")
    public PlaceHolder warningNotificationWindow;

    public void clickButton(String button){
        driver.findElement(By.xpath(".//button[text() = '" + button + "']")).click();
    }

    public boolean popupIsOpened(String name) throws Exception {
        Waiters.waitAppearanceOf(15, formHeader.getWrappedElement());
        boolean popupAppeared = formHeader.isDisplayed();
        boolean popupNameMatches = formHeader.getText().toLowerCase().equals(name.toLowerCase());
        return popupAppeared&&popupNameMatches;
    }

    public boolean waitForSuccessNotification(String text) throws Exception{
        Waiters.waitAppearanceOf(60, successNotificationWindow.getWrappedElement());
        Thread.sleep(500);
        if (successNotificationWindow.isDisplayed()) {
            if (text.equals("")) {
                successNotificationWindow.click();
                return true;
            } else {
                String s = successNotificationWindow.getText();
                successNotificationWindow.click();
                return s.equals(text);
            }
        }
        return false;
    }

    public boolean waitForSuccessNotification(Pattern pattern) throws Exception{
        Waiters.waitAppearanceOf(60, successNotificationWindow.getWrappedElement());
        Thread.sleep(500);
        if (successNotificationWindow.isDisplayed()) {
            String s = successNotificationWindow.getText();
            successNotificationWindow.click();
            return pattern.matcher(s).matches();
        }
        return false;
    }

    public boolean waitForErrorNotification(String text) throws Exception{
        Waiters.waitAppearanceOf(60, errorNotificationWindow.getWrappedElement());
        Thread.sleep(500);
        if (errorNotificationWindow.isDisplayed()) {
            if (text.equals("")) {
                errorNotificationWindow.click();
                return true;
            } else {
                String s = errorNotificationWindow.getText();
                errorNotificationWindow.click();
                return s.equals(text);
            }
        }
        return false;
    }

    public void clickButtonOnPopUp(String button) throws Exception {
        Thread.sleep(1000);
        boolean buttonClicked = false;
        for (Button e : FormButtons) {
            String s = e.getText();
            if (s.toLowerCase().equals(button.toLowerCase())) {
                e.click();
                buttonClicked = true;
                break;
            }
        }
        Thread.sleep(1000);
        ReportService.reportAction("\"" + button + "\" button was clicked", buttonClicked);
    }
}