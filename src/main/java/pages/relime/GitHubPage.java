package pages.relime;

import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.*;
import gherkin.lexer.Th;
import helpers.CommonHelper;
import helpers.DBHelper;
import helpers.Node;
import helpers.SystemHelper;
import org.apache.commons.collections.map.HashedMap;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Lazy;
import pages.base.BasePage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by kozlov on 7/5/2016.
 */
public class GitHubPage extends BasePage {


    @Override
    protected WebElement elementForLoading() throws Exception {
        return loadingElement;
    }

    public final String GIT_FILE_LINK = "./td[2]//a";
    public final String GIT_FILE_ICON = ".//td[1]/*[name()='svg']";

    @Lazy
    @FindBy(xpath = ".//a[@class = 'header-logo-invertocat']")
    public WebElement loadingElement;

    @Lazy
    @FindBy(xpath = ".//div[contains(@id, 'ngdialog')]//div[@class='form-group']")
    public List<Label> gitFileLines;

    @Lazy
    @FindBy(xpath = ".//div[contains(@id, 'ngdialog')]//div[@class='form-group'][1]")
    public Label firstGitFileLine;

    @Lazy
    @FindBy(xpath = ".//button[@aria-label = 'Edit this file']")
    public Label gitFileEditButton;

    @Lazy
    @FindBy(xpath = ".//button[@id = 'submit-file']")
    public Label gitFileSubmitButton;

    @Lazy
    @FindBy(xpath = ".//a[text() = 'Sign in']")
    public Button gitSignInButton;

    @Lazy
    @FindBy(xpath = ".//input[@value = 'Sign in']")
    public Button gitLoginPageSignInButton;

    @Lazy
    @FindBy(xpath = ".//input[@id='login_field']")
    public TextInput gitLoginInput;

    @Lazy
    @FindBy(xpath = ".//input[@id='password']")
    public TextInput gitPasswordInput;

    @Lazy
    @FindBy(xpath = ".//td[contains(@id, 'LC')]")
    public List<Label> bitbucketFileContentLines;

    @Lazy
    @FindBy(xpath = ".//pre[@class=' CodeMirror-line ']/span")
    public List<Label> editedBitbucketFileContentLines;

    @Lazy
    @FindBy(xpath = "(.//pre[@class=' CodeMirror-line ']/span)[1]")
    public Label firstBitbucketFileContentLine;

    @Lazy
    @FindBy(xpath = ".//div[contains(@id, 'ngdialog')]//div[@class='form-group']")
    public List<Label> bitbucketFileLines;

    @Lazy
    @FindBy(xpath = ".//div[contains(@id, 'ngdialog')]//div[@class='form-group'][1]")
    public Label firstBitbucketFileLine;

    @Lazy
    @FindBy(xpath = ".//button[@id='file-edit-button']")
    public Label bitbucketFileEditButton;

    @Lazy
    @FindBy(xpath = ".//button[contains(@class, 'save-button')]")
    public Label bitbucketFileSubmitButton;

    @Lazy
    @FindBy(xpath = ".//button[contains(@class, 'commit-button')]")
    public Label bitbucketPopupSubmitButton;

    @Lazy
    @FindBy(xpath = ".//tr[contains(@class, 'up-tree')]//a")
    public Button goUpGitTree;

    @Lazy
    @FindBy(xpath = ".//table/tbody[2]/tr[contains(@class, 'js-navigation-item')]")
    public List<PlaceHolder> gitFiles;

    @Lazy
    @FindBy(xpath = ".//td//span[not(contains(@class, 'browse-up'))]/parent::a")
    public List<PlaceHolder> bitbucketFiles;

    @Lazy
    @FindBy(xpath = ".//span[contains(@class, 'browse-up')]/parent::a")
    public Button goUpBitbucketTree;

    @Lazy
    @FindBy(xpath = ".//input[@value = 'Log in']")
    public Button bitbucketLoginPageSignInButton;

    @Lazy
    @FindBy(xpath = ".//input[@id='js-email-field']")
    public TextInput bitbucketLoginInput;

    @Lazy
    @FindBy(xpath = ".//input[@id='js-password-field']")
    public TextInput bitbucketPasswordInput;

    @Lazy
    @FindBy(xpath = ".//td[contains(@id, 'LC')]")
    public List<Label> gitFileContentLines;

    @Lazy
    @FindBy(xpath = ".//div[@class = 'CodeMirror-code']/div")
    public List<Label> editedGitFileContentLines;

    @Lazy
    @FindBy(xpath = "(.//td[contains(@id, 'LC')])[1]")
    public Label firstGitFileContentLine;

    public boolean uniqueTagsArePresentInProjectFeatures(String url) throws Exception {
        driver.navigate().to(url);
        Pattern p = Pattern.compile("SC_[a-z,A-Z,0-9]+");
        boolean tagsAppeared = true;
        Waiters.waitAppearanceOf(10, this.firstGitFileLine.getWrappedElement());
        for (int i = 1; i < this.gitFileLines.size(); i++) {
            if(this.gitFileLines.get(i).getText().contains("Scenario")){
                if(!p.matcher(this.gitFileLines.get(i-1).getText()).matches()){
                    tagsAppeared = false;
                    break;
                }
            }
        }
        return tagsAppeared;
    }

    public ArrayList<String> readContentsOfTheGitFile(String url) throws Exception {
        driver.navigate().to(url);
        ArrayList<String> lines = new ArrayList<>();
        Waiters.waitAppearanceOf(10, this.firstGitFileContentLine.getWrappedElement());
        for (Element e : this.gitFileContentLines) {
            lines.add(e.getText());
        }
        return lines;
    }

    public ArrayList<String> readContentsOfTheBitbucketFile(String url) throws Exception {
        driver.navigate().to(SystemHelper.DEFAULTGIT + "src");
        Thread.sleep(1000);
        if(bitbucketLoginPageSignInButton.isDisplayed()) {
            this.bitbucketLoginInput.click();
            this.bitbucketLoginInput.sendKeys("vasilpetrov128@gmail.com");
            this.bitbucketPasswordInput.click();
            this.bitbucketPasswordInput.sendKeys("12345678");
            this.bitbucketLoginPageSignInButton.click();
            Thread.sleep(3000);
        }
        Thread.sleep(1000);
        List<String> path = new ArrayList<>();
        url = url.substring(url.indexOf("src/"));
        while (url.contains("/")){
            path.add(url.substring(0, url.indexOf("/")));
            url = url.substring(url.indexOf("/") + 1);
        }
        path.add(url);
        for (String s : path) {
            for (int i = 0; i < bitbucketFiles.size(); i++) {
                Element e = bitbucketFiles.get(i);
                if(e.getText().equals(s)){
                    e.click();
                    break;
                }
            }
            Thread.sleep(1000);
        }
        Thread.sleep(2000);
        this.bitbucketFileEditButton.click();
        Thread.sleep(3000);
        ArrayList<String> lines = new ArrayList<>();
        for (int i = 0; i < this.editedBitbucketFileContentLines.size(); i++) {
            Element e = this.editedBitbucketFileContentLines.get(i);
            lines.add(e.getText());
        }
        return lines;
    }

    public Map<Integer, String> getFeatureIdNameMapFromDatabaseByProjectKey(String Key) throws Exception {
        Map<Integer, String> result = new HashedMap();
        ResultSet set = DBHelper.executeQuery("SELECT [Id] FROM [RelimeDb].[dbo].[Project] WHERE ProjectKey = '" + Key + "'");
        set.next();
        int projectId = set.getInt("Id");
        set = DBHelper.executeQuery("SELECT [Id], [Name] FROM [RelimeDb].[dbo].[Tree] WHERE Project_Id = '" + projectId + "'");
        while(set.next()){
            int id = set.getInt("Id");
            String name = set.getString("Name");
            result.put(id, name);
        }
        return result;
    }

    public Node getFolderStructureFromGit(String url) throws Exception {
        driver.navigate().to(url);
        Waiters.waitAppearanceOf(5, this.goUpGitTree.getWrappedElement());
        Node<String> root = new Node(url.substring(url.lastIndexOf("/") + 1));
        for (int i = 0; i < gitFiles.size(); i++) {
            Element e = gitFiles.get(i);
            Node<String> node = new Node(e.findElement(By.xpath(this.GIT_FILE_LINK)).getText().replace(".feature", ""));
            node.setParent(root);
            WebElement el = e.findElement(By.xpath(this.GIT_FILE_ICON));
            String s = el.getAttribute("class");
            if(e.findElement(By.xpath(this.GIT_FILE_ICON)).getAttribute("class").contains("octicon-file-directory")){
                e.findElement(By.xpath(this.GIT_FILE_LINK)).click();
                Thread.sleep(1000);
                getFolderStructureFromGit(node);
                this.goUpGitTree.click();
                Thread.sleep(1000);
            }
        }
        return root;
    }

    private void getFolderStructureFromGit(Node parent) throws Exception {
        for (int i = 0; i < gitFiles.size(); i++) {
            Element e = gitFiles.get(i);
            Node<String> node = new Node(e.findElement(By.xpath(this.GIT_FILE_LINK)).getText().replace(".feature", ""));
            node.setParent(parent);
            if(e.findElement(By.xpath(this.GIT_FILE_ICON)).getAttribute("class").contains("octicon-file-directory")){
                e.findElement(By.xpath(this.GIT_FILE_LINK)).click();
                Thread.sleep(500);
                getFolderStructureFromGit(node);
                this.goUpGitTree.click();
                Thread.sleep(500);
            }
        }
    }

    public Node getFolderStructureFromBitbucket(String url) throws Exception {
        driver.navigate().to(url);
        Thread.sleep(3000);
        if(bitbucketLoginPageSignInButton.isDisplayed()) {
            this.bitbucketLoginInput.click();
            this.bitbucketLoginInput.sendKeys("vasilpetrov128@gmail.com");
            this.bitbucketPasswordInput.click();
            this.bitbucketPasswordInput.sendKeys("12345678");
            this.bitbucketLoginPageSignInButton.click();
            Thread.sleep(3000);
        }
        for (int i = 0; i < bitbucketFiles.size(); i++) {
            Element e = bitbucketFiles.get(i);
            String s = e.getText();
            if(e.getText().equals("src")){
                e.click();
                break;
            }
        }
        Thread.sleep(1000);
        for (int i = 0; i < bitbucketFiles.size(); i++) {
            Element e = bitbucketFiles.get(i);
            if(e.getText().equals("test")){
                e.click();
                break;
            }
        }
        Thread.sleep(1000);
        for (int i = 0; i < bitbucketFiles.size(); i++) {
            Element e = bitbucketFiles.get(i);
            if(e.getText().equals("resources")){
                e.click();
                break;
            }
        }
        Thread.sleep(1000);
        for (int i = 0; i < bitbucketFiles.size(); i++) {
            Element e = bitbucketFiles.get(i);
            if(e.getText().equals("feature")){
                e.click();
                break;
            }
        }
        Thread.sleep(1000);
        Node<String> root = new Node("feature");
        for (int i = 0; i < bitbucketFiles.size(); i++) {
            Element e = bitbucketFiles.get(i);
            Node<String> node = new Node(e.getText().replace(".feature", ""));
            node.setParent(root);
            String s = e.findElement(By.xpath("./span")).getAttribute("class");
            if(e.findElement(By.xpath("./span")).getAttribute("class").contains("folder")){
                e.click();
                Thread.sleep(1000);
                getFolderStructureFromBitbucket(node);
                this.goUpBitbucketTree.click();
                Thread.sleep(1000);
            }
        }
        return root;
    }

    private void getFolderStructureFromBitbucket(Node parent) throws Exception {
        for (int i = 0; i < bitbucketFiles.size(); i++) {
            Element e = bitbucketFiles.get(i);
            String s = e.getText();
            Node<String> node = new Node(e.getText().replace(".feature", ""));
            node.setParent(parent);
            if(e.findElement(By.xpath("./span")).getAttribute("class").contains("folder")){
                e.click();
                Thread.sleep(1000);
                getFolderStructureFromBitbucket(node);
                this.goUpBitbucketTree.click();
                Thread.sleep(1000);
            }
        }
    }

    public void deleteTagsForGit(String url) throws Exception {
        driver.navigate().to(url);
        Waiters.waitAppearanceOf(10, this.firstGitFileContentLine.getWrappedElement());
        if(this.gitSignInButton.isDisplayed()){
            this.gitSignInButton.click();
            Waiters.waitAppearanceOf(5, this.gitLoginPageSignInButton.getWrappedElement());
            this.gitLoginInput.sendKeys("a.galichenko@nixsolutions.com");
            this.gitPasswordInput.sendKeys("FCMKh1982");
            this.gitLoginPageSignInButton.click();
            Waiters.waitAppearanceOf(10, this.firstGitFileContentLine.getWrappedElement());
        }
        Thread.sleep(500);
        this.gitFileEditButton.click();
        Thread.sleep(1000);
        for (int i = 0; i < this.editedGitFileContentLines.size(); i++) {
            Element e = this.editedGitFileContentLines.get(i);
            if(e.getText().contains("@SC_")||e.getText().contains("@ST_")){
                CommonHelper.clickWithActions(e.getWrappedElement());
                Thread.sleep(500);
                CommonHelper.selectLine(e.getWrappedElement());
                Actions actions = new Actions(driver);
                actions.sendKeys(Keys.BACK_SPACE);
                if(i==0){
                    actions.sendKeys(Keys.DELETE);
                }else
                    actions.sendKeys(Keys.BACK_SPACE);
                actions.build().perform();
                i--;
                Thread.sleep(1000);
            }
        }
        this.gitFileSubmitButton.click();
        Waiters.waitAppearanceOf(5, this.gitFileEditButton.getWrappedElement());
    }

    public void deleteTagsForBitbucket(String url) throws Exception {
        driver.navigate().to(SystemHelper.DEFAULTGIT + "src");
        Thread.sleep(1000);
        if(bitbucketLoginPageSignInButton.isDisplayed()) {
            this.bitbucketLoginInput.click();
            this.bitbucketLoginInput.sendKeys("vasilpetrov128@gmail.com");
            this.bitbucketPasswordInput.click();
            this.bitbucketPasswordInput.sendKeys("12345678");
            this.bitbucketLoginPageSignInButton.click();
            Thread.sleep(3000);
        }
        Thread.sleep(1000);
        List<String> path = new ArrayList<>();
        url = url.substring(url.indexOf("src/"));
        while (url.contains("/")){
            path.add(url.substring(0, url.indexOf("/")));
            url = url.substring(url.indexOf("/") + 1);
        }
        path.add(url);
        for (String s : path) {
            for (int i = 0; i < bitbucketFiles.size(); i++) {
                Element e = bitbucketFiles.get(i);
                if(e.getText().equals(s)){
                    e.click();
                    break;
                }
            }
            Thread.sleep(1000);
        }
        Thread.sleep(2000);
        this.bitbucketFileEditButton.click();
        Thread.sleep(3000);
        for (int i = 0; i < this.editedBitbucketFileContentLines.size(); i++) {
            Element e = this.editedBitbucketFileContentLines.get(i);
            String s = e.getText();
            if(e.isDisplayed()&&e.getText().contains("@SC_")||e.isDisplayed()&&e.getText().contains("@ST_")){
                CommonHelper.scrollToElement(e.getWrappedElement());
                Thread.sleep(500);
                e = new Label(e.findElement(By.xpath(".")));
                CommonHelper.clickWithActions(e.getWrappedElement());
                Thread.sleep(500);
                CommonHelper.selectLine(e.getWrappedElement());
                Actions actions = new Actions(driver);
                actions.sendKeys(Keys.BACK_SPACE);
                if(i==0){
                    actions.sendKeys(Keys.DELETE);
                }else
                    actions.sendKeys(Keys.BACK_SPACE);
                actions.build().perform();
                i--;
                Thread.sleep(1000);
            }
        }
        this.bitbucketFileSubmitButton.click();
        Waiters.waitAppearanceOf(5, this.bitbucketPopupSubmitButton.getWrappedElement());
        this.bitbucketPopupSubmitButton.click();
        Thread.sleep(1000);
    }
}
