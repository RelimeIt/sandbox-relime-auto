package pages.relime;

import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.*;
import cucumber.api.DataTable;
import helpers.CommonHelper;
import helpers.Node;
import helpers.SystemHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Lazy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class EditorPage extends RelimePage {

    public EditorPage() {
        TypeFactory.containerInitHTMLElements(this);
    }

    @Override
    protected WebElement elementForLoading() throws Exception {
        return addNewFolderButton.getWrappedElement();
    }

    public final String TREE_ELEMENT_NAME = "./a/div/span";
    public final String TREE_ELEMENT_SUBELEMENTS = "./ul/li";
    public final String FORM_EXAMPLE_TABLE_ROW_CHECKBOX = "./div/button";
    public final String FORM_EXAMPLE_TABLE_ROW_DATA = "./div[contains(@class, 'example-table-col-')]";

    @Lazy
    @FindBy(xpath = "(.//span[contains(@class, 'tree-node-content')])[1]")
    public Button firstTreeNode;

    @Lazy
    @FindBy(xpath = "(.//h3[contains(text(), 'Scenarios')]/following-sibling::div//div[contains(@class, 'scnearioName')])[1]")
    public Button firstScenario;

    @Lazy
    @FindBy(xpath = ".//div[contains(@id, 'accordion-')][contains(@class, 'panel-open')]")
    public List<PlaceHolder> expandedScenarios;

    @Lazy
    @FindBy(xpath = ".//button[@data-uib-tooltip = 'New folder']")
    public Button addNewFolderButton;

    @Lazy
    @FindBy(xpath = ".//button[@data-uib-tooltip='New feature']")
    public Button addNewFeatureButton;

    @Lazy
    @FindBy(xpath = ".//button[@data-uib-tooltip = 'Update from GIT']")
    public Button updateFromGit;

    @Lazy
    @FindBy(xpath = ".//button[@data-uib-tooltip = 'Update from Jira']")
    public Button updateFromJira;

    @Lazy
    @FindBy(xpath = ".//button[@data-uib-tooltip = 'Export to Git']")
    public Button uploadToGit;

    @Lazy
    @FindBy(xpath = ".//button[@data-uib-tooltip = 'Export to Jira']")
    public Button uploadToJira;

    @Lazy
    @FindBy(xpath = ".//div[@class='control-holder ng-scope']//button[1]")
    public Button cancelTreeButton;

    @Lazy
    @FindBy(xpath = ".//div[@class='control-holder ng-scope']//button[2]")
    public Button selectAllTreeButton;

    @Lazy
    @FindBy(xpath = ".//button[contains(@class, 'ico-checkbox')][not(@disabled)]")
    public List<Button> treeElementCheckboxes;

    @Lazy
    @FindBy(xpath = ".//div[@class='control-holder ng-scope']//button[3]")
    public Button confirmTreeButton;

    @Lazy
    @FindBy(xpath = ".//ul[contains(@class, 'whirl')]")
    public Label treeUpdating;

    @Lazy
    @FindBy(xpath = ".//rl-stories-explorer")
    public Label storiesExplorer;

    @Lazy
    @FindBy(xpath = ".//auto-complete//ul")
    public List<Label> tagAutocompleteElements;

    @Lazy
    @FindBy(xpath = ".//form//ti-tag-item")
    public List<Label> addedTagsOnPopup;

    @Lazy
    @FindBy(xpath = ".//rl-stories-explorer//ti-tag-item")
    public List<Label> featureAddedTags;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'scenario-column')]//ti-tag-item")
    public List<Label> scenarioAddedTags;

    @Lazy
    @FindBy(xpath = ".//button[@data-uib-tooltip='Export to Git']")
    public Button saveChangesToGit;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class,'ui-notification ng-scope info')]")
    public PlaceHolder infoNotification;

    @Lazy
    @FindBy(xpath = ".//div[@class='example-table-container']")
    public PlaceHolder examplesTable;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class,'accordion-block-active')]//div[@class='example-table-container']//div[contains(@class, 'example-table-header')]//div[contains(@class, 'example-table-col-')]")
    public List<PlaceHolder> pageExamplesTableColumns;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class,'accordion-block-active')]//div[@class='example-table-container']//div[contains(@class, 'example-table-content-row')]")
    public List<PlaceHolder> pageExamplesTableRows;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class,'accordion-block-active')]//rl-example-table//h3[text()='example table:']")
    public PlaceHolder pageExamplesTableHeader;

    @Lazy
    @FindBy(xpath = ".//form//rl-example-table//textarea[@id='extdescription']")
    public PlaceHolder formExamplesTableDescription;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class,'accordion-block-active')]//rl-example-table/div/button[contains(@class, 'btn-add')]")
    public PlaceHolder pageAddExampleRowButton;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class,'accordion-block-active')]//rl-example-table/div/button[contains(@class, 'btn-remove')]")
    public PlaceHolder pageDeleteExampleRowButton;

    @Lazy
    @FindBy(xpath = ".//form//div[@class='example-table-container']")
    public PlaceHolder formExamplesTable;

    @Lazy
    @FindBy(xpath = ".//form//div[@class='example-table-container']//div[contains(@class, 'example-table-header')]//div[contains(@class, 'example-table-col-')]")
    public List<PlaceHolder> formExamplesTableColumns;

    @Lazy
    @FindBy(xpath = ".//form//div[@class='example-table-container']//div[contains(@class, 'example-table-content-row')]")
    public List<PlaceHolder> formExamplesTableRows;

    @Lazy
    @FindBy(xpath = ".//form//rl-example-table/div/h3[text()='example table']")
    public PlaceHolder fromExamplesTableHeader;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class,'accordion-block-active')]//rl-example-table//a")
    public PlaceHolder pageExamplesTableDescriptionBeforeFocus;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class,'accordion-block-active')]//rl-example-table//form//textarea")
    public PlaceHolder pageExamplesTableDescriptionAfterFocus;

    @Lazy
    @FindBy(xpath = ".//form//rl-example-table/div/button[contains(@class, 'btn-add')]")
    public PlaceHolder formAddExampleRowButton;

    @Lazy
    @FindBy(xpath = ".//form//rl-example-table/div/button[contains(@class, 'btn-remove')]")
    public PlaceHolder formDeleteExampleRowButton;

    @Lazy
    @FindBy(xpath = ".//button[@data-uib-tooltip='Delete']")
    public Button deleteTreeNodeButton;

    @Lazy
    @FindBy(xpath = ".//button[@data-uib-tooltip='New Scenario']")
    public Button addNewScenarioButton;

    @Lazy
    @FindBy(id = "folder-name")
    public TextInput folderNameInput;

    @Lazy
    @FindBy(id = "name")
    public TextInput nameInput;

    @Lazy
    @FindBy(id = "description")
    public TextInput descriptionInput;

    @Lazy
    @FindBy(xpath = ".//span[@class = 'text-danger ng-binding']")
    public Label inputNotificationMessage;

    @Lazy
    @FindBy(xpath = ".//form//div[contains(@class, 'tags')]/input")
    public TextInput tagsInputOnNewScenarioPopup;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'dialog-holder')]//input[contains(@style, 'width:')][@placeholder='Type and press Enter']")
    public TextInput tagsInputOnNewFeaturePopup;

    @Lazy
    @FindBy(id = "file-name")
    public TextInput fileNameInput;

    @Lazy
    @FindBy(id = "tags-fiels")
    public TextInput tagsInput;

    @Lazy
    @FindBy(xpath = ".//form//div[@id= 'editor_']")
    public TextInput formStepsTextarea;

    @Lazy
    @FindBy(xpath = ".//div[contains(@id,  'editor_')]")
    public TextInput scenarioStepsTextarea;

    @Lazy
    @FindBy(xpath = ".//form//div[contains(@class, 'ace_line_group')]")
    public List<Label> formStepLines;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'ace_line_group')]")
    public List<Label> scenarioStepLines;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'ace_line_group')]")
    public List<Label> tableStepLines;

    @Lazy
    @FindBy(xpath = ".//form//div[contains(@class, 'ace_gutter-cell')]")
    public List<Label> formStepStatusLabels;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'footer-data')]/div")
    public List<PlaceHolder> treeElementFields;

    @Lazy
    @FindBy(xpath = ".//form//*[contains(@class, 'checkbox')]/label")
    public List<PlaceHolder> formCheckboxes;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'ace_gutter-cell')]")
    public List<Label> scenarioStepStatusLabels;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'ace_autocomplete')][not(contains(@style, 'display: none;'))]//div[contains(@class, 'ace_content')]//div[contains(@class, 'ace_line')]")
    public List<Label> formStepAutocompleteLines;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'ace_autocomplete')][not(contains(@style, 'display: none;'))]//div[contains(@class, 'ace_content')]")
    public PlaceHolder formStepAutocomplete;

    @Lazy
    @FindBy(xpath = ".//h4[contains(text(), 'New Feature')]")
    public Label newFeatureLabel;

    @Lazy
    @FindBy(xpath = ".//rl-scenarios-explorer//div[contains(@class, 'panel-heading')]/h1")
    public Label featureNameLabel;

    @Lazy
    @FindBy(id = "nameNewScenario")
    public TextInput newScenarioNameInput;

    @Lazy
    @FindBy(xpath = ".//div[contains(@id, 'accordion-')][contains(@class, 'accordion-block-active')]//*[@id='nameScenario']")
    public Label activeScenarioNameLabel;

    @Lazy
    @FindBy(xpath = ".//div[contains(@id, 'accordion-')][contains(@class, 'accordion-block-active')]")
    public Label activeScenario;

    @Lazy
    @FindBy(xpath = ".//form//rl-jira-autocomplete/input")
    public TextInput jiraKeyNameInput;

    @Lazy
    @FindBy(xpath = ".//form//rl-jira-autocomplete//p")
    public List<Label> jiraKeyAutocompleteLines;

    @Lazy
    @FindBy(xpath = ".//h3[text()='feature']/parent::*/parent::*//a[contains(@class, 'editable-click')]")
    public TextInput changeFeatureNameBeforeFocusInput;

    @Lazy
    @FindBy(xpath = ".//h3[text()='file name']/parent::*/parent::*//a[contains(@class, 'editable-click')]")
    public TextInput changeFileNameBeforeFocusInput;

    @Lazy
    @FindBy(xpath = ".//h3[text()='jira key']/parent::*/parent::*//a")
    public TextInput jiraKeyLink;

    @Lazy
    @FindBy(xpath = ".//h3[text()='description']/parent::*/parent::*//a[contains(@class, 'editable-click')]")
    public TextInput changeDescriptionBeforeFocusInput;

    @Lazy
    @FindBy(xpath = ".//h3[contains(text(), 'Scenarios')]/following-sibling::div//div[contains(@class, 'scnearioName')]")
    public List<Link> scenariosList;

    @Lazy
    @FindBy(xpath = ".//span[contains(@class, 'counter')][text()='BG']/parent::div//div[contains(@class, 'scnearioName')]")
    public Link backgroundElement;

    @Lazy
    @FindBy(xpath = ".//div[contains(@id, 'accordion-')][contains(@class, 'background-accordion')][contains(@class, 'panel-open')]")
    public Link backgroundAccordion;

    @Lazy
    @FindBy(xpath = ".//span[contains(text(), 'BG')]/parent::*/parent::*/parent::*//a[contains(@class, 'description-project-content')]")
    public Link backgroundDescription;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'accordion-block-active')]//a[contains(text(), 'Delete')]")
    public Button actionDeleteInScenarioBlockButton;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'editable-controls')]/span/button[1]")
    public Button saveFeatureEditedFieldButton;

    @Lazy
    @FindBy(xpath = ".//form[contains(@class, 'editable-text')]/*[1]/span/button[2]")
    public Button cancelEditedFieldButton;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'accordion-block-active')]//i[@data-uib-tooltip='Edit scenario title']")
    public Link changeScenarioNameBeforeFocusButton;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'accordion-block-active')]//input[contains(@class, 'editable-input')]")
    public TextInput changeScenarioNameInputAfterFocus;

    @Lazy
    //@FindBy(xpath = ".//div[contains(@class, 'description-project-content')]")
    @FindBy(xpath = ".//div[contains(@class, 'accordion-block-active')]//a[contains(@class, 'description-project-content')]")
    public Link changeScenarioDescriptionInputBeforeFocus;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'accordion-block-active')]//button[contains(@class, 'btn-success')]")
    public TextInput submitChangeScenarioDescriptionButton;

    @Lazy
    @FindBy(xpath = ".//textarea[contains(@class, 'editable-input')]")
    public TextInput changeScenarioDescriptionInputAfterFocus;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'accordion-block-active')]//input[contains(@ng-model, 'newTag.text')]")
    public TextInput scenarioTagsInput;

    @Lazy
    @FindBy(xpath = ".//rl-stories-explorer//tags-input[@name='tags']//input")
    public TextInput featureTagsInput;

    @Lazy
    @FindBy(xpath = ".//button[@data-uib-tooltip='Save scenarios']")
    public Button saveScenariosButton;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'dialog-holder')]//label[contains(@class, 'checkbox')]")
    public TextInput backgroundOnNewScenarioPopupCheckbox;

    @Lazy
    //@FindBy(xpath = ".//input[@id='jiralinked']")
    @FindBy(id = "jiralinked")
    public TextInput jiraLinkedFeatureCheckbox;

    @Lazy
    @FindBy(xpath = ".//li[contains(@class, 'active')]//span[contains(@class, 'tree-node-content')]")
    public Label currentNodeLabel;

    @Lazy
    @FindBy(xpath = ".//div[@class = 'footer-data']")
    public PlaceHolder featureDataPanel;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'angular-ui-tree')]/div[3]/ul/li")
    public PlaceHolder topTreeFolder;

    @Lazy
    @FindBy(xpath = ".//rl-scenarios-explorer//div[contains(@class, 'btn-holder')]")
    public PlaceHolder scenariosListControlElements;

    @Lazy
    @FindBy(xpath = ".//ul[contains(@class, 'pagination')]/li")
    public List<Label> scenariosListPages;

    @Lazy
    @FindBy(xpath = ".//li[contains(@class, 'angular-ui-tree-node')]/a")
    public List<Label> treeElements;

    @Lazy
    @FindBy(xpath = ".//li[contains(@class, 'angular-ui-tree-node')][contains(@class, 'active')]/a")
    public Label activeTreeElement;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'accordion-block-active')]//button[contains(text(), 'Actions')]")
    public Button actionsButton;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'accordion-block-active')]//div[contains(@class, 'dropdown open')]//a")
    public List<Button> actionOptions;

    @Lazy
    @FindBy(xpath = ".//ul[contains(@class, 'pagination')]")
    public Label scenariosListPaginationElement;

    @Lazy
    @FindBy(xpath = ".//uib-accordion//h3[contains(@class, 'title')]")
    public Label scenariosLabel;

    @Lazy
    @FindBy(xpath = ".//div[@class='column-holder'][1]//a")
    public Link changeFolderNameInputBeforeFocus;

    @Lazy
    @FindBy(xpath = ".//input[@type='text'][contains(@class, 'editable-input')]")
    public TextInput changeFieldAfterFocusInput;

    @Lazy
    @FindBy(xpath = ".//form[contains(@class, 'editable-textarea')]//textarea")
    public TextInput changeFeatureDescriptionAfterFocusInput;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'dialog-holder')]//input[contains(@ng-checked, 'allSelected')]")
    public CheckBox selectAllFeaturesCheckbox;



    //Parameter: "feature" / "file name" / "description"
    public String getChangeFeatureInputBeforeFocusText(String inputName){
        return driver.findElement(By.xpath(".//h3[text()='" + inputName + "']/parent::*/parent::*//a[contains(@class, 'editable-click')]")).getText();
    }

    public boolean treeElementExists(String folderName){
        try {
            Thread.sleep(500);
            driver.findElement(By.xpath(".//span[text() = '" + folderName + "']"));
            return true;
        }
        catch(Exception ex) {
            return false;
        }
    }

    public boolean scenarioExists(String scenarioName) throws Exception{
        Thread.sleep(500);
        for (Link l : scenariosList) {
            if (l.getText().equals(scenarioName))
                return true;
        }
        return false;
    }

    public void selectScenarioByName(String scenarioName) throws Exception {
        if(scenarioName == "")
            scenarioName = "(no name)";
        for (Link l : scenariosList) {
            if(l.getText().equals(scenarioName))
                l.click();
        }
    }

    public void selectFolderByName(String treeElementName) throws Exception {

        Waiters.waitAppearanceOf(CommonHelper.delay, firstTreeNode.getWrappedElement());
        for (Label l : treeElements) {
            if(l.getText().startsWith(treeElementName))
                l.click();
        }
        CommonHelper.testFolderName = treeElementName;
    }

    public void selectFeatureByName(String treeElementName) throws Exception {
        Waiters.waitAppearanceOf(CommonHelper.delay, firstTreeNode.getWrappedElement());
        for (Label l : treeElements) {
            if(l.getText().startsWith(treeElementName)) {
                l.click();
                break;
            }
        }
        CommonHelper.testFeatureFileName = treeElementName;
    }

    public TextInput getInputFieldForExampleOnForm(String column, int row) throws Exception {
        row--;
        for (int i = 0; i < this.formExamplesTableColumns.size(); i++) {
            Element e = this.formExamplesTableColumns.get(i);
            if(e.getText().equals(column)){
                i++;
                return new TextInput(formExamplesTableRows.get(row).findElement(By.xpath(FORM_EXAMPLE_TABLE_ROW_DATA + "[" + i + "]")));
            }
        }
        return null;
    }

    public TextInput getInputFieldForExampleOnPage(String column, int row) throws Exception {
        row--;
        for (int i = 0; i < this.pageExamplesTableColumns.size(); i++) {
            Element e = this.pageExamplesTableColumns.get(i);
            if(e.getText().equals(column)){
                i++;
                return new TextInput(pageExamplesTableRows.get(row).findElement(By.xpath(FORM_EXAMPLE_TABLE_ROW_DATA + "[" + i + "]")));
            }
        }
        return null;
    }

    public boolean exampleTableHeaderIsPresentOnForm(String name) throws Exception {
        boolean present = false;
        for (Element e : this.formExamplesTableColumns) {
            if(e.getText().equals(name)){
                present = true;
                break;
            }
        }
        return present;
    }

    public boolean exampleTableHeaderIsPresentOnPage(String name) throws Exception {
        boolean present = false;
        for (Element e : this.pageExamplesTableColumns) {
            if(e.getText().equals(name)){
                present = true;
                break;
            }
        }
        return present;
    }

    public boolean verifyIfTreeElementIsSelected(String treeNodeName) throws Exception {
        Waiters.waitAppearanceOf(5, activeTreeElement.getWrappedElement());
        if(activeTreeElement.isDisplayed()&&activeTreeElement.getText().equals(treeNodeName))
            return true;
        return false;
    }

    public void changeScenarioName(String scenarioName) throws Exception {
        Waiters.waitAppearanceOf(10, changeScenarioNameBeforeFocusButton.getWrappedElement());
        changeScenarioNameBeforeFocusButton.click();
        Waiters.waitAppearanceOf(5, changeScenarioNameInputAfterFocus.getWrappedElement());
        changeScenarioNameInputAfterFocus.clear();
        changeScenarioNameInputAfterFocus.sendKeys(scenarioName);
        submitChangeScenarioDescriptionButton.sendKeys(Keys.ENTER);
        CommonHelper.testScenarioName = scenarioName;
    }

    public void changeScenarioDescription(String scenarioDescription) throws Exception {
        if(changeScenarioDescriptionInputBeforeFocus.isDisplayed()) {
            Waiters.waitAppearanceOf(CommonHelper.delay, changeScenarioDescriptionInputBeforeFocus.getWrappedElement());
            changeScenarioDescriptionInputBeforeFocus.click();
            Waiters.waitAppearanceOf(CommonHelper.delay, changeScenarioDescriptionInputAfterFocus.getWrappedElement());
        }
        changeScenarioDescriptionInputAfterFocus.clear();
        changeScenarioDescriptionInputAfterFocus.sendKeys(scenarioDescription);
    }

    public void addToScenarioDescription(String scenarioDescription) throws Exception {
        if(!changeScenarioDescriptionInputAfterFocus.isDisplayed()) {
            Waiters.waitAppearanceOf(CommonHelper.delay, changeScenarioDescriptionInputBeforeFocus.getWrappedElement());
            changeScenarioDescriptionInputBeforeFocus.click();
            Waiters.waitAppearanceOf(CommonHelper.delay, changeScenarioDescriptionInputAfterFocus.getWrappedElement());
        }
        changeScenarioDescriptionInputAfterFocus.sendKeys(scenarioDescription);
    }

    public void changeScenarioTags(String scenarioTags) throws Exception {
        Waiters.waitAppearanceOf(CommonHelper.delay, scenarioTagsInput.getWrappedElement());
        scenarioTagsInput.clear();
        scenarioTagsInput.sendKeys(scenarioTags);
    }

    //Parameter: "Save" / "Rollback" / "Show History" / "Delete" / "Execute"
    public void clickActionsMenuOption(String option) throws Exception {
        SystemHelper.waitAppearanceByElement(driver, 100, 30, actionsButton.getWrappedElement());
        actionsButton.click();
        Thread.sleep(1000);
        for (Button b : actionOptions) {
            if(b.getText().equals(option)) {
                b.click();
                break;
            }
        }
    }

    //Parameter: "Save" / "Rollback" / "Show History" / "Delete" / "Execute"
    public boolean actionsMenuOptionIsNotEnabled(String option) throws Exception {
        SystemHelper.waitAppearanceByElement(driver, 100, 30, actionsButton.getWrappedElement());
        actionsButton.click();
        Thread.sleep(1000);
        for (Button b : actionOptions) {
            if(b.getText().equals(option)&&!b.findElement(By.xpath("./parent::li")).getAttribute("class").contains("disabled")) {
                return false;
            }
        }
        actionsButton.click();
        Thread.sleep(1000);
        return true;
    }

    public boolean actionsMenuOptionIsEnabled(String option) throws Exception {
        SystemHelper.waitAppearanceByElement(driver, 100, 30, actionsButton.getWrappedElement());
        actionsButton.click();
        Thread.sleep(1000);
        for (Button b : actionOptions) {
            if(b.getText().equals(option)&&!b.findElement(By.xpath("/parent::li")).getAttribute("class").contains("disabled")) {
                return true;
            }
        }
        actionsButton.click();
        Thread.sleep(1000);
        return false;
    }

    public boolean verifyIfScenarioAccordionIsExpanded() throws Exception {
        try {
            Thread.sleep(1000);
            Waiters.waitAppearanceOf(10, activeScenario.getWrappedElement());
            if(activeScenario.isDisplayed())
                return true;
        }
        catch(Exception ex) {
            return false;
        }
        return false;
    }

    public boolean verifyIfBackgroundAccordionIsExpanded() throws Exception {
        try {
            Thread.sleep(1000);
            Waiters.waitAppearanceOf(10, backgroundAccordion.getWrappedElement());
            if(backgroundAccordion.isDisplayed())
                return true;
        }
        catch(Exception ex) {
            return false;
        }
        return false;
    }

    public boolean verifyIfExampleTableIsDisplayed() throws Exception {
        try {
            Waiters.waitAppearanceOf(5, examplesTable.getWrappedElement());
            if(examplesTable.isDisplayed())
                return true;
        }
        catch(Exception ex) {
            return false;
        }
        return false;
    }

    public boolean verifyIfBackgroundIsDisplayedInSeparatedControl() throws Exception {
        Waiters.waitAppearanceOf(5, backgroundElement.getWrappedElement());
        try {
            if(backgroundElement.isDisplayed())
                return true;
        }
        catch(Exception ex) {
            return false;
        }
        return false;
    }

    public boolean verifyScenarioName() throws Exception {
        Waiters.waitAppearanceOf(5, activeScenarioNameLabel.getWrappedElement());
        try{
            if(CommonHelper.testScenarioName.equals(""))
                CommonHelper.testScenarioName = "No scenario name";

            if (activeScenarioNameLabel.getText().equals(CommonHelper.testScenarioName))
                return true;
            else
                return false;
        }
        catch(Exception ex){
            return false;
        }
    }

    public boolean verifyScenarioDescription() throws Exception {
        Waiters.waitAppearanceOf(5, changeScenarioDescriptionInputBeforeFocus.getWrappedElement());
        try{
            if(CommonHelper.testScenarioDescription.equals(""))
                CommonHelper.testScenarioDescription = "No scenario description";
            if (changeScenarioDescriptionInputBeforeFocus.getText().equals(CommonHelper.testScenarioDescription))
                return true;
            else
                return false;
        }
        catch(Exception ex){
            return false;
        }
    }

    public boolean verifyScenarioTags() throws Exception {
        Waiters.waitAppearanceOf(5, scenarioTagsInput.getWrappedElement());
        try{
            boolean tagsArePresent = true;
            for (String s : CommonHelper.testScenarioTags) {
                boolean tagPresent = false;
                for (Element e : scenarioAddedTags) {
                    String tag = e.getText().substring(0, e.getText().indexOf("\n"));
                    if(e.getText().substring(0, e.getText().indexOf("\n")).equals(s)){
                        tagPresent = true;
                        break;
                    }
                }
                if(!tagPresent){
                    tagsArePresent = false;
                    break;
                }
            }
            if (tagsArePresent){
                return true;
            }
            return false;
        }
        catch(Exception ex){
            return false;
        }
    }

    public boolean verifyBackgroundName() throws Exception {
        Waiters.waitAppearanceOf(5, this.backgroundElement.getWrappedElement());
        try{
            if (this.backgroundElement.getText().equals(CommonHelper.testScenarioName))
                return true;
            else
                return false;
        }
        catch(Exception ex){
            return false;
        }
    }

    public boolean verifyBackgroundDescription() throws Exception {
        Waiters.waitAppearanceOf(5, this.backgroundDescription.getWrappedElement());
        try{
            if (this.backgroundDescription.getText().equals(CommonHelper.testScenarioDescription))
                return true;
            else
                return false;
        }
        catch(Exception ex){
            return false;
        }
    }

    public boolean verifyFeatureTags() throws Exception {
        Waiters.waitAppearanceOf(5, featureTagsInput.getWrappedElement());
        try{
            boolean tagsArePresent = true;
            for (String s : CommonHelper.testFeatureTags) {
                boolean tagPresent = false;
                for (Element e : featureAddedTags) {
                    if(e.getText().substring(0, e.getText().indexOf("\n")).equals(s)){
                        tagPresent = true;
                        break;
                    }
                }
                if(!tagPresent){
                    tagsArePresent = false;
                    break;
                }
            }
            if (tagsArePresent){
                return true;
            }
            return false;
        }
        catch(Exception ex){
            return false;
        }
    }

    public void checkJiraLinkedFeatureCheckbox() throws Exception {
        SystemHelper.waitAppearanceByElement(driver, 100, 30, jiraLinkedFeatureCheckbox.getWrappedElement());
        Thread.sleep(500);
        //jiraLinkedFeatureCheckbox.sendKeys(Keys.SPACE);
        //jiraLinkedFeatureCheckbox.click();
        CommonHelper.clickWithJavascript(jiraLinkedFeatureCheckbox.getWrappedElement());
    }

    public boolean verifyCreateJiraLinkedFeaturePopup() throws Exception {
        for (int i=0; i<30; i++) {
            try {
                if (fileNameInput.getAttribute("value").startsWith("REL-")) {
                    return true;
                }
            }
            catch (Exception e) {}
            finally {
                Thread.sleep(100);
            }
        }
        return false;
    }

    public Node getFolderStructureFromEditPage() throws Exception {
        Waiters.waitAppearanceOf(5, topTreeFolder.getWrappedElement());
        Node<String> root = new Node(topTreeFolder.findElement(By.xpath(TREE_ELEMENT_NAME)).getText());
        try{
            for (int i = 0; i < topTreeFolder.findElements(By.xpath(TREE_ELEMENT_SUBELEMENTS)).size(); i++) {
                WebElement e = topTreeFolder.findElements(By.xpath(TREE_ELEMENT_SUBELEMENTS)).get(i);
                if(e.isDisplayed()) {
                    Node<String> node = new Node(e.findElement(By.xpath(this.TREE_ELEMENT_NAME)).getText());
                    node.setParent(root);
                    try {
                        getFolderStructureFromInnerFolders(node, e);
                    } catch (Exception ex) {
                    }
                }
            }
            return root;
        }
        catch (Exception e){
            return root;
        }

    }

    private void getFolderStructureFromInnerFolders(Node parent, WebElement element) throws Exception {
        try {
            for (int i = 0; i < element.findElements(By.xpath(TREE_ELEMENT_SUBELEMENTS)).size(); i++) {
                WebElement e = element.findElements(By.xpath(TREE_ELEMENT_SUBELEMENTS)).get(i);
                Node<String> node = new Node(e.findElement(By.xpath(this.TREE_ELEMENT_NAME)).getText());
                node.setParent(parent);
                try {
                    getFolderStructureFromInnerFolders(node, e);
                } catch (Exception ex){}
            }
        } catch (Exception ex){}
    }

    public boolean verifyIfBackgroundHasLabelBG() throws Exception {
        Waiters.waitAppearanceOf(5, backgroundElement.getWrappedElement());
        try {
            if(backgroundElement.isDisplayed())
                return true;
        }
        catch(Exception ex) {
            return false;
        }
        return false;
    }

    public void fillFeatureTextboxOnNewFeaturePopUp(String text) throws Exception {
        SystemHelper.waitAppearanceByElement(driver, 30, 100, nameInput.getWrappedElement());
        nameInput.enterText(text);
    }

    public void fillFileNameTextboxOnNewFeaturePopUp(String text) throws Exception {
        SystemHelper.waitAppearanceByElement(driver, 30, 100, fileNameInput.getWrappedElement());
        fileNameInput.enterText(text);
        CommonHelper.testFeatureName = text;
    }

    public void fillDescriptionTextboxOnPopUp(String text) throws Exception {
        SystemHelper.waitAppearanceByElement(driver, 30, 100, descriptionInput.getWrappedElement());
        descriptionInput.enterText(text);
    }

    public void fillScenarioNameTextboxOnNewFeaturePopUp(String text) throws Exception {
        SystemHelper.waitAppearanceByElement(driver, 30, 100, newScenarioNameInput.getWrappedElement());
        newScenarioNameInput.enterText(text);
    }

    public void addTagOnNewFeaturePopUp(String text) throws Exception {
        SystemHelper.waitAppearanceByElement(driver, 30, 100, tagsInputOnNewFeaturePopup.getWrappedElement());
        tagsInputOnNewFeaturePopup.click();
        tagsInputOnNewFeaturePopup.clear();
        tagsInputOnNewFeaturePopup.sendKeys(text);
        Thread.sleep(500);
        CommonHelper.sendKeyWithActions(Keys.ENTER);
    }

    public void addTagOnNewScenarioPopUp(String text) throws Exception {
        SystemHelper.waitAppearanceByElement(driver, 30, 100, tagsInputOnNewScenarioPopup.getWrappedElement());
        tagsInputOnNewScenarioPopup.click();
        tagsInputOnNewScenarioPopup.clear();
        tagsInputOnNewScenarioPopup.sendKeys(text);
        Thread.sleep(500);
        CommonHelper.sendKeyWithActions(Keys.ENTER);
    }

    public boolean openPopUpNewFeature() throws Exception {
        Waiters.waitAppearanceOf(CommonHelper.delay, addNewFeatureButton.getWrappedElement());
        addNewFeatureButton.click();
        ReportService.reportAction("Button 'New feature' in panel 'Features' was clicked.", true);
        return popupIsOpened("New Feature");
    }

    public boolean openPopUpNewFolder() throws Exception {
        Waiters.waitAppearanceOf(CommonHelper.delay, addNewFolderButton.getWrappedElement());
        addNewFolderButton.click();
        ReportService.reportAction("Button 'New folder' in panel 'Features' was clicked.", true);
        return popupIsOpened("New folder");
    }

    public boolean openPopUpNewScenario() throws Exception {
        Waiters.waitAppearanceOf(15, addNewScenarioButton.getWrappedElement());
        addNewScenarioButton.click();
        return popupIsOpened("New scenario");
    }

    public void fillInDataForNewFeaturePopUp(DataTable table) throws Exception {
        List<List<String>> data = table.raw();
        for (String query : data.get(0)) {
            if(query.startsWith("featureName:")){
                fillFeatureTextboxOnNewFeaturePopUp(query.substring(12));
                ReportService.reportAction("'Feature' textbox in pop-up 'New feature' was filled with data.", true);
            }
            else if(query.startsWith("fileName:")){
                fillFileNameTextboxOnNewFeaturePopUp(query.substring(9));
                ReportService.reportAction("'Feature name' textbox in pop-up 'New feature' was filled with data.", true);
            }
            else if(query.startsWith("description:")){
                fillDescriptionTextboxOnPopUp(query.substring(12));
                ReportService.reportAction("'Description' textbox in pop-up 'New feature' was filled with data.", true);
            }
            else if(query.startsWith("tag:")){
                addTagOnNewFeaturePopUp(query.substring(4));
                ReportService.reportAction("Tag was added in pop-up 'New feature'.", true);
            }
            else
                ReportService.reportAction("Invalid input. Each cell should begin with one of the following phrases: 'featureName:', 'fileName:', 'description:' or 'tag:'.", false);

        }
        ReportService.reportAction("'Feature' textbox in pop-up 'New feature' was filled with data.", true);
    }
    public void fillInDataForNewScenarioPopUp(DataTable table) throws Exception {
        List<List<String>> data = table.raw();
        CommonHelper.testScenarioTags = new ArrayList<>();
        for (String query : data.get(0)) {
            if(query.startsWith("scenarioName:")){
                String name = query.substring(13);
                if(name.contains("[random]")){
                    Random r = new Random();
                    name = name.replace("[random]", String.valueOf(r.nextInt()));
                }
                fillScenarioNameTextboxOnNewFeaturePopUp(name);
                CommonHelper.testScenarioName = name;
                ReportService.reportAction("'Name' textbox in pop-up 'New feature' was filled with data.", true);
            }
            else if(query.startsWith("description:")){
                fillDescriptionTextboxOnPopUp(query.substring(12));
                CommonHelper.testScenarioDescription = query.substring(12);
                ReportService.reportAction("'Description' textbox in pop-up 'New feature' was filled with data.", true);
            }
            else if(query.startsWith("tag:")){
                addTagOnNewScenarioPopUp(query.substring(4));
                CommonHelper.testScenarioTags.add(query.substring(4));
                ReportService.reportAction("Tag was added in pop-up 'New feature'.", true);
            }
            else
                ReportService.reportAction("Invalid input. Each cell should begin with one of the following phrases: 'featureName:', 'fileName:', 'description:' or 'tag:'.", false);
        }
        if(data.size()>1) {
            formStepsTextarea.click();
            for (int i = 1; i < data.size(); i++) {
                String line = data.get(i).get(0);
                if(line.equals("comment")){
                    line = "\"\"\" comment";
                }
                TextInput t = new TextInput(formStepsTextarea.findElement(By.xpath("./textarea")));
                for (char c : line.toCharArray()) {
                    String s = String.valueOf(c);
                    t.sendKeys(s);
                }
                t.sendKeys("\n");
                Thread.sleep(1000);
            }
        }
        ReportService.reportAction("'Feature' textbox in pop-up 'New feature' was filled with data.", true);
    }

    public void addTagViaEnterForScenario(String tag) throws Exception {
        scenarioTagsInput.clear();
        scenarioTagsInput.sendKeys(tag);
        CommonHelper.sendKeyWithActions(Keys.ENTER);
    }

    public void addTagViaCommaForScenario(String tag) throws Exception {
        scenarioTagsInput.clear();
        scenarioTagsInput.sendKeys(tag + ",");
    }

    public void addTagViaEnterForFeature(String tag) throws Exception {
        featureTagsInput.clear();
        featureTagsInput.sendKeys(tag);
        CommonHelper.sendKeyWithActions(Keys.ENTER);
    }

    public void addTagViaCommaForFeature(String tag) throws Exception {
        featureTagsInput.clear();
        featureTagsInput.sendKeys(tag + ",");
    }

    public void addTagViaEnterOnPopUp(String tag) throws Exception {
        tagsInputOnNewScenarioPopup.clear();
        tagsInputOnNewScenarioPopup.sendKeys(tag);
        CommonHelper.sendKeyWithActions(Keys.ENTER);
    }

    public void addTagViaCommaOnPopUp(String tag) throws Exception {
        tagsInputOnNewScenarioPopup.clear();
        tagsInputOnNewScenarioPopup.sendKeys(tag + ",");
    }
}
