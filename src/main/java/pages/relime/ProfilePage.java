package pages.relime;

import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Lazy;

import java.util.List;

/**
 * Created by kozlov on 5/24/2016.
 */
public class ProfilePage extends RelimePage {

    public final String CREDENTIALS_ROW_ACCOUNT = ".//dt/span[1]";
    public final String CREDENTIALS_ROW_NAME = ".//dd/span";
    public final String CREDENTIALS_ROW_DELETE = "./button";
    public final String CREDENTIALS_FORM_UPDATE = ".//button[1]";
    public final String CREDENTIALS_FORM_CANCEL = ".//button[2]";
    public final String CREDENTIALS_FORM_ACCOUNT_FIELD = ".//input[contains(@id, 'AccountName')]";
    public final String CREDENTIALS_FORM_NAME_FIELD = ".//input[contains(@id, 'UserName')]";
    public final String CREDENTIALS_FORM_PASSWORD_FIELD = ".//input[contains(@id, 'Password')]";

    public ProfilePage() {
        TypeFactory.containerInitHTMLElements(this);
    }

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'user-content')]")
    public PlaceHolder profileUserInfo;

    @Lazy
    @FindBy(xpath = ".//span[contains(@class, 'user-email')]")
    public PlaceHolder profileUserEmail;

    @Override
    protected WebElement elementForLoading() throws Exception {
        return loadingElement;
    }

    @Lazy
    @FindBy(xpath = ".//md-dialog//h2")
    public Label dialogHeader;

    @Lazy
    @FindBy(xpath = ".//md-dialog//div[@class='error-block']/span")
    public Label dialogErrorField;

    @Lazy
    @FindBy(xpath = ".//md-dialog//button[contains(@class, 'btn-close')]")
    public Label closeDialogButton;

    @Lazy
    @FindBy(xpath = ".//button[@aria-label = 'Update photo']")
    public Button updatePhotoButton;

    @Lazy
    @FindBy(xpath = ".//md-dialog//button[@class='btn btn-update']")
    public Button dialogUpdateButton;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'user-data')]//img")
    public Button accountIcon;

    @Lazy
    @FindBy(xpath = ".//span[contains(@class, 'btn-load')]")
    public Button selectPhotoButton;

    @Lazy
    @FindBy(xpath = ".//md-content//span[contains(@class, 'input-content')][not(contains(@class, 'ng-hide'))]")
    public Button selectedPhotoNameLabel;

    @Lazy
    @FindBy(xpath = ".//*[name()='g']//*[name()='text'][contains(@class, 'discrete-bar-value')]")
    public List<Label> tariffPlanOptions;

    @Lazy
    @FindBy(xpath = ".//*[name()='g']/*[name() = 'polygon']/following-sibling::*[name()='text'][contains(@class, 'discrete-bar-value')]")
    public Label currentTariffPlan;

    @Lazy
    @FindBy(xpath = ".//*[name()='ld-tariff-plan']//button[contains(@class, 'btn-update')]")
    public Button updateTariffPlanButton;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'tariff-pending')]//a")
    public Button cancelTariffPlanRequestButton;

    @Lazy
    @FindBy(xpath = ".//dl[contains(@class, 'tariff-content')]/dd/time")
    public Label validTariffTermLabel;

    @Lazy
    @FindBy(xpath = ".//ld-domains//button[contains(@class, \"btn-create\")]")
    public Button createDomainButton;

    @Lazy
    @FindBy(xpath = ".//md-dialog//input[@id='domain-name']")
    public TextInput domainNameDialogField;

    @Lazy
    @FindBy(xpath = ".//div[contains(@id, 'domain-type-')][contains(@class, 'dropdown-show')]//li//button")
    public List<Label> domainTypeDropdownOptions;

    @Lazy
    @FindBy(xpath = ".//md-dialog/md-dialog-actions/button")
    public List<Button> dialogButtons;

    @Lazy
    @FindBy(xpath = ".//div[@class = 'domain-block']/div[contains(@class, 'all-domains-section')]/ul/li")
    public List<Button> domainsList;

    @Lazy
    @FindBy(xpath = ".//ld-credentials//div[contains(@class, 'accordion-heading')]")
    public List<Label> credentialsTableRows;

    @Lazy
    @FindBy(xpath = ".//ul[contains(@class, 'pagination')]/li")
    public List<WebElement> accountTablePages;

    @Lazy
    @FindBy(xpath = "(.//div[@id='credentials-section']//button[contains(@class, 'btn-create')])[1]")
    public Button createNewCredentialButton;

    @Lazy
    @FindBy(xpath = ".//rl-credentials-form[contains(@class, 'create-form-holder')]")
    public PlaceHolder createNewCredentialForm;

    @Lazy
    @FindBy(xpath = ".//rl-credentials-form[not(contains(@class, 'create-form-holder'))]")
    public PlaceHolder editCredentialForm;

    @Lazy
    @FindBy(xpath = ".//div[contains(@class, 'error-block')]/span")
    public List<Label> profileFieldWarnings;

    public Label findRowInAccountPanel(String account, String userName) throws Exception {
        for (Label p : credentialsTableRows) {
            if(p.findElement(By.xpath(CREDENTIALS_ROW_ACCOUNT)).getText().equals(account)
                    &&p.findElement(By.xpath(CREDENTIALS_ROW_NAME)).getText().equals(userName)){
                return p;
            }
        }
        return null;
    }

    /*public Label findRowInAccountPanel(String account, String userName) throws Exception {
        Waiters.waitAppearanceOf(5, this.credentialsTableRows.get(0).getWrappedElement());
        if(this.accountTablePages!=null&& this.accountTablePages.size()>3) {
            boolean firstPage = true;
            this.accountTablePages.get(1).click();
            outer:
            while(true) {
                for (int i = 0; i < this.accountTablePages.size(); i++) {
                    WebElement e = this.accountTablePages.get(i);
                    String s1 = e.getText();
                    String s2 = e.getAttribute("class");
                    if (e.getAttribute("class").contains("active") && !this.accountTablePages.get(i + 1).getText().equals(">")) {
                        if(!firstPage) {
                            this.accountTablePages.get(i + 1).click();
                        }
                        else{
                            firstPage = false;
                        }
                        Thread.sleep(500);
                        for (Label row : this.credentialsTableRows) {
                            String s = row.findElement(By.xpath(this.ACCOUNT)).getText();
                            boolean accountCorrect = s.equals(account);
                            s = row.findElement(By.xpath(this.NAME)).getText();
                            boolean nameCorrect = s.equals(userName);
                            if (accountCorrect && nameCorrect) {
                                return row;
                            }
                        }
                    }
                    else if(e.getAttribute("class").contains("active") && this.accountTablePages.get(i + 1).getText().equals(">")){
                        break outer;
                    }
                }
            }
        }
        else{
            for (Label row : this.credentialsTableRows) {
                String s = row.findElement(By.xpath(this.ACCOUNT)).getText();
                boolean accountCorrect = s.equals(account);
                s = row.findElement(By.xpath(this.NAME)).getText();
                boolean nameCorrect = s.equals(userName);
                if (accountCorrect && nameCorrect) {
                    return row;
                }
            }
        }
        return null;
    }*/
}
