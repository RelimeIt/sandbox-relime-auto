package pages.relime;

import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.*;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Lazy;

import java.util.List;

/**
 * Created by kozlov on 9/30/2016.
 */
public class AdminPage extends RelimePage{

    public AdminPage() {
        TypeFactory.containerInitHTMLElements(this);
    }

    public final String INVOICE_EMAIL_FIELD = "./span[2]";
    public final String INVOICE_TARIFF_FIELD = "./span[5]";
    public final String INVOICE_CONFIRM_BUTTON = "./input[@type='button']";
    public final String INVOICE_SET_TO_DEFAULT = ".//input[@type='checkbox']";

    @Lazy
    @FindBy(xpath = ".//div[@class='navbar-header']")
    public Label adminPageHeader;

    @Lazy
    @FindBy(xpath = ".//input[@id='Email']")
    public TextInput adminLoginField;

    @Lazy
    @FindBy(xpath = ".//input[@id='Password']")
    public TextInput adminPasswordField;

    @Lazy
    @FindBy(xpath = ".//input[@value='Log in']")
    public Button adminLogInButton;

    @Lazy
    @FindBy(xpath = ".//div[@id='invoicesToHandleGrid']//li[@class='grid-data-row']")
    public List<PlaceHolder> newInvoicesRows;

    @Lazy
    @FindBy(xpath = ".//div[@class='custom-table-header']")
    public List<PlaceHolder> newInvoicesHeaderRow;

    @Lazy
    @FindBy(xpath = ".//div[@id='invoicesInTheSystemGrid']//li[@class='grid-data-row']")
    public List<PlaceHolder> allInvoicesRows;

    @Lazy
    @FindBy(xpath = ".//div[@id='invoicesInTheSystemGrid']")
    public PlaceHolder allInvoicesTable;
}
