package helpers;

import javax.mail.*;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by kozlov on 11/8/2016.
 */
public class MailHelper {

    public String getLastConfirmationCode(String username, String password) throws MessagingException, IOException {
        String host = "pop.gmail.com";

        Properties props = new Properties();
        props.put("mail.pop3s.host", host);
        props.put("mail.pop3s.port", "995");
        props.put("mail.pop3s.starttls.enable", "true");
        Session emailSession = Session.getDefaultInstance(props);

        Store store = emailSession.getStore("pop3s");

        store.connect(host, username, password);
        Folder inbox = store.getFolder("INBOX");
        inbox.open(Folder.READ_WRITE);
        Message[] messages = inbox.getMessages();
        for (int i = messages.length - 1; i>=0; i--) {
            Message m = messages[i];
            if(m.getSubject().equals("Relime registration confirmation code.")){
                Multipart mp = (Multipart) m.getContent();
                BodyPart bp = mp.getBodyPart(0);
                String s = bp.getContent().toString();
                s = s.substring(s.indexOf("Confirmation Code:"));
                s = s.substring(s.indexOf("<b>") + 3);
                s = s.substring(0, s.indexOf("</b>"));
                s.toLowerCase();
                store.close();
                return s;
            }
        }
        store.close();
        return null;
    }

    public String getLastInvitationLinkURL(String username, String password) throws MessagingException, IOException {
        String host = "pop.gmail.com";

        Properties props = new Properties();
        props.put("mail.pop3s.host", host);
        props.put("mail.pop3s.port", "995");
        props.put("mail.pop3s.starttls.enable", "true");
        Session emailSession = Session.getDefaultInstance(props);

        Store store = emailSession.getStore("pop3s");

        store.connect(host, username, password);
        Folder inbox = store.getFolder("INBOX");
        inbox.open(Folder.READ_WRITE);
        Message[] messages = inbox.getMessages();
        for (int i = messages.length - 1; i>=0; i--) {
            Message m = messages[i];
            if(m.getSubject().equals("Invitation to the project")){
                Multipart mp = (Multipart) m.getContent();
                BodyPart bp = mp.getBodyPart(0);
                String s = bp.getContent().toString();
                s = s.substring(s.indexOf("href=\"http://"));
                s = s.substring(s.indexOf("http://"));
                s = s.substring(0, s.indexOf("\""));
                s.toLowerCase();
                store.close();
                return s;
            }
        }
        store.close();
        return null;
    }
}
