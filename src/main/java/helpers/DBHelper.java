package helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

/**
 * Created by kozlov on 12/6/2016.
 */
public class DBHelper {

    private static Connection connection;

    private static void checkConnection() throws Exception {
        if(connection==null){
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:jtds:sqlserver://" + SystemHelper.DATABASEIP + ";databaseName=RelimeDb;user=" + SystemHelper.DATABASELOGIN + ";password=" + SystemHelper.DATABASEPASS + ";");
        }
        else if(connection.isClosed()) {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:jtds:sqlserver://" + SystemHelper.DATABASEIP + ";databaseName=RelimeDb;user=" + SystemHelper.DATABASELOGIN + ";password=" + SystemHelper.DATABASEPASS + ";");
        }
    }

    public static boolean executeStatement(String statement) throws Exception {
        checkConnection();
        return connection.createStatement().execute(statement);
    }

    public static ResultSet executeQuery(String query) throws Exception {
        checkConnection();
        return connection.createStatement().executeQuery(query);
    }
}
