package helpers.TableVerifiers.TeamVerifiers;

import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import cucumber.api.java.gl.E;
import helpers.TableVerifiers.TableVerifierInterface;
import org.openqa.selenium.By;

/**
 * Created by kozlov on 11/8/2016.
 */
public class TeamEmailVerifier implements TableVerifierInterface {

    String email;

    public TeamEmailVerifier(String email){
        this.email = email;
    }

    @Override
    public boolean verify(Element element) throws Exception {
        boolean verified = true;
        element.click();
        Thread.sleep(1000);
        String name = element.findElement(By.xpath(".//span[contains(@class, 'user-email')]")).getText();
        if (!name.toLowerCase().contains(email.toLowerCase())) {
            verified = false;
        }
        try{
            String highlight = element.findElement(By.xpath(".//span[contains(@class, 'user-email')]")).findElement(By.xpath("span")).getText();
            if (!highlight.toLowerCase().contains(email.toLowerCase())) {
                verified = false;
            }
        }catch (Exception ex){
            verified = false;
        }
        return verified;
    }
}
