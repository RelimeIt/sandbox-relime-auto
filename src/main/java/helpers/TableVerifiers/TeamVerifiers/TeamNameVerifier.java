package helpers.TableVerifiers.TeamVerifiers;

import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import helpers.TableVerifiers.TableVerifierInterface;
import org.openqa.selenium.By;

/**
 * Created by kozlov on 11/8/2016.
 */
public class TeamNameVerifier implements TableVerifierInterface {

    String name;

    public TeamNameVerifier(String name){
        this.name = name;
    }

    @Override
    public boolean verify(Element element) throws Exception {
        boolean verified = true;
        String actualName = element.findElement(By.xpath(".//div[@class='accordion-name']/span[1]")).getText();
        String project = element.findElement(By.xpath(".//span[contains(@class, 'project-name')]")).getText();
        if (!actualName.toLowerCase().contains(name.toLowerCase())&&!project.toLowerCase().contains(name.toLowerCase())) {
            verified = false;
        }
        try{
            String highlight = element.findElement(By.xpath(".//div[@class='accordion-name']/span[1]")).findElement(By.xpath("span")).getText();
            if (!highlight.toLowerCase().contains(name.toLowerCase())) {
                verified = false;
            }
        }catch (Exception ex){
            String projectHighlight = element.findElement(By.xpath(".//span[contains(@class, 'project-name')]")).findElement(By.xpath("span")).getText();
            if (!projectHighlight.toLowerCase().contains(name.toLowerCase())) {
                verified = false;
            }
        }
        return verified;
    }
}
