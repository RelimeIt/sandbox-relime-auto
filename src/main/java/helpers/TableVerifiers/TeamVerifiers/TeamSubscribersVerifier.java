package helpers.TableVerifiers.TeamVerifiers;

import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import helpers.TableVerifiers.TableVerifierInterface;
import org.openqa.selenium.By;

import java.util.List;

/**
 * Created by kozlov on 11/8/2016.
 * Verifies that only users that are subscribed to current user's projects are displayed.
 */
public class TeamSubscribersVerifier implements TableVerifierInterface {

    List<String> names;

    public TeamSubscribersVerifier(List<String> names){
        this.names = names;
    }

    @Override
    public boolean verify(Element element) throws Exception {
        boolean verified = true;
        String name = element.findElement(By.xpath(".//div[@class='accordion-name']/span[1]")).getText();
        if (!names.contains(name)) {
            verified = false;
        }
        return verified;
    }
}
