package helpers.TableVerifiers.TeamVerifiers;

import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import helpers.TableVerifiers.TableVerifierInterface;
import org.openqa.selenium.By;

/**
 * Created by kozlov on 11/8/2016.
 */
public class TeamProjectVerifier implements TableVerifierInterface {

    String name;

    public TeamProjectVerifier(String name){
        this.name = name;
    }

    @Override
    public boolean verify(Element element) throws Exception {
        boolean verified = true;
        String actualName = element.findElement(By.xpath(".//span[contains(@class, 'project-name')]")).getText();
        if (!actualName.toLowerCase().contains(name.toLowerCase())) {
            verified = false;
        }
        try{
            String highlight = element.findElement(By.xpath(".//span[contains(@class, 'project-name')]")).findElement(By.xpath("span")).getText();
            if (!highlight.toLowerCase().contains(name.toLowerCase())) {
                verified = false;
            }
        }catch (Exception ex){
            verified = false;
        }
        return verified;
    }
}
