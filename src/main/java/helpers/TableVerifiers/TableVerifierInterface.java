package helpers.TableVerifiers;

import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;

/**
 * Created by kozlov on 11/8/2016.
 */
public interface TableVerifierInterface {
    boolean verify(Element element) throws Exception;
}
