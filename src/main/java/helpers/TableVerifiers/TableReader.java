package helpers.TableVerifiers;

import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import org.openqa.selenium.By;
import pages.relime.DashboardPage;

/**
 * Created by kozlov on 11/8/2016.
 */
public class TableReader {

    public boolean checkProjectsTable(TableVerifierInterface verifier, DashboardPage page) throws Exception {
        boolean projectsAreCorrect = true;
        boolean firstPage = true;
        Thread.sleep(2000);
        if (page.projectTablePages != null && page.projectTablePages.size() > 3) {
            outer:
            while (!page.projectTablePages.get(page.projectTablePages.size() - 2).getAttribute("class").contains("active")) {
                for (int i = 0; i < page.projectTablePages.size(); i++) {
                    Element e = page.projectTablePages.get(i);
                    if (e.getAttribute("class").contains("active") && !page.projectTablePages.get(i + 1).getText().equals("&gt;")) {
                        if (!firstPage) {
                            page.projectTablePages.get(i + 1).click();
                            Thread.sleep(1000);
                        } else {
                            firstPage = false;
                        }
                        for (Element row : page.projectTableRows) {
                            if(!verifier.verify(row)){
                                projectsAreCorrect = false;
                                break outer;
                            }
                        }
                        //break;
                    } else if (e.getAttribute("class").contains("active") && page.projectTablePages.get(i + 1).getText().equals("&gt;")) {
                        break outer;
                    }
                }
            }
        }
        else {
            for (Element row : page.projectTableRows) {
                if(!verifier.verify(row)){
                    projectsAreCorrect = false;
                    break;
                }
            }
        }
        return projectsAreCorrect;
    }

    public boolean checkTeamTable(TableVerifierInterface verifier, DashboardPage page) throws Exception {
        boolean teamIsCorrect = true;
        boolean firstPage = true;
        Thread.sleep(2000);
        if (page.teamTablePages != null && page.teamTablePages.size() > 3) {
            outer:
            while (!page.teamTablePages.get(page.teamTablePages.size() - 2).getAttribute("class").contains("active")) {
                for (int i = 0; i < page.teamTablePages.size(); i++) {
                    Element e = page.teamTablePages.get(i);
                    if (e.getAttribute("class").contains("active") && !page.teamTablePages.get(i + 1).getText().equals("&gt;")) {
                        if (!firstPage) {
                            page.teamTablePages.get(i + 1).click();
                            Thread.sleep(1000);
                        } else {
                            firstPage = false;
                        }
                        for (Element row : page.teamTableRows) {
                            if(!verifier.verify(row)){
                                teamIsCorrect = false;
                                break outer;
                            }
                        }
                        //break;
                    } else if (e.getAttribute("class").contains("active") && page.teamTablePages.get(i + 1).getText().equals("&gt;")) {
                        break outer;
                    }
                }
            }
        }
        else {
            for (Element row : page.teamTableRows) {
                String name = row.findElement(By.xpath(".//span[contains(@class, 'project-name')]")).getText();
                if(!verifier.verify(row)){
                    teamIsCorrect = false;
                    break;
                }
            }
        }
        return teamIsCorrect;
    }
}
