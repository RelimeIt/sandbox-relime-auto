package helpers.TableVerifiers.ProjectsVerifiers;

import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import helpers.TableVerifiers.TableVerifierInterface;
import org.openqa.selenium.By;
import pages.relime.DashboardPage;

import java.util.List;

/**
 * Created by kozlov on 11/8/2016.
 */
public class ProjectsPresentVerifier implements TableVerifierInterface{

    DashboardPage dashboardPage;

    List<String> names;

    public ProjectsPresentVerifier(List<String> names, DashboardPage page){
        this.names = names;
        this.dashboardPage = page;
    }

    @Override
    public boolean verify(Element element) throws Exception {
        boolean verified = true;
        String key = element.findElement(By.xpath(dashboardPage.PROJECT_KEY)).getText();
        if(!names.contains(key)){
            verified = false;
        }
        return verified;
    }
}
