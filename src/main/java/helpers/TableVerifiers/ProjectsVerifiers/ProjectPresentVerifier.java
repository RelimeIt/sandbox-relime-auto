package helpers.TableVerifiers.ProjectsVerifiers;

import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import helpers.TableVerifiers.TableVerifierInterface;
import org.openqa.selenium.By;
import pages.relime.DashboardPage;

/**
 * Created by kozlov on 11/8/2016.
 */
public class ProjectPresentVerifier implements TableVerifierInterface {

    DashboardPage dashboardPage;

    String name;

    public ProjectPresentVerifier(String name, DashboardPage page){
        this.name = name;
        this.dashboardPage = page;
    }

    @Override
    public boolean verify(Element element) throws Exception {
        boolean verified = true;
        String actualName = element.findElement(By.xpath(dashboardPage.PROJECT_NAME)).getText();
        if (!actualName.contains(name)) {
            verified = false;
        }
        try{
            String highlight = element.findElement(By.xpath(dashboardPage.PROJECT_NAME)).findElement(By.xpath("span")).getText();
            if (!highlight.contains(name)) {
                verified = false;
            }
        }catch (Exception ex){
            verified = false;
        }
        return verified;
    }
}
