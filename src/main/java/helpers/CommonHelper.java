package helpers;

import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import pages.base.PageInstance;

import java.awt.Robot;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class CommonHelper extends PageInstance {

    public static String testFolderName;
    public static String testFeatureName;
    public static String testFeatureFileName;
    public static ArrayList<String> testFeatureTags;
    public static String testJiraLinkedFeatureName;
    public static String testJiraLinkedFeatureFilename;
    public static String testScenarioName;
    public static String testScenarioDescription;
    public static ArrayList<String> testScenarioTags;
    public static ArrayList<String> createdProjectKeys = new ArrayList<>();
    public static String testBackgroundName;
    public static String testBackgroundDescription;
    public static String testProjectName;
    public static String testProjectKey;
    public static String testProjectDescription;
    public static String testProjectType;
    public static String testAccountName = "";
    public static String testAccountUserName = "";
    public static String testAccountPassword;
    public static int delay = 5;
    public static Map<Integer, String> DBFeaturesTreeElements;
    public static Node gitHubFileTree;
    public static Node editorPageFileTree;
    public static String savedURL;
    public static String currentDomain;
    public static String publicProject;
    public static String privateProject;
    public static boolean firstRun = true;

    public static void dragAndDropWithActions(WebElement fromElement, WebElement toElement) throws Exception{
        Actions builder = new Actions(driver);
        Action dragAndDrop = builder.clickAndHold(fromElement)
                .moveToElement(toElement)
                .release(toElement)
                .build();
        dragAndDrop.perform();
    }

    public static void sendKeysWithActions(WebElement element, String text) throws Exception{
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
    }

    public static void sendKeyWithActions(WebElement element, CharSequence key) throws Exception{
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(key);
        actions.build().perform();
    }

    public static void sendKeyWithActions(CharSequence key) throws Exception{
        Actions actions = new Actions(driver);
        actions.sendKeys(key);
        actions.build().perform();
    }

    public static void scrollToElement(WebElement element) throws Exception{
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.build().perform();
    }

    public static void clickWithActions(WebElement element) throws Exception{
        element.getLocation();
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.click(element);
        actions.build().perform();
    }

    public static void doubleClickWithActions(WebElement element) throws Exception {
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.doubleClick();
        actions.build().perform();
    }

    public static void selectLine(WebElement element) throws Exception {
        Actions actions = new Actions(driver);
        actions.sendKeys(Keys.HOME);
        actions.keyDown(Keys.LEFT_SHIFT);
        actions.sendKeys(Keys.END);
        actions.keyUp(Keys.LEFT_SHIFT);
        actions.build().perform();
    }

    public static void
    sendKeyWithAWT(int key) throws Exception {
        Robot r = new Robot();
        r.keyPress(key);
        r.keyRelease(key);
    }

    public static void clickWithJavascript(WebElement element) throws Exception {
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", element);
    }

    public static boolean navigateToPage(String URL, Element pageElement) throws Exception {
        Thread.sleep(1000);
        if (!pageElement.isDisplayed()) {
            driver.navigate().to(URL);
            ngDriver.waitForAngularRequestsToFinish();
            Thread.sleep(1000);
            ngDriver.waitForAngularRequestsToFinish();
            Thread.sleep(1000);
            ngDriver.waitForAngularRequestsToFinish();
            Thread.sleep(5000);
            Waiters.waitAppearanceOf(15, pageElement.getWrappedElement());
            return true;
        } else
            return true;
    }

    public static boolean selectElementByText(List<? extends Element> elements, String text) throws Exception {
        for (Element tab : elements) {
            if(tab.getText().toLowerCase().equals(text.toLowerCase())){
                tab.click();
                return true;
            }
        }
        return false;
    }

    public static String replaceUserWithActualEmail(String user) throws Exception {
        switch (user){
            case "DO":
                user = SystemHelper.DOUSERNAME;
                break;
            case "TM":
                user = SystemHelper.TMUSERNAME;
                break;
            case "PM":
                user = SystemHelper.PMUSERNAME;
                break;
            case "unsubscribed":
                user = "domainuser981@gmail.com";
                break;
            default:
                ReportService.reportAction("Wrong user field. Available options: DO, PM, TM, unsubscribed.", false);
        }
        return user;
    }

    public static String replaceProjectWithActualProjectKey(String project) throws Exception {
        switch (project){
            case "public":
                project = CommonHelper.publicProject;
                break;
            case "private":
                project = CommonHelper.privateProject;
                break;
            case "rDefaultOne":
                project = SystemHelper.DEFAULTREGRESSIONPROJECTONE;
                break;
            case "WRONG":
                break;
            default:
                ReportService.reportAction("Wrong project field. Available options: public, private, rDefaultOne.", false);

        }
        return project;
    }

    public static boolean elementWithTextIsPresent(List<? extends Element> elements, String text) throws Exception {
        for (Element tab : elements) {
            if(tab.getText().toLowerCase().equals(text.toLowerCase())){
                return true;
            }
        }
        return false;
    }

    public static String getDate(){
        return new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date());
    }
}
