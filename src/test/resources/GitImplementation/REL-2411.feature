@ST_1754
Feature: As a DO I want to see  if my account for Git is not valid

Background: 

Given I am signed in as "DO"
And I already uploaded data from GIT from Public repository

@SC_6802
Scenario: Check validation message on 'Settings' page if  linked account for GIT is invalid
When I navigate to page "Profile"
And I change valid credentials for GIT to invalid
And I navigate to page "[Settings]" for project "[string]"
Then I see "[Repository is not accessible with these credentials]" warning under VCS account field

@SC_6803
Scenario: Check validation message on 'Settings' page if try link invalid account for GIT
When I navigate to page "Profile"
And I change valid credentials for GIT to invalid
And I navigate to page "[Settings]" for project "[string]"
And I change drop-down 'Saving mode' value to "[setting]" in page 'Project settings'
And I select account "[string]"
Then I see "[Repository is not accessible with these credentials]" warning under VCS account field

@SC_6804
Scenario: Check notification for 'Editor' page if linked account for GIT is invalid for 'requiring confirmation' save mode
When I navigate to page "Profile"
And I change valid credentials for GIT to invalid
And I navigate to page "[Settings]" for project "[string]"
And I see that drop-down 'Saving mode' value is "[requiring confirmation]" in page 'Project settings'
Then I navigate to page "[Editor]" for project "[string]"
And I see error notification "[You should link your valid Git account to perform changes in the feature."
And I select feature "[string]" in the tree
And I can change and save scenarios

@SC_6805
Scenario: Check notification for 'Editor' page if linked account for GIT is invalid for 'direct' save mode
When I navigate to page "Profile"
And I change valid credentials for GIT to invalid
And I navigate to page "[Settings]" for project "[string]"
And I see that drop-down 'Saving mode' value is "[direct]" in page 'Project settings'
Then I navigate to page "[Editor]" for project "[string]"
And I see error notification "[You should link your valid Git account to perform changes in the feature."
And I select feature "[string]" in the tree
And I see error notification "[You should link your valid Git account to perform changes in the feature."
And I see scenarios in read only mode