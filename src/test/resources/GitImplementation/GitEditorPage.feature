@ST_1752 @GitEditorPage
Feature: Git editor page

Background: 

Given I am signed in as "DO"
When I navigate to page "Dashboard"
And I check that VCS "default" is absent in DB
And I read folder structure from GIT "default"
And I navigate to page "Dashboard"
#And I check that project "rDefaultOne" is absent in DB
When I create project in page 'Dashboard'
|projectName:project_name1|projectKey:random|description:test|projectType:private|
And I see that project is added to panel 'Projects'
When I select project in panel 'Projects' in page "Dashboard"
And I navigate to page "Project Settings"
And I change drop-down 'Saving mode' value to "requiring confirmation" in page 'Project settings'
When I change Git path to "default"
Then I see that Git "default" is added in page 'Project settings'
When I selected menu "default" in dropdown 'Account' at block VCS
Then I see success notification ""

@SC_6791 @REL-779
Scenario: Load project from Git
When I update project from Git on Settings page
When I navigate to page "Editor"
And I read folder structure from Editor page
Then I check that GitHub and ReLime trees are the same

@SC_6792 @REL-780
Scenario: Reload project from Git
When I update project from Git on Settings page
When I read initial IDs from database
And I navigate to page "Editor"
And I update all features from Git on Editor page
And I can see IDs is changed in ng-ispector
When I read folder structure from Editor page
Then I check that GitHub and ReLime trees are the same

@SC_6793 @REL-788
Scenario: Load and update project on Editor page
When I update project from Git on Settings page
And I navigate to page "Editor"
And I save changes to Git
Then I see success notification ""

@SC_6794 @REL-775
Scenario: Check that selected story is updated
When I update project from Git on Settings page
When I navigate to page "Editor"
And I select feature "search_book" in the tree
And I select "Search books by publication year" scenario in the table
Then I see scenario "Search books by publication year" is expanded
When I change textbox 'Name' value to "scenarioName1" in scenario accordion
And I change textbox 'Description' value to "scenarioDescription1" in scenario accordion
And I Save edited field
And I type "tag2" into tags input textbox in scenario accordion
And I push 'Enter'
And I type "And one more step" into string "3" in Scenario Editor in scenario accordion
And I save changes using drop-down 'Actions' in scenario accordion
Then I see that the scenario is collapsed
And I update all features from Git on Editor page
When I select feature "search_book" in the tree
And I select "Search books by publication year" scenario in the table
Then I see scenario "Search books by publication year" is expanded
And I can see that structures in scenario accordion for scenario "Search books by publication year" and Git file "[default]blob/master/src/test/resources/feature/salary/search_book.feature" are the same

@SC_6795 @REL-786
Scenario: Reload project without unique tags from Git via Editor page
When I delete tags for scenarios and feature in Git file "[default]blob/master/src/test/resources/feature/salary/salary_management.feature"
And I navigate to page "Project Settings"
And I update project from Git on Settings page
And I see that tags are present for scenarios and feature in Git file "[default]blob/master/src/test/resources/feature/salary/salary_management.feature"