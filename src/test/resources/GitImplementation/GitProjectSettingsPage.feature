@ST_1753 @GitProjectSettingsPage
Feature: Project settings page

Background: 

Given I am signed in as "DO"
When I navigate to page "Dashboard"
And I check that VCS "default" is absent in DB
When I create project in page 'Dashboard'
|projectName:project_name1|projectKey:random|description:test|projectType:private|
And I see that project is added to panel 'Projects'
When I select project in panel 'Projects' in page "Dashboard"
And I navigate to page "Project Settings"
And I change drop-down 'Saving mode' value to "requiring confirmation" in page 'Project settings'

@SC_6797 @REL-773
Scenario: Check that button 'Load/Reload from GIT' is absent
And I can see that block 'Version control system' is empty
And I can't see button 'Load/Reload project from GIT' in block "Version control system'

@SC_6798 @REL-768
Scenario: Check 'Load project from GIT' button for the first time
When I change Git path to "default"
Then I see that Git "default" is added in page 'Project settings'
And I see success notification ""
When I selected menu "default" in dropdown 'Account' at block VCS
Then I see success notification ""
And I can see button 'Load/Reload project from GIT' in block "Version control system'
And I can see tooltip "Load project from Git" on button 'Load/Reload project from GIT' in block "Version control system'
When I load from Git on page 'Settings'
Then I see info notification "Loading the project from Git repository"
And I see success notification "The project has been successfully updated from Git repository"
And I see that tags are present for scenarios and feature in Git file "[default]blob/master/src/test/resources/feature/salary/salary_management.feature"
When I navigate to page "Editor"
Then I can see project was uploaded

@SC_6799 @REL-769
Scenario: Check 'Load project from GIT' button for the non-first time
When I change Git path to "default"
Then I see that Git "default" is added in page 'Project settings'
And I see success notification ""
When I selected menu "default" in dropdown 'Account' at block VCS
Then I see success notification ""
And I can see button 'Load/Reload project from GIT' in block "Version control system'
And I can see tooltip "Load project from Git" on button 'Load/Reload project from GIT' in block "Version control system'
When I Load from Git on page 'Settings'
Then I see info notification "Loading the project from Git repository"
And I see success notification "The project has been successfully updated from Git repository"
And I can see button 'Load/Reload project from GIT' in block "Version control system'
And I can see tooltip "Reload project from Git" on button 'Load/Reload project from GIT' in block "Version control system'
When I Load from Git on page 'Settings'
Then I see info notification "Reloading the project from Git repository"
And I see success notification "The project has been successfully updated from Git repository"
And Unique tags appear in Git repository "[default]blob/master/src/test/resources/feature/book/search_book.feature"
And I navigate to page "Editor"
And I can see project was uploaded

@SC_6800 @REL-771
Scenario: Check notification when project was uploaded with errors
When I change Git path to "default"
And I see success notification ""
Then I see that Git "default" is added in page 'Project settings'
When I selected menu "wrong" in dropdown 'Account' at block VCS
Then I can't see button 'Load/Reload project from GIT' in block "Version control system'
And I see "Repository is not accessible with these credentials" warning under VCS account field