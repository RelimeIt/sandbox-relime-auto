@ST_1762 @RemoveProject
Feature: Remove project

@SC_6834 @REL-610
Scenario: Check ability to delete project from Dashboard page
Given I am signed in as "DO"
And I navigate to page "Dashboard"
When I create project in page 'Dashboard'
|projectName:project_name2|projectKey:random|description:test|projectType:private|
#And I check that project "rDefaultOne" is absent in DB
And I see that project is added to panel 'Projects'
When I delete project in panel 'Projects' in page "Dashboard"
Then I see success notification ""
And I see that the project was successfully deleted