@ST_1764 @ProjectSettings
Feature: Project settings

Background: 

Given I am signed in as "DO"
When I navigate to page "Dashboard"
And I create project in page 'Dashboard'
And I see that project is added to panel 'Projects'
|projectName:project_name2|projectKey:random|description:test|projectType:private|

@SC_6838 @REL-1601
Scenario: Project Description display
When I select project in panel 'Projects' in page "Dashboard"
Then I see that textbox 'Description' is "No project description" in page 'Project settings'
When I change textbox 'Description' value to "test1" in page 'Project settings'
And I push 'Enter'
And I add text "test2" to textbox 'Description' in page 'Project settings'
And I save changes
Then I see that textbox 'Description' is "test1\ntest2" in page 'Project settings'
When I change textbox 'Description' value to "test1" in page 'Project settings'
And I cancel changes
Then I see that textbox 'Description' is "test1\ntest2" in page 'Project settings'

@SC_6839 @REL-618
Scenario: Check that project name can be edited with existed project name
When I create project in page 'Dashboard'
|projectName:project_name3|projectKey:random|description:test|projectType:private|
And I see that project is added to panel 'Projects'
When I select project in panel 'Projects' in page "Dashboard"
And I change textbox 'Name' value to "project_name2" in page 'Project settings'
Then I see success notification ""

@SC_6840 @REL-611
Scenario: Check ability to edit project from Dashboard page
When I select project in panel 'Projects' in page "Dashboard"
And I change textbox 'Name' value to "project_name12" in page 'Project settings'
Then I see success notification ""
And I see that textbox 'Name' is "project_name12" in page 'Project settings'
When I change textbox 'Description' value to "test1" in page 'Project settings'
And I save changes
Then I see success notification ""
And I see that textbox 'Description' is "test1" in page 'Project settings'
When I change drop-down 'Project type' value to "public" in page 'Project settings'
Then I see success notification ""
And I see that drop-down 'Project type' value is "public" in page 'Project settings'