@ST_1763 @LeftPanelContents @REL-2082
Feature: Project panel contents should be displayed according to user's role on Dashboard page

Background: 

Given I am signed in as "DO"
And I navigate to page "Dashboard"

@SC_6836
Scenario: Left panel icons should not be available when Git is not set
When I create project in page 'Dashboard'
|projectName:project_name2|projectKey:random|description:test|projectType:private|
And I see that project is added to panel 'Projects'
When I select project in panel 'Projects' in page "Dashboard"
Then I can see that "Activity" left column icon has tooltip "The project should be uploaded from Git repository"
And I can see that "Editor" left column icon has tooltip "The project should be uploaded from Git repository"