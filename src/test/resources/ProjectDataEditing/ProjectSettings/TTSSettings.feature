@ST_1766 @TTSSettings
Feature: TTS settings

@SC_6846 @REL-825
Scenario: Check task tracker notifications
Given I am signed in as "DO"
When I navigate to page "Dashboard"
When I create project in page 'Dashboard'
|projectName:project_name2|projectKey:random|description:test|projectType:private|
And I see that project is added to panel 'Projects'
When I select project in panel 'Projects' in page "Dashboard"
Then I see that all the entered data matches data in page 'Project Settings' for the new project
When I change Jira path to "https://jira.nixsolutions.com/"
And I selected menu "default" in dropdown 'Account' at block TTS
Then I see success notification ""
And I see that "https://jira.nixsolutions.com/" is added to setting 'Task tracking system' in page 'Project settings'

@SC_6847 @REL-830
Scenario: Make changes in task tracker
Given I am signed in as "DO"
When I navigate to page "Dashboard"
And I check that project "rDefaultOne" is absent in DB
When I create project in page 'Dashboard'
|projectName:project_name2|projectKey:rDefaultOne|description:test|projectType:private|
And I see that project is added to panel 'Projects'
When I select project in panel 'Projects' in page "Dashboard"
Then I see that all the entered data matches data in page 'Project Settings' for the new project
When I change Jira path to "https://jira.nixsolutions.com/"
And I selected menu "default" in dropdown 'Account' at block TTS
Then I see success notification ""
And I see that "https://jira.nixsolutions.com/" is added to setting 'Task tracking system' in page 'Project settings'