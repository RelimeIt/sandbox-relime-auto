@ST_1765 @SelectAccount @REL-371
Feature: Adding an account on Project Settings page

Background: 

Given I am signed in as "DO"
And I navigate to page "Dashboard"
When I check that project "rDefaultOne" is absent in DB
And I check that VCS "default" is absent in DB
When I create project in page 'Dashboard'
|projectName:project_name2|projectKey:random|description:test|projectType:private|
And I see that project is added to panel 'Projects'
When I select project in panel 'Projects' in page "Dashboard"
When I change Git path to "default"
Then I see success notification ""
And I see that Git "default" is added in page 'Project settings'

@SC_6842
Scenario: Add valid VCS account
When I selected menu "default" in dropdown 'Account' at block VCS
Then I see success notification "Account was linked successfully!"

@SC_6843
Scenario: Add invalid VCS account
When I selected menu "wrong" in dropdown 'Account' at block VCS
Then I see "Repository is not accessible with these credentials" warning under VCS account field

@SC_6844
Scenario: Add valid TTS account
When I change Jira path to "https://jira.nixsolutions.com/"
Then I see success notification ""
Then I see that "https://jira.nixsolutions.com/" is added to setting 'Task tracking system' in page 'Project settings'
When I selected menu "default" in dropdown 'Account' at block TTS
Then I see success notification "Task tracking system was changed successfully!"

@SC_6845
Scenario: Add invalid TTS account
When I change Jira path to "https://jira.nixsolutions.com/"
Then I see success notification ""
And I see that "https://jira.nixsolutions.com/" is added to setting 'Task tracking system' in page 'Project settings'
When I selected menu "wrong" in dropdown 'Account' at block TTS
And I see "Task tracker is not accessible with this account" warning under TTS account field