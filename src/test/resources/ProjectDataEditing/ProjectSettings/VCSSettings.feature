@ST_1767 @VCSSettings
Feature: VCS settings

Background: 

Then I see that all the entered data matches data in page 'Project Settings' for the new project
When I select project in panel 'Projects' in page "Dashboard"
And I see that project is added to panel 'Projects'
|projectName:project_name2|projectKey:random|description:test|projectType:private|
#And I check that project "F002" is absent in DB
When I create project in page 'Dashboard'
And I check that VCS "default" is absent in DB
When I navigate to page "Dashboard"
Given I am signed in as "DO"

@SC_6849 @REL-800
Scenario: Check that VCS is added to project only once
When I change Git path to "default"
Then I see success notification ""
And I see that Git "default" is added in page 'Project settings'
When I selected menu "default" in dropdown 'Account' at block VCS
Then I see that Git "default" is added in page 'Project settings'
And I am not able to add or delete VCS

@SC_6850 @ignore @REL-829
Scenario: Check VCS with invalid account
When I change Git path to "default"
Then I see success notification ""
And I see that Git "default" is added in page 'Project settings'
When I selected menu "wrong" in dropdown 'Account' at block VCS
Then I see error notification ""

@SC_6851 @REL-778
Scenario: Check validation for path field for VERSION CONTROL SYSTEM table when you add VCS
When I change Git path to ""
Then I see "Field is required" warning under VCS Path field
When I change Git path to "github.com/san4a/SomeRepository"
Then I see "Must start with 'http(s)://.'" warning under VCS Path field
When I change Git path to "default"
Then I see that Git "default" is added in page 'Project settings'