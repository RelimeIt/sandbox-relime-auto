@ST_1761 @CreateNewProject
Feature: Create new project

Background: 

Given I am signed in as "DO"
When I navigate to page "Dashboard"
And I check that project "rDefaultOne" is absent in DB

@SC_6829 @REL-824
Scenario: Create new project
When I create project in page 'Dashboard'
|projectName:project_name1|projectKey:random|projectType:private|
And I see that project is added to panel 'Projects'
When I select project in panel 'Projects' in page "Dashboard"
Then I see that all the entered data matches data in page 'Project Settings' for the new project

@SC_6830 @REL-613
Scenario: Check that validation message 'This project key is already in use' must be shown under the Key field filled with existing key
When I create project in page 'Dashboard'
|projectName:project_name2|projectKey:rDefaultOne|description:test|projectType:private|
And I see that project is added to panel 'Projects'
And I open pop-up 'New Project' in panel 'Projects' in page 'Dashboard'
And I fill in data in pop-up 'New project'
|projectName:project_name2|projectKey:rDefaultOne|description:test|projectType:private|
Then I see a notification message "This project key is already in use" under "1" required fields on pop-up "New Project"
When I finish creating "New Project"
Then I see pop up "New Project" was not closed

@SC_6831 @REL-617
Scenario: Check that project with existed name can be created
When I create project in page 'Dashboard'
|projectName:project_name2|projectKey:random|description:test|projectType:private|
Then I see that project is added to panel 'Projects'
When I create project in page 'Dashboard'
|projectName:project_name2|projectKey:random|description:test|projectType:private|
Then I see that project is added to panel 'Projects'

@SC_6832 @REL-630
Scenario: Check that characters in project key name are in upper-case
When I open pop-up 'New Project' in panel 'Projects' in page 'Dashboard'
And I fill in data in pop-up 'New project'
|projectKey:test|
And I see that characters in 'Project key' field in pop-up 'New Project' are in upper-case

@SC_6833 @REL-629
Scenario: Check that project key consists of 3 or 4 characters
When I open pop-up 'New Project' in panel 'Projects' in page 'Dashboard'
And I fill in data in pop-up 'New project'
|projectKey:te|
Then I see a notification message "Must be 3 or 4 characters length" under second field
When I fill in data in pop-up 'New project'
|projectKey:text1|
Then I see a notification message "Must be 3 or 4 characters length" under second field