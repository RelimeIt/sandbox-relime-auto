@ST_1730 @Smoke
Feature: Smoke test set

Background: Sign in

Given I am signed in as "DO"
And I check that VCS "smoke" is absent in DB
And I navigate to page "Dashboard"

@SC_6679
Scenario: Access to private domain for non-subsribed user
When I am signed in as "unsubscribed"
And I select domain "private" by direct link
Then I see warning notification "Invalid domain name. You were redirected to the Profile page."
And I am in page "Profile"

@SC_6680
Scenario: Access to the feature within Public project in Public domain for DO after following direct link and Sign In
When I navigate to page "Editor" for project "public"
And I update all features from Git on Editor page
And I select feature "search_book" in the tree
And I save current link
And I navigate to page "Profile"
And I sign out
And I navigate by saved link
Then I am in page "Editor"
When I am signed in as "DO"
Then I am in page "Editor"
And I see story "search_book" is selected in the tree

@SC_6681
Scenario: Access the scenario within Private project in Private domain for PM after following direct link and Sign In
And I select domain "private" by direct link
When I select project "private" in panel 'Projects' in page "Dashboard"
And I update project from Git on Settings page
When I navigate to page "Editor"
And I select feature "search_book" in the tree
And I select "Search books by publication year" scenario in the table
And I save current link
And I sign out
And I navigate by saved link
Then I am in page "Default"
And I can see 'Sign in' dialog is opened
When I am signed in as "PM"
Then I am in page "Editor"
And I see story "search_book" is selected in the tree
And I see scenario "Search books by publication year" is expanded

@SC_6682
Scenario: Access to the feature within Private project in Public domain for TM after following direct link and Sign In
When I check that user "TM" is subscribed on project "private" as "TM"
When I select project "private" in panel 'Projects' in page "Dashboard"
And I update project from Git on Settings page
When I navigate to page "Editor"
And I select feature "ElevenScenarios" in the tree
And I select page "2" on scenarios pagination panel
And I save current link
When I am signed in as "TM"
And I navigate by saved link
Then I am in page "Editor"
And I see story "ElevenScenarios" is selected in the tree
And I am on the "2" page on scenarios pagination panel

@SC_6683
Scenario: Access to public domain for non-subsribed user
Given I check that user "unsubscribed" is not subscribed on project "public"
When I am signed in as "unsubscribed"
And I select domain "domain" by direct link
Then I can see that domain "default" contains only "public" projects
And I can see that Actions column is not displayed for projects

@SC_6684 @REL-863
Scenario: Create project
When I create project in page 'Dashboard'
|projectName:project_name1|projectKey:random|description:test|projectType:private|
And I see that project is added to panel 'Projects'
And I select project in panel 'Projects' in page "Dashboard"
And I see that all the entered data matches data in page 'Project Settings' for the new project

@SC_6685 @REL-876
Scenario: Add VCS
When I create project in page 'Dashboard'
|projectName:project_name1|projectKey:random|description:test|projectType:private|
And I see that project is added to panel 'Projects'
And I select project in panel 'Projects' in page "Dashboard"
And I change drop-down 'Saving mode' value to "requiring confirmation" in page 'Project settings'
When I change Git path to "smoke"
When I selected menu "San4aGit" in dropdown 'Account' at block VCS
Then I see that Git "smoke" is added in page 'Project settings'

@SC_6686 @REL-879
Scenario: Update from Git
When I create project in page 'Dashboard'
|projectName:project_name1|projectKey:random|description:test|projectType:private|
And I see that project is added to panel 'Projects'
And I select project in panel 'Projects' in page "Dashboard"
And I change drop-down 'Saving mode' value to "requiring confirmation" in page 'Project settings'
When I change Git path to "smoke"
Then I see that Git "smoke" is added in page 'Project settings'
When I selected menu "default" in dropdown 'Account' at block VCS
Then I see that Git "smoke" is added in page 'Project settings'
When I update project from Git on Settings page
And I read folder structure from GIT "smoke"
And I navigate to page "Editor"
When I update all features from Git on Editor page
And I read folder structure from Editor page
Then I check that GitHub and ReLime trees are the same

@SC_6687 @REL-871
Scenario: Edit folder
When I create project in page 'Dashboard'
|projectName:project_name1|projectKey:random|description:test|projectType:private|
And I see that project is added to panel 'Projects'
And I select project in panel 'Projects' in page "Dashboard"
And I change drop-down 'Saving mode' value to "requiring confirmation" in page 'Project settings'
When I change Git path to "smoke"
Then I see that Git "smoke" is added in page 'Project settings'
When I selected menu "default" in dropdown 'Account' at block VCS
When I update project from Git on Settings page
And I navigate to page "Editor"
And I create folder "folderName_1"
Then I see that new folder is successfully created
And I select folder "folderName_1" in the tree
When I change textbox 'Folder name' value to "folderName_1_1" in folder info block under the tree
And I push 'Enter'
Then I see success notification ""
And I see that changes are successfully saved in textbox 'Folder name' in folder info block under the tree

@SC_6688 @REL-875
Scenario: Add TTS
When I create project in page 'Dashboard'
|projectName:project_name1|projectKey:random|description:test|projectType:private|
And I see that project is added to panel 'Projects'
And I select project in panel 'Projects' in page "Dashboard"
And I change drop-down 'Saving mode' value to "requiring confirmation" in page 'Project settings'
When I change Git path to "smoke"
And I see success notification ""
Then I see that Git "smoke" is added in page 'Project settings'
And I change Jira path to "https://jira.nixsolutions.com/"
And I see success notification ""
And I tie "DansJira" account in TTS settings
And I see success notification ""
Then I see that "https://jira.nixsolutions.com/" is added to setting 'Task tracking system' in page 'Project settings'
And I see that entered data matches data in setting 'Task tracking system' in page 'Project settings' for Jira

@SC_6689 @REL-877
Scenario: Create Jira-linked feature
When I create project in page 'Dashboard'
|projectName:project_name1|projectKey:random|description:test|projectType:private|
And I see that project is added to panel 'Projects'
And I select project in panel 'Projects' in page "Dashboard"
And I change drop-down 'Saving mode' value to "requiring confirmation" in page 'Project settings'
When I change Git path to "smoke"
Then I see that Git "smoke" is added in page 'Project settings'
And I see success notification ""
And I change Jira path to "https://jira.nixsolutions.com/"
When I selected menu "default" in dropdown 'Account' at block VCS
When I update project from Git on Settings page
And I tie "default" account in TTS settings
And I see success notification ""
Then I see that "https://jira.nixsolutions.com/" is added to setting 'Task tracking system' in page 'Project settings'
And I see that entered data matches data in setting 'Task tracking system' in page 'Project settings' for Jira
And I navigate to page "Editor"
And I create Jira-linked feature "smoke"
Then I see that new jira-linked feature is successfully created
And I see that data from linked Jira issue matches data in new feature in feature info block under the tree

@SC_6690 @REL-878
Scenario: Remove TTS
When I create project in page 'Dashboard'
|projectName:project_name1|projectKey:random|description:test|projectType:private|
And I see that project is added to panel 'Projects'
And I select project in panel 'Projects' in page "Dashboard"
And I change drop-down 'Saving mode' value to "requiring confirmation" in page 'Project settings'
When I change Git path to "smoke"
Then I see that Git "smoke" is added in page 'Project settings'
And I change Jira path to "https://jira.nixsolutions.com/"
And I tie "DansJira" account in TTS settings
And I see success notification ""
Then I see that "https://jira.nixsolutions.com/" is added to setting 'Task tracking system' in page 'Project settings'
And I see that entered data matches data in setting 'Task tracking system' in page 'Project settings' for Jira
And I can delete "https://jira.nixsolutions.com/" from setting 'Task tracking system' in page 'Project settings'

@SC_6691 @REL-884
Scenario: Remove project
When I create project in page 'Dashboard'
|projectName:project_name1|projectKey:random|description:test|projectType:private|
When I delete project in panel 'Projects' in page "Dashboard"
Then I see success notification ""
And I see that the project was successfully deleted