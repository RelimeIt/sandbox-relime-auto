@ST_1731 @DeleteUsersFromDomain @REL-1770
Feature: Deleting users from domain by Team panel by Domain Owner

@SC_6692
Scenario: Checking that subscriber may be deleted by Domain Owner on Team Panel
Given I check that user "unsubscribed" is subscribed on project "public" as "PM"
And I check that user "unsubscribed" is subscribed on project "private" as "TM"
And I am signed in as "DO"
When I navigate to page "Dashboard"
And I delete user "unsubscribed" from domain
And I see pop up "DELETE USER FROM DOMAIN" is opened
And I decline "DELETE USER FROM DOMAIN" pop up option
Then I see pop up "DELETE USER FROM DOMAIN" was closed
And I can see that user "unsubscribed" is subscribed on current domain on Dashboard
When I delete user "unsubscribed" from domain
And I see pop up "DELETE USER FROM DOMAIN" is opened
And I accept delete option in "DELETE USER FROM DOMAIN" pop up
Then I see pop up "DELETE USER FROM DOMAIN" was closed
And I see success notification ""
And I can see that user "unsubscribed" is not subscribed on current domain on Dashboard
When I am signed in as "unsubscribed"
Then I can see that domain "public" is not displayed on profile page