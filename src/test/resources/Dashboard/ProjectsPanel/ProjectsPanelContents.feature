@ST_1735 @ProjectPanelContents @REL-1748 @REL-1996
Feature: Project panel contents should be displayed according to user's role on Dashboard page

Background: 

Given I am signed in as "DO"

@SC_6705
Scenario: Only current domain projects are displayed on Dashboard
When I navigate to page "Dashboard"
Then I can see that domain "default" contains only related projects

@SC_6706
Scenario: Dashboard panel for TM/PM user: My subscription tab
When I select domain "auxiliary" by direct link
Then I see tab "My subscriptions" is active
And I see Project table column "#" is present
And I see Project table column "Name" is present
And I see Project table column "Key" is present
And I see Project table column "Actions" is present
And I can see that Actions column has "2" icons

@SC_6707
Scenario: Dashboard panel for TM/PM user: Public projects tab
When I select domain "auxiliary" by direct link
When I select tab "Public projects" on dashboard page
Then I see Project table column "#" is present
And I see Project table column "Name" is present
And I see Project table column "Key" is present
And I see Project table column "Actions" is not present

@SC_6708
Scenario: Dashboard panel for DO user
When I navigate to page "Dashboard"
Then I see no tabs on dashboard page
And I can see that domain "default" contains only related projects
And I see Project table column "#" is present
And I see Project table column "Name" is present
And I see Project table column "Key" is present
And I see Project table column "Actions" is present
And I can see that Actions column has "2" icons
And I see button ‘Create new project’

@SC_6709
Scenario: Message should be displayed if no projects are displayed on Dashboard
When I select domain "empty" by direct link
Then I see message 'No projects exist for the current domain'