@ST_1736 @REL-347 @PaginationPanel @REL-1027
Feature: Pagination panel on a Dashboard

@SC_6710
Scenario: Check that pagination panel is not displayed if less than "5" projects on Dashboard page
Given I am signed in as "DO"
When I select domain "5projects" by direct link
Then I see that pagination element didn't appear
And I can see that pagination element is absent in Projects table

@SC_6711
Scenario: Check pagination panel for projects on Dashboard page
Given I am signed in as "DO"
When I navigate to page "Dashboard"
Then I can see that pagination element is present in Projects table
When I go to the next page using pagination panel in Projects table
Then I am in page "2" on pagination panel in Projects table
When I go to the previous page using pagination panel in Projects table
Then I am in page "1" on pagination panel in Projects table

@SC_6712
Scenario: Quantity of projects per page for DO
Given I am signed in as "DO"
When I navigate to page "Dashboard"
Then I can see only 10 projects on page

@SC_6713
Scenario: Quantity of projects per page for PM/TM
Given I navigate to page "Dashboard"
And I am signed in as "PM"
When I navigate to page "Dashboard"
Then I can see only 10 projects on page

@SC_6714
Scenario: “…” separator work
Given I am signed in as "DO"
When I navigate to page "Dashboard"
And I click button "..." on pagination panel in Projects table
Then I am in page "4" on pagination panel in Projects table
And I click button "..." on pagination panel in Projects table
Then I am in page "6" on pagination panel in Projects table
And I click button "..." on pagination panel in Projects table
Then I am in page "4" on pagination panel in Projects table