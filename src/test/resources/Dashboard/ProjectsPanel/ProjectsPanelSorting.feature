@ST_1738 @ProjectsPanelSorting @REL-1743
Feature: Sorting projects

@SC_6718
Scenario: Public and Private checkboxes
Given I am signed in as "DO"
When I navigate to page "Dashboard"
Then I can see that checkbox 'Private' above Projects table is enabled
And I can see that checkbox 'Public' above Projects table is enabled

@SC_6719
Scenario: Public projects
Given I am signed in as "DO"
When I navigate to page "Dashboard"
And I select checkbox 'Private' above Projects table
Then I can see that domain "default" contains only "public" projects

@SC_6720
Scenario: Private projects
Given I am signed in as "DO"
When I navigate to page "Dashboard"
And I click checkbox 'Public' above Projects table
Then I can see that domain "default" contains only "private" projects

@SC_6721
Scenario: Checkboxes are unchecked
Given I am signed in as "DO"
When I navigate to page "Dashboard"
And I click checkbox 'Private' above Projects table
And I click checkbox 'Public' above Projects table
Then I can see placeholder "No projects match selected criteria\nSelect project type to watch your projects" above Projects table