@ST_1737 @ProjectsPanelSearch @REL-1752
Feature: User should be able to search projects by name on Projects panel on Dashboard page

@SC_6715
Scenario: Check block 'Search' view
Given I am signed in as "DO"
When I navigate to page "Dashboard"
Then I can see block 'Search' in Projects panel
And I can see block 'Search' is displayed correctly in Projects panel

@SC_6716
Scenario: Check 'Search' block functionality for Domain Owner
Given I am signed in as "DO"
And I navigate to page "Dashboard"
When I type "name" into 'Search' field in Projects panel
Then I can see that Projects panel contains only projects that contain "name"
When I type "ridiculus!" into 'Search' field in Projects panel
Then I can see placeholder "No projects found" above Projects table

@SC_6717
Scenario: Check 'Search' block functionality for Project managers and Team Members
Given I am signed in as "PM"
And I navigate to page "Dashboard"
Then I can see block 'Search' in Projects panel
And I see tab "My subscriptions" is active
When I type "Test" into 'Search' field in Projects panel
Then I can see that Projects panel contains only projects that contain "Test"
When I select tab "Public projects" on dashboard page
Then I can see that 'Search' field text equals "Test"  in Projects panel
When I type "name" into 'Search' field in Projects panel
Then I can see that Projects panel contains only projects that contain "name"