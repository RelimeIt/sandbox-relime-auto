@ST_1732 @PublicDashboardForNonSubscribedUser @REL-1777
Feature: Not subscribed user should be able to watch public projects for public domain

@SC_6693
Scenario: Dashboard for not logged-in user
Given I sign out
When I navigate to page "Dashboard"
Then I can see panel Team is not visible
And I can see block 'Search' in Projects panel
And I can see that domain "default" contains only "public" projects
And I see Project table column "#" is present
And I see Project table column "Name" is present
And I see Project table column "Key" is present
And I see Project table column "Actions" is not present
And I can see explanation text "This is public domain dashboard. Public projects are available in read-only mode. To start working with Relime please sign in."
And I can see 'Sign in' button in Greetings block
And I can see 'Create an account' link in Greetings block
And I can see 'Learn more about Relime' link in Greetings block
When I type "name" into 'Search' field in Projects panel
Then I can see that Projects panel contains only projects that contain "name"
When I type "" into 'Search' field in Projects panel
And I select project "public" in panel 'Projects' in page "Dashboard"
Then I am in page "Statistics Page"

@SC_6694
Scenario: Dashboard for not subscribed user
Given I check that user "unsubscribed" is not subscribed on project "public"
And I am signed in as "unsubscribed"
When I navigate to page "Dashboard"
Then I can see panel Team is not visible
And I can see block 'Search' in Projects panel
And I can see that domain "default" contains only "public" projects
And I see Project table column "#" is present
And I see Project table column "Name" is present
And I see Project table column "Key" is present
And I see Project table column "Actions" is not present
And I can see explanation text "This is public domain dashboard. Public projects are available in read-only mode. Go to your Profile page to view your domains or create a new one"
And I can see 'Create domain' button in Greetings block
And I can see 'Learn more about Relime' link in Greetings block
When I type "name" into 'Search' field in Projects panel
Then I can see that Projects panel contains only projects that contain "name"
When I type "" into 'Search' field in Projects panel
And I select project "public" in panel 'Projects' in page "Dashboard"
Then I am in page "Statistics Page"

@SC_6695
Scenario: Team panel on Dashboard for not subscribed user
Given I check that user "unsubscribed" is not subscribed on project "public"
And I am signed in as "unsubscribed"
When I navigate to page "Dashboard"
And I choose 'Create domain' option in Greetings block
Then I am in page "Default"
And I can see 'Careate domain' dialog is opened
When I navigate to page "Dashboard"
And I choose 'Profile page' option in Greetings block
Then I am in page "Profile"
When I navigate to page "Dashboard"
And I choose 'Learn more about Relime' option in Greetings block
Then I am in page "About"