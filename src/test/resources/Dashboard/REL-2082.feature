@ST_1733
Feature: Only Project settings page should not available for DO and PM users till them loads the project from Git repository

Background: 

Given I am signed in as "DO"
And I’m on Dashboard page
And I selected project without GIT data

@SC_6697 @bug
Scenario: 'Activity' icon tooltip on left panel is not displayed
When I hovered 'Activity' icon on left panel
And I checked tooltip
Then I see tooltip "The project should be uploaded from Git repository"

@SC_6698 @bug
Scenario: 'Editor' icon tooltip on left panel is not displayed
When I hovered 'Editor' icon on left panel
And I checked tooltip
Then I see tooltip "The project should be uploaded from Git repository"

@SC_6699 @bug
Scenario: 'Feature Managemen' icon tooltip on left panel is not displayed
When I hovered 'Feature Management' icons on left panel
And I checked tooltip
Then I see tooltip "The project should be uploaded from Git repository"