@ST_1739 @TeamPanelEmpty @REL-1937
Feature: Display explanation text on Team panel when no users present in it

@SC_6722
Scenario: Sign in as DO
Given I am signed in as "DO"
When I select domain "empty1" by direct link
Then I can see message "No users are subscribed to projects of the current domain" on Team panel
And I see that link 'Learn more about Relime' is displayed
And I see that picture for empty Team panel is displayed

@SC_6723
Scenario: Sign in as PM / TM
And I am signed in as "PM"
When I select domain "empty2" by direct link
Then I can see message "No users are subscribed to projects of the current domain" on Team panel
And I see that link 'Learn more about Relime' is displayed
And I see that picture for empty Team panel is displayed