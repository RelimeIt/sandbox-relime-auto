@ST_1742 @TeamPanelUnsubscribe @REL-1753
Feature: Team Members and Project Managers should be able to unsubscribe from the project

@SC_6730
Scenario: Check behavior when user clicks OK button in pop-up 'Unsubscribe me'
Given I check that user "unsubscribed" is subscribed on project "public" as "PM"
And I check that user "unsubscribed" is subscribed on project "private" as "TM"
And I am signed in as "unsubscribed"
And I navigate to page "Dashboard"
When I choose 'Unsubscribe' for project "public"
Then I see pop up "Unsubscribe me" is opened
And I can see button "Cancel" on pop-up "Unsubscribe me"
When I accept "Unsubscribe me" option
And I see success notification ""
Then I can see that project "public" is absent
Then I can see that only users of subscribed projects are displayed for user "unsubscribed"