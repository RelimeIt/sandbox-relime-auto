@ST_1740 @TeamPanelSearch @REL-1774
Feature: User should be able to search other users data by Team panel on Dashboard page

@SC_6724
Scenario: Search block on Team panel
Given I am signed in as "DO"
When I navigate to page "Dashboard"
Then I can see panel Team is visible
And I can see block 'Search' in Team panel
And I can see block 'Search' is displayed correctly in Team panel

@SC_6725
Scenario: Search functionality on Team panel
Given I am signed in as "DO"
When I navigate to page "Dashboard"
Then I can see panel Team is visible
When I type "auto" into 'Search' field in Team panel
Then I can see that Team panel contains only users that contain "auto"
When I type "auto tes" into 'Search' field in Team panel
Then I can see that Team panel contains only users that contain "auto tes"
When I type "autotesting" into 'Search' field in Team panel
Then I can see that Team panel contains only users that contain email "autotesting"
When I type "automation" into 'Search' field in Team panel
Then I can see that Team panel contains only subscribers that contain project "Automation"
When I type "never use this name" into 'Search' field in Team panel
Then I can see message "No users found" on Team panel

@SC_6726
Scenario: Manipulations with results
Given I am signed in as "DO"
When I navigate to page "Dashboard"
Then I can see panel Team is visible
When I type "auto" into 'Search' field in Team panel
Then I can see that Team panel contains only users that contain "auto"
When I sort subscribers table by column 'Name'
Then I can see that subscribers are sorted by "ascending" order by first name

@SC_6727
Scenario: List of subscribed users on Team panel for PM
Given I am signed in as "PM"
When I navigate to page "Dashboard"
Then I can see that only users of subscribed projects are displayed for user "PM"