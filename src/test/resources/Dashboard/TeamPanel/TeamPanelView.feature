@ST_1743 @TeamPanel @REL-1766
Feature: Team panel view for Domain Owner on Dashboard page

@SC_6731
Scenario: Team panel view for TM user and it's fuctionality
Given I am signed in as "TM"
When I navigate to page "Dashboard"
Then I can see Team table column "Actions" is not present
When I select subscriber "PM" in Team table
Then I can see that user "PM" has only projects that he is subscribed to within current domain in Team panel
And I can see that I can not change role for subscriber "PM"

@SC_6732
Scenario: Team panel view for PM user and role changing
Given I check that user "unsubscribed" is subscribed on project "public" as "PM"
And I check that user "unsubscribed" is subscribed on project "private" as "PM"
And I am signed in as "PM"
When I navigate to page "Dashboard"
Then I can see Team table column "Actions" is not present
When I select subscriber "unsubscribed" in Team table
Then I can see that user "unsubscribed" has only projects that he is subscribed to within current domain in Team panel
When I change role for user "unsubscribed" in project "private" to "TM"
Then I see success notification ""
When I am signed in as "unsubscribed"
And I navigate to page "Dashboard"
Then I can see that current user has role "TM" in project "private"

@SC_6733
Scenario: Unsibscribe functionality for PM user within Team panel on Dashboard
Given I check that user "unsubscribed" is subscribed on project "public" as "PM"
And I check that user "unsubscribed" is subscribed on project "private" as "PM"
And I am signed in as "PM"
When I navigate to page "Dashboard"
Then I can see Team table column "Actions" is not present
When I select subscriber "unsubscribed" in Team table
When I unsubscribe user "unsubscribed" from project "private"
Then I see pop up "Unsubscribe user" is opened
When I decline "Unsubscribe user" option in pop up
Then I see pop up "Unsubscribe user" was closed
When I unsubscribe user "unsubscribed" from project "private"
Then I see pop up "Unsubscribe user" is opened
When I accept "Unsubscribe user" option in pop up
Then I see pop up "Unsubscribe user" was closed
And I see success notification ""
Then I check that user "unsubscribed" is not subscribed on project "private"
When I am signed in as "unsubscribed"
And I navigate to page "Dashboard"
Then I can see that project "private" is absent

@SC_6734
Scenario: Check columns on list of subscribers on panel 'Team'
Given I am signed in as "DO"
When I navigate to page "Dashboard"
And I see the list of subscribers on panel 'Team'
And I can see Team table column "#" is present
And I can see Team table column "Name" is present
And I can see Team table column "Projects" is present
And I can see Team table column "Actions" is present

@SC_6735
Scenario: Check that 10 users should be displayed per page on panel 'Team'
Given I am signed in as "DO"
When I navigate to page "Dashboard"
And I can see that pagination element is present in Team table

@SC_6736
Scenario: Check that each table row is represented by accordion
Given I am signed in as "DO"
When I navigate to page "Dashboard"
Then I can see panel Team is visible
And I see that all table rows are collapsed

@SC_6737
Scenario: Check collapsed accordion
Given I am signed in as "DO"
When I navigate to page "Dashboard"
Then I can see panel Team is visible
And I can see Team table column "#" is present
And I can see Team table column "Name" is present
And I can see Team table column "Projects" is present
And I can see Team table column "Actions" is present

@SC_6738
Scenario: Check expanded accordion
Given I am signed in as "DO"
When I navigate to page "Dashboard"
And I select subscriber "PM" in Team table
And I can see Team table column "#" is present
And I can see Team table column "Name" is present
And I can see Team table column "Projects" is present
And I can see Team table column "Actions" is present
And I can see that subscriber "PM" is expanded