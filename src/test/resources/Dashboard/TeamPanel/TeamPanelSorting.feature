@ST_1741 @REL-1775 @TeamPanelSorting
Feature: User should be able to sort users by Name on Dashboard page

@SC_6728
Scenario: Sorting availability in TEAM panel for Domain Owner
Given I am signed in as "DO"
When I navigate to page "Dashboard"
Then I can see panel Team is visible
And I can see that subscribers table has "10" rows
And I can see that column header 'Name' has an arrow in subscribers table
When I sort subscribers table by'Name'
Then I can see that subscribers are sorted by "ascending" order by first name
When I sort subscribers table by 'Name'
Then I see that numeration in Subscribers panel is correct
And I can see that subscribers are sorted by "descending" order by first name

@SC_6729
Scenario: Sorting availability in TEAM panel for subscribed PM user
Given I am signed in as "PM"
When I navigate to page "Dashboard"
Then I can see panel Team is visible
And I can see that subscribers table has "10" rows
And I can see that column header 'Name' has an arrow in subscribers table
When I sort subscribers table by 'Name'
And I can see that subscribers are sorted by "ascending" order by first name
When I sort subscribers table by 'Name'
Then I see that numeration in Subscribers panel is correct
And I can see that subscribers are sorted by "descending" order by first name