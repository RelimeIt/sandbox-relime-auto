@ST_1734
Feature: Adding an account on Project Settings page

@SC_6700
Scenario: Add valid VCS account
Given I am signed in as "DO"
And I’m on Project Settings page
And I added GIT path
And I have accounts in dropdown
When I select valid account from dropdown
Then I see notification ‘Account was linked successfully’

@SC_6701
Scenario: Add invalid VCS account
Given I am signed in as "DO"
And I’m on Project Settings page
And I added GIT path to private repository
And I have GIT accounts in dropdown
When I select invalid GIT account
Then I see validation message ‘Repository is not accessible with these credentials’

@SC_6702
Scenario: Add valid TTS account
Given I am signed in as "DO"
And I’m on Project Settings page
And I added Jira path
And I have GIT accounts in dropdown
When I select valid GIT account
Then I see notification ‘Account was linked successfully’

@SC_6703
Scenario: Add invalid TTS account
Given I am signed in as "DO"
And I’m on Project Settings page
And I added Jira path
And I have GIT accounts in dropdown
When I select invalid GIT account
Then I see validation message ‘Task tracker is not accessible with this account’