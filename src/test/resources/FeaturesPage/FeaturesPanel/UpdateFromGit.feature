@ST_1748 @REL-101 @REL-2027
Feature: Update from Git and Update from Jira icons should be disabled for TM users on Features panel on Editor page when requiring confirmation mode is set for the project

@SC_6749
Scenario: JIRA and GIT icons display within Features Tree for TM user in 'Requiring Confirmation' save mode
Given I am signed in as "TM"
And I navigate to page "Project Settings" for project "public"
And I selected menu "default" in dropdown 'Account' at block VCS
And I selected menu "default" in dropdown 'Account' at block TTS
When I navigate to page "Editor"
Then I can see that button 'Upload from Git' is not displayed
And I can see that button 'Upload from Jira' is not displayed

@SC_6750
Scenario: JIRA and GIT items within 'Actions' drop-down display for TM user in 'Requiring Confirmation' save mode
  I am logged as TM user for the project I am subscribed to
Given I am signed in as "TM"
And I navigate to page "Project Settings" for project "public"
And I selected menu "default" in dropdown 'Account' at block VCS
And I selected menu "default" in dropdown 'Account' at block TTS
And I navigate to page "Editor"
When I select feature "REL-" in the tree
And I select "33" scenario in the table
Then I can see that option "Update from Git" is not enabled in drop-down 'Actions' in scenario accordion
And I can see that option "Update from Jira" is not enabled in drop-down 'Actions' in scenario accordion