@ST_1745 @CreateNewFolder
Feature: Create new folder

Background: 

Given I am signed in as "DO"
And I navigate to page "Editor" for project "public"
And I update all features from Git on Editor page

@SC_6743 @REL-480
Scenario: Name folder using invalid symbols
When I create folder "!@#"
Then I see a notification message "Allowed characters: ' A-z 0-9 _ . - '" under "folder" field on pop-up "New folder"
And I see pop up "New folder" was not closed

@SC_6744 @REL-476
Scenario: Name new folder using dot in the beginning
When I create folder ".dot"
Then I see a notification message "Can't start with '.'" under "folder" field on pop-up "New folder"
And I see pop up "New folder" was not closed

@SC_6745 @REL-471
Scenario: Name folder using valid symbols
When I create folder "folderName_1"
Then I see that new folder is successfully created
And I see that the new folder is selected in the tree

@SC_6746 @REL-444
Scenario: Name duplicate folder
When I create folder "folderName_1"
Then I see that new folder is successfully created
And I see that the new folder is selected in the tree
And I select folder "feature" in the tree
When I create folder "folderName_1"
And I see pop up "New folder" was not closed