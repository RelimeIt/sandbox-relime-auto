@ST_1744 @CreateNewFeature
Feature: Create new feature

Background: 

Given I am signed in as "DO"
And I navigate to page "Editor" for project "private"
And I update all features from Git on Editor page

@SC_6740 @REL-490
Scenario: Invalid symbols for file name field
And I open pop-up 'New feature' in panel 'Features'
And I fill in data in pop-up 'New feature'
|fileName:!@3A2_-|
Then I see a notification message "Story file name must contain only the following characters: A-z 0-9 _ -" under "fileName" field on pop-up "New feature"

@SC_6741 @REL-529
Scenario: Valid symbols for file name
And I create feature in page 'Editor'
|featureName:featureName_1|fileName:fileName_1|description:test|tag:tag1|tag:tag2|
Then I see that new feature is successfully created
And I see that the new feature is selected in the tree