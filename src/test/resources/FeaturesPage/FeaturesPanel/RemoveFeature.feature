@ST_1746 @RemoveFeature
Feature: Remove feature

@SC_6747 @REL-449
Scenario: Remove feature
Given I am signed in as "DO"
And I navigate to page "Editor" for project "public"
And I update all features from Git on Editor page
When I delete feature "salary_management" in the tree
Then I see that the feature was successfully deleted