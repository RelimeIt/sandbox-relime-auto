@ST_1747 @RemoveFolder
Feature: Remove folder

@SC_6748 @REL-446
Scenario: Remove folder
Given I am signed in as "DO"
And I navigate to page "Editor" for project "public"
And I update all features from Git on Editor page
When I delete folder "book" in the tree
Then I see that the folder was successfully deleted