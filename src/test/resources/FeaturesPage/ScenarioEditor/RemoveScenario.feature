@ST_1751 @RemoveScenario
Feature: Remove scenario

@SC_6789 @REL-838
Scenario: Remove scenario pop-up
Given I am signed in as "DO"
And I navigate to page "Editor" for project "public"
And I update all features from Git on Editor page
When I select feature "NineScenarios" in the tree
And I select "2" scenario in the table
And I delete scenario in scenario accordion
Then I see that the scenario was successfully deleted