@ST_1750 @NewScenarioPopup
Feature: New Scenario Popup

Background: 

Given I am signed in as "DO"
And I navigate to page "Editor" for project "public"
And I update all features from Git on Editor page
And I create feature in page 'Editor'
|featureName:featureName_1|fileName:fileName_1|description:test|tag:tag1|tag:tag2|

@SC_6781 @REL-806
Scenario: Invalid symbols for tags
When I open pop-up 'New Scenario' in page Editor
And I verify invalid tags on pop-up
|>tag2|&gt; is invalid for a tag name|
|@tag1|@ is invalid for a tag name|

@SC_6782 @REL-808
Scenario: Prohibiting to create the second Background
When I create backgroung in page 'Editor'
|scenarioName:backgroundName|description:backgroundDescription|
|Given step||
|And another step||
Then I see that new background is successfully created and entered data matches data in Scenario Editor for the new background
And I see that the new background is expanded
When I open pop-up 'New Scenario' in page Editor
And I see that checkbox 'background' is disabled in pop-up 'New Scenario'
And I see that checkbox 'background' has tooltip "This story already has a background" in pop-up 'New Scenario'

@SC_6783 @REL-811
Scenario Outline: Critical functionality
When I open pop-up 'New Scenario' in page Editor
When I fill in data in pop-up 'New Scenario'
|scenarioName:scenarioName|description:scenarioDescription|tag:tag1|
|Given step|||
|And another <step>|||
Then I see that all the example tab elements appeared on popup 'New Scenario'
And I can see that example table  "step" is present on pop-up 'New Scenario'
And I see that example table has "1" rows on popup 'New Scenario'
And I see that button 'Delete row' is disabled on popup 'New Scenario'
When I add row to example table on popup 'New Scenario'
Then I see that example table has "2" rows on popup 'New Scenario'
When I change example table column "step" row "1" to "something" on pop-up 'New Scenario'
Then I see that example table column "step" row "1" value equals "something" on pop-up 'New Scenario'
When I select row "1" in example table on pop-up 'New Scenario'
Then I see that button 'Delete row' is enabled on popup 'New Scenario'
And I see that button 'Delete row' text equals "Delete Row" on popup 'New Scenario'
When I select row "2" in example table on pop-up 'New Scenario'
Then I see that button 'Delete row' text equals "Delete 2 Rows" on popup 'New Scenario'
When I delete row on popup 'New Scenario'
Then I see that example table has "1" rows on popup 'New Scenario'
When I create a scenario
Then I see that the new scenario is expanded
And I see that new scenario is created and entered data matches data in Scenario Editor for the new scenario

Examples: 
|step|
||


@SC_6784 @REL-860
Scenario: Critical functions - background
When I create backgroung in page 'Editor'
|scenarioName:backgroundName|description:backgroundDescription|
|Given step||
|And another step||
Then I see that new background is successfully created and entered data matches data in Scenario Editor for the new background
And I see that the new background is expanded

@SC_6785 @REL-807
Scenario: Background functionality
When I create backgroung in page 'Editor'
|scenarioName:backgroundName|description:backgroundDescription|
|Given step||
|And another step||
Then I see that the new background is expanded
And I see that example table is not displayed in the new background in Scenario Editor

@SC_6786 @REL-841
Scenario: Background validation
When I create background in page 'Editor'
|scenarioName:backgroundName|description:backgroundDescription|
|step||
|another step||
Then I see pop up "New Scenario" was not closed
And I see a notification message "Background must start with 'Given'" under "1" required fields on pop-up "New feature"

@SC_6787 @REL-859
Scenario: BDD validation when create a scenario in feature having no back
When I create scenario in page 'Editor'
|scenarioName:scenarioName|description:scenarioDescription|tag:tag1|
|When step|||
|And another step|||
Then I see pop up "New Scenario" was not closed
And I see a notification message "Scenario must start with 'Given' (when feature doesn't have a background)" under "1" required fields on pop-up "New feature"

@SC_6788 @REL-809
Scenario: Example table
When I open pop-up 'New Scenario' in page Editor
When I type "Given I have an example parameter" in the first string in Scenario Editor in pop-up 'New Scenario'
Then I see that all the example tab elements appeared on popup 'New Scenario'
When I press 'Enter'
Then I see that all the example tab elements appeared on popup 'New Scenario'
And I can see that example table  "example" is present on pop-up 'New Scenario'
And I see that example table has "1" rows on popup 'New Scenario'
And I see that button 'Add row' is enabled on popup 'New Scenario'
And I see that button 'Delete row' is disabled on popup 'New Scenario'
When I add row to example table on popup 'New Scenario'
Then I see that example table has "2" rows on popup 'New Scenario'
When I change example table column "example" row "1" to "something" on pop-up 'New Scenario'
Then I see that example table column "example" row "1" value equals "something" on pop-up 'New Scenario'
When I select row "1" in example table on pop-up 'New Scenario'
Then I see that button 'Delete row' is enabled on popup 'New Scenario'
And I see that button 'Delete row' text equals "Delete Row" on popup 'New Scenario'
When I select row "2" in example table on pop-up 'New Scenario'
Then I see that button 'Delete row' text equals "Delete 2 Rows" on popup 'New Scenario'
When I click button 'Delete row' on popup 'New Scenario'
Then I see that example table has "1" rows on popup 'New Scenario'