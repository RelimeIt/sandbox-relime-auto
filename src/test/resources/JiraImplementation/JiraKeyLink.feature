@ST_1758 @REL-1273
Feature: As a DO I want to see Jira-key link under the navigation tree for Jira-linked stories.

Background: 

Given I am signed in as "DO"
And I navigate to page "Dashboard"

@SC_6816
Scenario: Jira-story must be opened in a new tab by on click link 'Jira-key'
When I navigate to page "Editor" for project "public"
And I create Jira-linked feature "default"
Then I see that new jira-linked feature is successfully created
And I see that Jira Key link URL matches feature "default"