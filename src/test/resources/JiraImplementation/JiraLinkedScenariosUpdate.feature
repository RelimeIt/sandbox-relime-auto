@ST_1760 @REL-1287
Feature: As a DO I want to update each scenario of the jira-linked stories from Action menu

Background: 

Given I am signed in as "DO"
And I navigate to page "Editor" for project "public"
And I update all features from Git on Editor page

@SC_6825
Scenario: Menu item 'Update from Jira' is enabled when scenario is 'Not Jira synced'
When I create Jira-linked feature "default"
And I create scenario in page 'Editor'
|scenarioName:scenarioName|description:scenarioDescription|tag:tag1|
|Given a|||
|And b|||
Then I can see that option "Update from Jira" is enabled in drop-down 'Actions' in scenario accordion

@SC_6826
Scenario: Menu item 'Update from Jira' is disabled when scenario is 'Jira synced'
When I create Jira-linked feature "default"
And I select "5" scenario in the table
Then I can see that option "Update from Jira" is not enabled in drop-down 'Actions' in scenario accordion

@SC_6827
Scenario: Menu item 'Update from Jira' is disabled for not Jira-linked feature
And I create feature in page 'Editor'
|featureName:featureName_1|fileName:fileName_1|description:test|tag:tag1|tag:tag2|
And I create scenario in page 'Editor'
|scenarioName:scenarioName|description:scenarioDescription|tag:tag1|
|Given a|||
|And b|||
And I can see that option "Update from Jira" is not enabled in drop-down 'Actions' in scenario accordion