@ST_1755 @REL-2575
Feature: As a DO I want to  export stories to Jira

Background: 

Given I am signed in as "DO"
When I navigate to page "Editor" for project "public"

@SC_6807
Scenario: Export all jira-linked stories to jira
Then I export all Jira-linked projects to Jira on Editor page
And I export "2" Jira-linked projects to Jira on Editor page
And I export "1" Jira-linked projects to Jira on Editor page