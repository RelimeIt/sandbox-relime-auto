@ST_1756 @JiraEditorPage
Feature: Jira editor page

Background: 

Given I am signed in as "DO"

@SC_6809 @REL-794
Scenario: Check that Jira linked story can be created
When I create project in page 'Dashboard'
|projectName:project_name1|projectKey:random|description:test|projectType:private|
And I see that project is added to panel 'Projects'
And I select project in panel 'Projects' in page "Dashboard"
And I change drop-down 'Saving mode' value to "requiring confirmation" in page 'Project settings'
When I change Git path to "smoke"
Then I see that Git "smoke" is added in page 'Project settings'
And I see success notification ""
And I change Jira path to "https://jira.nixsolutions.com/"
When I selected menu "default" in dropdown 'Account' at block VCS
When I update project from Git on Settings page
And I tie "default" account in TTS settings
And I see success notification ""
Then I see that "https://jira.nixsolutions.com/" is added to setting 'Task tracking system' in page 'Project settings'
And I see that entered data matches data in setting 'Task tracking system' in page 'Project settings' for Jira
And I navigate to page "Editor"
And I create Jira-linked feature "default"
Then I see that new jira-linked feature is successfully created
And I see that data from linked Jira issue matches data in new feature in feature info block under the tree

@SC_6810 @ignore @REL-795
Scenario: Check that Jira linked story can't be created without valid Jira path and account
And I navigate to page "Dashboard"
When I select project "public" in panel 'Projects' in page "Dashboard"
When I click on button 'Load from Git' on page 'Settings'
When I selected menu "wrong" in dropdown 'Account' at block TTS
Then I see "Task tracker is not accessible with this account" warning under TTS account field
When I navigate to page "Editor"
And I open pop-up 'New feature' in panel 'Features'
Then I see that checkbox 'Jira linked' is disabled
And I see that checkbox 'Jira linked' tooltip equals "Set up Jira at Project settings page"

@SC_6811 @REL-796
Scenario: Check that Jira linked story can be changed by use
#    And I selected menu "default" in dropdown 'Account' at block TTS
#    Then I see success notification ""
And I navigate to page "Dashboard"
When I select project "public" in panel 'Projects' in page "Dashboard"
When I Load from Git on page 'Settings'
When I navigate to page "Editor"
And I create Jira-linked feature "default"
Then I see that new jira-linked feature is successfully created
And I see that data from linked Jira issue matches data in new feature in feature info block under the tree
When I select feature "default" in the tree
When I change textbox 'Feature' value to "featureName_1_1" in feature info block under the tree
And I save edited field
Then I see success notification ""
And I see that changes are successfully saved in textbox 'Feature' in feature info block under the tree
When I change textbox 'File name' value to "fileName_1_1" in feature info block under the tree
And I save for edited field
Then I see success notification ""
And I see that changes are successfully saved in textbox 'File name' in feature info block under the tree
When I change textbox 'Description' value to "test1" in feature info block under the tree
And I lose focus
Then I see success notification ""
And I see that textbox 'Description' text equals "test1" in feature info block under the tree