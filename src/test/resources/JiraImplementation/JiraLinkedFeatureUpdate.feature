@ST_40589 @re
Feature: As a DO I want to update on click by Jira-linked story in the navigation tree in direct mode

Background: 

Given I am signed in as "DO"
And I navigate to page "Editor" for project "public"
And I update all features from Git on Editor page
And I create Jira-linked feature "default"
Then I see that new jira-linked feature is successfully created

@SC_153850
Scenario: Icon 'Jira synced' is displayed for each scenario of Jira-linked feature
And I can see that all scenarios are Jira-synced in the table
And I see that selected Feature(s) is synced with Jira

@SC_153849
Scenario: Feature name, tags and description are updated from Jira when you click on Jira-linked feature in the navigation tree
Then I see that new jira-linked feature is successfully created
And I see that data from linked Jira issue matches data in new feature in feature info block under the tree

@SC_153851
Scenario: Icon 'Jira synced' has two states "Jira synced" or "Not Jira synced"
When I create scenario in page 'Editor'
|scenarioName:scenarioName[random]|description:scenarioDescription|tag:tag1|
|Given a|||
|And b|||
Then I see that the new scenario is expanded
And I can see that scenario "current" is not Jira-synced in the table
And I export all Jira-linked projects to Jira on Editor page
And I can see that scenario "current" is Jira-synced in the table

@SC_153852
Scenario: "Jira synced"  status becomes "Not Jira synced" when user starts to editing scenario
When I select "5" scenario in the table
And I change textbox 'Description' value to "scenarioDescription3" in scenario accordion
Then I can see that scenario "5" is not Jira-synced in the table

@SC_153853
Scenario: Success notifications for updating from Jira by clicking in navigation tree
When I select feature "search_book" in the tree
And I select feature "jira" in the tree
Then I see info notification "Updating from Jira"
And I see success notification "The feature has been updated from Jira"

@SC_153854
Scenario: Error notifications for updating from Jira by clicking in navigation tree
When I navigate to page "Project Settings"
And I change Jira path to "wrong"
And I navigate to page "Editor"
And I update all projects from Jira on Editor page
Then I see info notification "Updating from Jira"
And I see error notification ""