@ST_1757 @REL-1566
Feature: As a DO I want to see autocomplete to the Jira-key input of the New feature pop-up when I am creating jira-linked feature

Background: 

Given I am signed in as "DO"
And I navigate to page "Editor" for project "public"
And I update all features from Git on Editor page

@SC_6813
Scenario: A user is able to select a Jira-feature among the ones, that are available for current Jira user
When I open pop-up 'New feature' in panel 'Features'
And I check 'Jira-linked' in pop-up 'New feature'
And I type "REL" into textbox 'Jira-key' in pop-up 'New feature'
Then I can see that autocomplete containing "REL" appeared for textbox 'Jira-key' in pop-up 'New feature'

@SC_6814
Scenario: Error message 'Jira-key is not valid' is displayed when not valid Jira key is entered
When I create Jira-linked feature "default"
And I open pop-up 'New feature' in panel 'Features'
And I check 'Jira-linked' in pop-up 'New feature'
And I type "default" into textbox 'Jira-key' in pop-up 'New feature'
Then I see a notification message "" under "1" required fields on pop-up "New Feature"