package stepdefinition.JiraImplementation;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.CommonHelper;
import helpers.SystemHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;
import pages.relime.EditorPage;
import pages.relime.GitHubPage;
import pages.relime.SettingsPage;

import java.util.regex.Pattern;

/**
 * Created by kozlov on 7/7/2016.
 */
public class JiraEditorPageImplementation extends PageInstance {

    @Autowired
    EditorPage editorPage;

    @Then("^I see that button 'Upload from Jira' is disabled")
    public void iSeeThatButtonUploadFromJiraIsDisabled() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Button 'Upload from Jira' is disabled.", editorPage.updateFromJira.getAttribute("class").contains("disabled"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that button 'Upload from Jira' is enabled")
    public void iSeeThatButtonUploadFromJiraIsEnabled() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Button 'Upload from Jira' is enabled.", !editorPage.updateFromJira.getAttribute("class").contains("disabled"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that checkbox 'Jira linked' is disabled")
    public void iSeeThatCheckboxJiraLinkedIsDisabled() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Chackbox 'Jira linked' is disabled.", editorPage.jiraLinkedFeatureCheckbox.getAttribute("disabled")!=null);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that checkbox 'Jira linked' is enabled")
    public void iSeeThatCheckboxJiraLinkedIsEnabled() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Chackbox 'Jira linked is enabled.", editorPage.jiraLinkedFeatureCheckbox.getAttribute("disabled")==null);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that Jira Key link URL matches feature \"([^\"]*)\"")
    public void iSeeThatJiraKeyLinkIsCorrect(String feature) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(feature.equals("smoke")){
                feature = SystemHelper.SMOKEJIRALINKEDFEATURE;
            }
            if(feature.equals("default")){
                feature = SystemHelper.DEFAULTJIRALINKEDFEATURE;
            }
            Waiters.waitAppearanceOf(5, editorPage.jiraKeyLink.getWrappedElement());
            String url = editorPage.jiraKeyLink.getAttribute("href");
            ReportService.reportAction("Jira Key link url is correct.", url.equals("https://jira.nixsolutions.com//browse/" + feature));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that checkbox 'Jira linked' tooltip equals \"([^\"]*)\"")
    public void iSeeThatCheckboxJiraLinkedTooltipEquals(String tooltip) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            String elementTooltip = editorPage.jiraLinkedFeatureCheckbox.findElement(By.xpath("./parent::label")).getAttribute("data-uib-tooltip");
            ReportService.reportAction("Chackbox 'Jira linked is enabled.", elementTooltip.toLowerCase().equals(tooltip.toLowerCase()));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I click checkbox 'Jira-linked' in pop-up 'New feature'$")
    public void iClickCheckboxJiraLinkedInPopUpNewFeature() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.checkJiraLinkedFeatureCheckbox();
            ReportService.reportAction("'Jira-linked feature' checkbox was clicked", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I create Jira-linked feature \"([^\"]*)\"$")
    public void iCreateJiraLinkedFeature(String feature) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("'New Feature' popup window is opened.", editorPage.openPopUpNewFeature());
            editorPage.checkJiraLinkedFeatureCheckbox();
            ReportService.reportAction("'Jira-linked feature' checkbox was clicked", true);
            if(feature.equals("smoke")){
                feature = SystemHelper.SMOKEJIRALINKEDFEATURE;
            }
            if(feature.equals("default")){
                feature = SystemHelper.DEFAULTJIRALINKEDFEATURE;
            }
            SystemHelper.waitAppearanceByElement(driver, 30, 100, editorPage.jiraKeyNameInput.getWrappedElement());
            editorPage.jiraKeyNameInput.enterText(feature);
            editorPage.newFeatureLabel.click();
            Thread.sleep(500);
            ReportService.reportAction("Data was loaded successfully.", editorPage.verifyCreateJiraLinkedFeaturePopup());
            CommonHelper.testJiraLinkedFeatureFilename = feature;
            CommonHelper.testJiraLinkedFeatureName = driver.findElement(By.xpath(".//form//input[@id='name']")).getAttribute("value");
            editorPage.clickButtonOnPopUp("Create");
            Thread.sleep(1000);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I type \"([^\"]*)\" into textbox 'Jira-key' in pop-up 'New feature'$")
    public void iTypeIntoTextboxJiraKeyInPopUpNewFeature(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(arg0.equals("smoke")){
                arg0 = SystemHelper.SMOKEJIRALINKEDFEATURE;
            }
            if(arg0.equals("default")){
                arg0 = SystemHelper.DEFAULTJIRALINKEDFEATURE;
            }
            SystemHelper.waitAppearanceByElement(driver, 30, 100, editorPage.jiraKeyNameInput.getWrappedElement());
            editorPage.jiraKeyNameInput.enterText(arg0);
            Thread.sleep(500);
            CommonHelper.sendKeyWithActions(Keys.ENTER);
            ReportService.reportAction("Jira key was entered.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see that autocomplete containing \"([^\"]*)\" appeared for textbox 'Jira-key' in pop-up 'New feature'$")
    public void iCanSeeThatAutocompleteAppearedForJiraKey(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(2000);
            boolean correct = editorPage.jiraKeyAutocompleteLines.size() > 0;
            if(correct) {
                for (Element line : editorPage.jiraKeyAutocompleteLines) {
                    Pattern p = Pattern.compile("REL-([^\"]*) - ([^\"]*)");
                    if(!line.getText().contains(arg0)||!p.matcher(line.getText()).matches()){
                        correct = false;
                        break;
                    }
                }
            }
            ReportService.reportAction("Autocomplete appeared and is correct.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
