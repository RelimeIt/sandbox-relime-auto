package stepdefinition.JiraImplementation;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;
import pages.relime.EditorPage;
import pages.relime.GitHubPage;
import pages.relime.SettingsPage;

/**
 * Created by kozlov on 7/7/2016.
 */
public class JiraProjectSettingsPage extends PageInstance {

    @Autowired
    SettingsPage settingsPage;

    @Then("^I see \"([^\"]*)\" warning under TTS Path field")
    public void iSeeWarningUnderTTSPathField(String warning) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(10, settingsPage.jiraAddressWarning.getWrappedElement());
            if(settingsPage.jiraAddressWarning.isDisplayed()) {
                if(warning.equals("")){
                    ReportService.reportAction("Warning field appeared.", true);
                }
                else {
                    String s = settingsPage.jiraAddressWarning.getText().toLowerCase();
                    ReportService.reportAction("\"" + warning + "\" warning field appeared.", s.equals(warning.toLowerCase()));
                }
            }
            else {
                ReportService.reportAction("Warning field didn't appear.", false);
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
