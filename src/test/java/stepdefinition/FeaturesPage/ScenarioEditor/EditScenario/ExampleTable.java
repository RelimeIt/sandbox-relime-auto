package stepdefinition.FeaturesPage.ScenarioEditor.EditScenario;

import com.unitedsofthouse.ucucumberpackage.typesfactory.types.*;
import cucumber.api.java.en.And;
import arp.CucumberArpReport;
import arp.ReportService;
import helpers.CommonHelper;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.EditorPage;

/**
 * Created by kozlov on 9/30/2016.
 */
public class ExampleTable extends PageInstance{

    @Autowired
    EditorPage editorPage;

    @And("^I change example table column \"([^\"]*)\" row \"([1-9])\" to \"([^\"]*)\" on Editor page$")
    public void iPopulateValidDataInExampleTableDataCellsInPopUpNewScenario(String column, int row, String value) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            TextInput e = editorPage.getInputFieldForExampleOnPage(column, row);
            e.click();
            CommonHelper.sendKeysWithActions(e.getWrappedElement(), value);
            ReportService.reportAction("Example table cell column '" + column + "' row " + row + " value was changed to '" + value + "'.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that example table column \"([^\"]*)\" row \"([1-9])\" value equals \"([^\"]*)\" on Editor page$")
    public void iSeeThatExampleTableCellValueIsCorrectOnPopUpNewScenario(String column, int row, String value) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Label e = new Label(editorPage.getInputFieldForExampleOnPage(column, row).getWrappedElement());
            ReportService.reportAction("Example table cell column '" + column + "' row " + row + " value equals '" + value + "'.", e.getText().equals(value));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see that example table  \"([^\"]*)\" is present on Editor page$")
    public void iSeeThatExampleTableIsPresent(String name) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Example table header '" + name + "' is present", editorPage.exampleTableHeaderIsPresentOnPage(name));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I select row \"([1-9])\" in example table on Editor page$")
    public void iClickCheckboxForRowInExampleTable(int row) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.pageExamplesTableRows.get(row - 1).findElement(By.xpath(editorPage.FORM_EXAMPLE_TABLE_ROW_CHECKBOX)).click();
            ReportService.reportAction("Checkbox for row " + row + " was clicked on Editor page", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that example tab did not appear on Editor page$")
    public void iSeeThatExamplesTabDidNotAppearOnPopupNewScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Example tab did not appear.", !editorPage.examplesTable.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that example table has \"([1-9]*)\" rows on Editor page$")
    public void iSeeThatExamplesTableHasRowsOnPopupNewScenario(int number) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {

            ReportService.reportAction("Example table hes " + number + " rows.", editorPage.pageExamplesTableRows.size()==number);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that button 'Delete row' is disabled on Editor page$")
    public void iSeeThatButtonDeleteRowIsDisabledOnPopupNewScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Button 'Delete row' is disabled on Editor page.", !editorPage.pageDeleteExampleRowButton.isEnabled());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that button 'Delete row' is enabled on Editor page$")
    public void iSeeThatButtonDeleteRowIsEnabledOnPopupNewScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Button 'Delete row' is enabled on Editor page.", editorPage.pageDeleteExampleRowButton.isEnabled());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that button 'Delete row' text equals \"([^\"]*)\" on Editor page$")
    public void iSeeThatButtonDeleteRowIsEnabledOnPopupNewScenario(String text) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Button 'Delete row' text equals '" + text + "' on Editor page.", editorPage.pageDeleteExampleRowButton.getText().toLowerCase().equals(text.toLowerCase()));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click button 'Delete row' on Editor page$")
    public void iClickButtonDeleteRowOnPopupNewScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.pageDeleteExampleRowButton.click();
            ReportService.reportAction("Button 'Delete row' was clicked on Editor page.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that button 'Add row' is enabled on Editor page$")
    public void iSeeThatButtonAddRowIsEnabledOnPopupNewScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Button 'Add row' is enabled on Editor page.", editorPage.pageAddExampleRowButton.isEnabled());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I add row to example table on Editor page$")
    public void iClickButtonAddRowOnPopupNewScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.pageAddExampleRowButton.click();
            ReportService.reportAction("Button 'Add row' was clicked on Editor page.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that all the example tab elements appeared on Editor page$")
    public void iSeeThatAllTheExamplesTabElementsAppearedOnPopupNewScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Example table appeared.", editorPage.examplesTable.isDisplayed());
            //ReportService.reportAction("Example table header appeared.", editorPage.pageExamplesTableHeader.isDisplayed());
            //ReportService.reportAction("Example table description appeared.", editorPage.pageExamplesTableDescription.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
