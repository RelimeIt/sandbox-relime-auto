package stepdefinition.FeaturesPage.ScenarioEditor.EditScenario;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Label;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.CommonHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;
import pages.relime.EditorPage;

import java.awt.event.KeyEvent;
import java.util.List;

/**
 * Created by kozlov on 7/26/2016.
 */
public class ScenatioTagsEditing extends PageInstance {
    @Autowired
    DashboardPage dashboardPage;

    @Autowired
    EditorPage editorPage;

    @When("^I add tag \"([^\"]*)\" in scenario info block via comma")
    public void iAddTagViaComma(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.addTagViaCommaForScenario(arg0);
            ReportService.reportAction("Tag '" + arg0 + "' was added.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I add tag \"([^\"]*)\" in scenario info block via Enter$")
    public void iAddTagViaEnter(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.addTagViaEnterForScenario(arg0);
            ReportService.reportAction("Tag '" + arg0 + "' was added.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I type \"([^\"]*)\" into tags input textbox in scenario info block")
    public void iTypeTotagsInput(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.scenarioTagsInput.clear();
            editorPage.scenarioTagsInput.sendKeys(arg0);
            ReportService.reportAction("Tag '" + arg0 + "' was added.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I verify invalid tags for scenario$")
    public void iVerifyInvalidTags(DataTable tags) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            for (List<String> line : tags.raw()) {
                boolean messageDisplayed = false;
                editorPage.addTagViaEnterForScenario(line.get(0));
                Thread.sleep(500);
                for (Label e : dashboardPage.EditorFieldWarnings) {
                    if (e.isDisplayed() && e.getText().equals(line.get(1))){
                        messageDisplayed = true;
                        break;
                    }
                }
                ReportService.reportAction("Error '" + line.get(1) + "' appeared for tag '" + line.get(0) + "' when entered via Enter.", messageDisplayed);
                messageDisplayed = false;
                editorPage.addTagViaCommaForScenario(line.get(0));
                Thread.sleep(500);
                for (Label e : dashboardPage.EditorFieldWarnings) {
                    if (e.isDisplayed() && e.getText().equals(line.get(1))){
                        messageDisplayed = true;
                        break;
                    }
                }
                ReportService.reportAction("Error '" + line.get(1) + "' appeared for tag '" + line.get(0) + "' when entered via comma.", messageDisplayed);
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that tag \"([^\"]*)\" is absent in scenario info$")
    public void iSeeThatTagIsAbsentOnPopupNewFeature(String tag) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean contains = false;
            for (Element e :editorPage.scenarioAddedTags) {
                if(e.findElement(By.xpath(".//span")).getText().equals(tag)){
                    contains = true;
                    break;
                }
            }
            ReportService.reportAction("Tag '" + tag + "' is absent for given scenario.", !contains);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that tag \"([^\"]*)\" is added to scenario info$")
    public void iSeeThatTagIsAddedOnPopupNewFeature(String tag) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean contains = false;
            for (Element e :editorPage.scenarioAddedTags) {
                if(e.findElement(By.xpath(".//span")).getText().equals(tag)){
                    contains = true;
                    break;
                }
            }
            ReportService.reportAction("Tag '" + tag + "' is added to scenario info.", contains);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click button 'Backspace' for scenario tags input$")
    public void iClickButtonBackspaceForScenarioTags() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            try{
                editorPage.scenarioTagsInput.sendKeys(Keys.BACK_SPACE);
                Thread.sleep(500);
            }
            catch (Exception ex) {
                CommonHelper.sendKeyWithAWT(KeyEvent.VK_BACK_SPACE);
                ReportService.reportAction("'Backspace' button was pressed.", true);
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I click button 'Delete' for tag \"([^\"]*)\" in scenario info$")
    public void iClickButtonDeleteForScenarioTag(String tag) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean contains = false;
            for (Element e :editorPage.scenarioAddedTags) {
                if(e.findElement(By.xpath(".//span")).getText().equals(tag)){
                    e.findElement(By.xpath(".//a")).click();
                    contains = true;
                    break;
                }
            }
            //ReportService.reportAction("Tag '" + tag + "' is added on pop-up.", contains);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
