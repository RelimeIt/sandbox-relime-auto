package stepdefinition.FeaturesPage.ScenarioEditor;

import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.*;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.CommonHelper;
import helpers.SystemHelper;
import arp.CucumberArpReport;
import arp.ReportService;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.EditorPage;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class NewScenarioPopup extends PageInstance{

    @Autowired
    EditorPage editorPage;

    @And("^I open pop-up 'New Scenario' in page Editor$")
    public void iClickButtonNewScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Pop-up 'New scenario' was opened.", editorPage.openPopUpNewScenario());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I change example table column \"([^\"]*)\" row \"([1-9])\" to \"([^\"]*)\" on pop-up 'New Scenario'$")
    public void iPopulateValidDataInExampleTableDataCellsInPopUpNewScenario(String column, int row, String value) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            TextInput e = editorPage.getInputFieldForExampleOnForm(column, row);
            e.click();
            CommonHelper.sendKeysWithActions(e.getWrappedElement(), value);
            ReportService.reportAction("Example table cell column '" + column + "' row " + row + " value was changed to '" + value + "'.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that example table column \"([^\"]*)\" row \"([1-9])\" value equals \"([^\"]*)\" on pop-up 'New Scenario'$")
    public void iSeeThatExampleTableCellValueIsCorrectOnPopUpNewScenario(String column, int row, String value) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Label e = new Label(editorPage.getInputFieldForExampleOnForm(column, row).getWrappedElement());
            ReportService.reportAction("Example table cell column '" + column + "' row " + row + " value equals '" + value + "'.", e.getText().equals(value));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see that example table  \"([^\"]*)\" is present on pop-up 'New Scenario'$")
    public void iSeeThatExampleTableIsPresent(String name) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Example table header '" + name + "' is present", editorPage.exampleTableHeaderIsPresentOnForm(name));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I select row \"([1-9])\" in example table on pop-up 'New Scenario'$")
    public void iClickCheckboxForRowInExampleTable(int row) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.formExamplesTableRows.get(row - 1).findElement(By.xpath(editorPage.FORM_EXAMPLE_TABLE_ROW_CHECKBOX)).click();
            ReportService.reportAction("Checkbox for row " + row + " was clicked on pop-up 'New Scenario'", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that example tab did not appear on popup 'New Scenario'$")
    public void iSeeThatExamplesTabDidNotAppearOnPopupNewScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Example tab did not appear.", !editorPage.examplesTable.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that example table has \"([1-9]*)\" rows on popup 'New Scenario'$")
    public void iSeeThatExamplesTableHasRowsOnPopupNewScenario(int number) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {

            ReportService.reportAction("Example table hes " + number + " rows.", editorPage.formExamplesTableRows.size()==number);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that button 'Delete row' is disabled on popup 'New Scenario'$")
    public void iSeeThatButtonDeleteRowIsDisabledOnPopupNewScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Button 'Delete row' is disabled on popup 'New Scenario'.", !editorPage.formDeleteExampleRowButton.isEnabled());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that button 'Delete row' is enabled on popup 'New Scenario'$")
    public void iSeeThatButtonDeleteRowIsEnabledOnPopupNewScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Button 'Delete row' is enabled on popup 'New Scenario'.", editorPage.formDeleteExampleRowButton.isEnabled());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that button 'Delete row' text equals \"([^\"]*)\" on popup 'New Scenario'$")
    public void iSeeThatButtonDeleteRowIsEnabledOnPopupNewScenario(String text) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Button 'Delete row' text equals '" + text + "' on popup 'New Scenario'.", editorPage.formDeleteExampleRowButton.getText().toLowerCase().equals(text.toLowerCase()));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click button 'Delete row' on popup 'New Scenario'$")
    public void iClickButtonDeleteRowOnPopupNewScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.formDeleteExampleRowButton.click();
            ReportService.reportAction("Button 'Delete row' was clicked on popup 'New Scenario'.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that button 'Add row' is enabled on popup 'New Scenario'$")
    public void iSeeThatButtonAddRowIsEnabledOnPopupNewScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Button 'Add row' is enabled on popup 'New Scenario'.", editorPage.formAddExampleRowButton.isEnabled());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I add row to example table on popup 'New Scenario'$")
    public void iClickButtonAddRowOnPopupNewScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.formAddExampleRowButton.click();
            ReportService.reportAction("Button 'Add row' was clicked on popup 'New Scenario'.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that all the example tab elements appeared on popup 'New Scenario'$")
    public void iSeeThatAllTheExamplesTabElementsAppearedOnPopupNewScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Example table appeared.", editorPage.formExamplesTable.isDisplayed());
            //ReportService.reportAction("Example table header appeared.", editorPage.fromExamplesTableHeader.isDisplayed());
            ReportService.reportAction("Example table description appeared.", editorPage.formExamplesTableDescription.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that new scenario is created$")
    public void iSeeThatNewScenarioIsCreated() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            SystemHelper.waitAppearanceByXpath(driver, 100, 30, ".//div[contains(@class, 'ace_line')][contains(text(), ' another step')]");
            ReportService.reportAction("New scenario was created.", driver.findElement(By.xpath(".//div[contains(@class, 'ace_line')][contains(text(), ' another step')]")).isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that all the autocomplete lines contain word \"([^\"]*)\" in pop-up 'New Scenario'$")
    public void iSeeThatAutocompleteContainsWord(String word) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean contains = true;
            for (Element e : editorPage.formStepAutocompleteLines) {
                if(e.isDisplayed()) {
                    String line = e.getText().toLowerCase();
                    if (!line.toLowerCase().contains(word.toLowerCase())) {
                        contains = false;
                        break;
                    }
                }
            }
            ReportService.reportAction("All the steps in autocomplete contain '" + word + "'",  contains);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that autocomplete line \"([^\"]*)\" is marked as \"([^\"]*)\" in pop-up 'New Scenario'$")
    public void iSeeThatAutocompleteLineIsMarkedAs(String line, String mark) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(3, editorPage.formStepAutocomplete.getWrappedElement());
            boolean marked = false;
            for (Element e : editorPage.formStepAutocompleteLines) {
                String s = e.getText();
                //String value = e.getText().substring(0, e.getText().indexOf("/n"));
                //String actualMark = e.getText().substring(e.getText().indexOf("/n"));
                if(e.isDisplayed()) {
                    if (e.getText().startsWith(line)&&e.getText().endsWith(mark)) {
                        marked = true;
                        break;
                    }
                }
            }
            ReportService.reportAction("'" + line + "' line in autocomplete is marked as '" + mark + "'",  marked);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I select step \"([^\"]*)\" in autocomplete in pop-up 'New Scenario'$")
    public void iSelectStepInAutocomplete(String word) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(3, editorPage.formStepAutocomplete.getWrappedElement());
            for (Element e : editorPage.formStepAutocompleteLines) {
                if(e.isDisplayed()) {
                    String line = e.getText();
                    if (line.startsWith(word)) {
                        CommonHelper.clickWithActions(e.getWrappedElement());
                        //e.click();
                        break;
                    }
                }
            }
            //ReportService.reportAction("All the steps in autocomplete contain '" + word + "'",  contains);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that \"([0-9]{1,2})\" line equals \"([^\"]*)\" in Ace Editor in pop-up 'New Scenario'$")
    public void iSeeThatLineEqualsInAceEditorInPopup(int line, String text) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(text.equals("comment"))
                text = "\"\"\"";
            ReportService.reportAction("Line " + line + "equals '" + text + "' in Ace Editor.",  editorPage.formStepLines.get(--line).getText().equals(text));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that \"([0-9]{1,2})\" line is marked as comment in Ace Editor in pop-up 'New Scenario'$")
    public void iSeeThatLineIsMarkedAsCommentInAceEditorInPopup(int line) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Label step = new Label(editorPage.formStepLines.get(--line).findElement(By.xpath(".//span")));
            ReportService.reportAction("Line " + line + " is marked as comment in Ace Editor.",  step.getAttribute("class").equals("ace_comment"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that created scenario contains no empty lines$")
    public void iSeeThatCreatedScenarioContainsNoEmptyLines() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean noEmptyLines = true;
            for (Element e : editorPage.tableStepLines) {
                if(e.getText().equals("")){
                    noEmptyLines = false;
                    break;
                }
            }
            ReportService.reportAction("Scenario contains no empty lines.",  noEmptyLines);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that step \"([1-9])\" is marked red in Ace Editor in pop-up 'New Scenario'$")
    public void iSeeThatStepIsMarkedRedInAceEditor(int step) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Step " + step + "equals is marked red in Ace Editor.",  editorPage.formStepStatusLabels.get(--step).getAttribute("class").contains("btn-danger"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that step \"([1-9])\" is marked green in Ace Editor in pop-up 'New Scenario'$")
    public void iSeeThatStepIsMarkedGreenInAceEditor(int step) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Step " + step + "equals is marked green in Ace Editor.",  editorPage.formStepStatusLabels.get(--step).getAttribute("class").contains("btn-success"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }


    @Then("^I see that step \"([1-9])\" is not marked in Ace Editor in pop-up 'New Scenario'$")
    public void iSeeThatStepIsNotMarkedInAceEditor(int step) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Step " + step + "equals is marked green in Ace Editor.",  editorPage.formStepStatusLabels.get(--step).getAttribute("class").equals("ace_gutter-cell "));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that autocomplete is displayed in pop-up 'New Scenario'$")
    public void iSeeThatAutocompleteIsDisplayed() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(3, editorPage.formStepAutocomplete.getWrappedElement());
            ReportService.reportAction("Autocomplete is displayed.'",  editorPage.formStepAutocomplete.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that autocomplete contains no keywords in pop-up 'New Scenario'$")
    public void iSeeThatAutocompleteContainsNoKeywords() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean contains = false;
            for (Element e : editorPage.formStepAutocompleteLines) {
                if(e.getText().startsWith("Given")
                        ||e.getText().startsWith("And")
                        ||e.getText().startsWith("When")
                        ||e.getText().startsWith("Then")){
                    contains = true;
                    break;
                }
            }
            ReportService.reportAction("All the steps in autocomplete contain no keywords.",  !contains);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that autocomplete contains no duplicates in pop-up 'New Scenario'$")
    public void iSeeThatAutocompleteContainsNoDuplicates() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean contains = false;
            for (Element e : editorPage.formStepAutocompleteLines) {
                int quantity = 0;
                String s1 = e.getText();
                for (Element el : editorPage.formStepAutocompleteLines) {
                    String s2 = el.getText();
                    if(e.getText().equals(el.getText())){
                        quantity++;
                    }
                }
                if(quantity>1){
                    contains = true;
                    break;
                }
            }
            ReportService.reportAction("All the steps in autocomplete contain no dulicates.",  !contains);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that checkbox 'background' has tooltip \"This story already has a background\" in pop-up 'New Scenario'$")
    public void iSeeThatCheckboxBackgroungHasTooltip() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Background chackbox has tooltip 'This story already has a background'.",  editorPage.backgroundOnNewScenarioPopupCheckbox.getAttribute("data-uib-popover").equals("This story already has a background"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that checkbox 'background' is disabled in pop-up 'New Scenario'$")
    public void iSeeThatCheckboxBackgroundIsDisabled() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("All the steps in autocomplete contain no dulicates.",  !editorPage.backgroundOnNewScenarioPopupCheckbox.findElement(By.xpath("./input")).isEnabled());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that numeric and literal regular expressions are replaced with placeholders in proposed steplines in autocomplete list in pop-up 'New Scenario'$")
    public void iSeeThatNumericAndLiteralExpressionsAreReplacedWithPlaceholders() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean contains = true;
            for (Element e : editorPage.formStepAutocompleteLines) {
                if(e.getText().contains("\"[")){
                    if(!e.getText().contains("string")) {
                        contains = false;
                        break;
                    }
                }
                else if(e.getText().contains("[")){
                    if(!e.getText().contains("number")) {
                        contains = false;
                        break;
                    }
                }
            }
            ReportService.reportAction("All numeric and literal regular expressions are replaced with '[number]' and '\"[string]\"'  in autocomplete.",  contains);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that autocomplete types order is correct in pop-up 'New Scenario'$")
    public void iSeeThatAutocompleteTypesOrderIsCorrect() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(3, editorPage.formStepAutocomplete.getWrappedElement());
            boolean orderIsRight = true;
            String current = "local";
            for (Element e : editorPage.formStepAutocompleteLines) {
                if(!e.getText().endsWith(current)){
                    if(current.equals("local")){
                        if(e.getText().endsWith("Implemented")){
                            current = "Implemented";
                        }
                        else if(e.getText().endsWith("Not-implemented")){
                            current = "Not-implemented";
                        }
                        else{
                            orderIsRight = false;
                            break;
                        }
                    }
                    else if(current.equals("Implemented")){
                        if(e.getText().endsWith("local")){
                            orderIsRight = false;
                            break;
                        }
                        else if(e.getText().endsWith("Not-implemented")){
                            current = "Not-implemented";
                        }
                        else{
                            orderIsRight = false;
                            break;
                        }
                    }
                    else if(current.equals("Not-implemented")){
                        if(e.getText().endsWith("local")){
                            orderIsRight = false;
                            break;
                        }
                        else if(e.getText().endsWith("Implemented")){
                            orderIsRight = false;
                            break;
                        }
                        else{
                            orderIsRight = false;
                            break;
                        }
                    }
                }
            }
            ReportService.reportAction("All the steps in autocomplete are in right order by type.",  orderIsRight);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click button 'Backspace' for steps input on Pop Up$")
    public void iClickButtonBackspaceFoSteps() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            try{
                TextInput t = new TextInput(editorPage.formStepsTextarea.findElement(By.xpath("./textarea")));
                t.sendKeys(Keys.BACK_SPACE);
            }
            catch (Exception ex) {
                CommonHelper.sendKeyWithAWT(KeyEvent.VK_BACK_SPACE);
                ReportService.reportAction("'Backspace' button was pressed.", true);
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I create scenario in page 'Editor'$")
    public void iCreateScenario(DataTable table) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("'New Scenario' popup window is opened.", editorPage.openPopUpNewScenario());
            editorPage.fillInDataForNewScenarioPopUp(table);
            editorPage.clickButtonOnPopUp("Create");
            Thread.sleep(2000);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I fill in data in pop-up 'New Scenario'$")
    public void iFillInScenario(DataTable table) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.fillInDataForNewScenarioPopUp(table);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I create backgroung in page 'Editor'$")
    public void iCreateBackground(DataTable table) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("'New Scenario' popup window is opened.", editorPage.openPopUpNewScenario());
            editorPage.backgroundOnNewScenarioPopupCheckbox.click();
            editorPage.fillInDataForNewScenarioPopUp(table);
            editorPage.clickButtonOnPopUp("Create");
            Thread.sleep(3000);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}