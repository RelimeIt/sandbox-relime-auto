package stepdefinition.FeaturesPage.FeaturesPanel;

import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.CommonHelper;
import arp.CucumberArpReport;
import arp.ReportService;
import org.openqa.selenium.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;
import pages.relime.EditorPage;

import java.awt.event.KeyEvent;

public class CreateNewFolder extends PageInstance{

    @Autowired
    EditorPage editorPage;

    @When("^I type \"([^\"]*)\" into textbox 'Name' in pop-up 'New folder'$")
    public void iTypeIntoTextboxNameInPopUpNewFolder(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            CommonHelper.testFolderName = arg0;
            editorPage.folderNameInput.enterText(arg0);
            ReportService.reportAction("'" + arg0 + "' is set to the field 'Name' in pop-up 'New folder'.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that new folder is successfully created$")
    public void iSeeThatNewFolderIsSuccessfullyCreated() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitWhileConditionIsTrue(CommonHelper.delay, editorPage.firstTreeNode.isDisplayed());
            ReportService.reportAction("Folder was created.", editorPage.treeElementExists(CommonHelper.testFolderName));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that the new folder is selected in the tree$")
    public void iSeeThatTheNewFolderIsSelectedInTheTree() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Folder was selected.", editorPage.verifyIfTreeElementIsSelected(CommonHelper.testFolderName));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I open pop-up 'New folder' in panel 'Features'$")
    public void iOpenPopUpNewFolder() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("'New folder' popup window is opened.", editorPage.openPopUpNewFolder());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I create folder \"([^\"]*)\"$")
    public void iCreateFolder(String folder) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("'New folder' popup window is opened.", editorPage.openPopUpNewFolder());
            editorPage.folderNameInput.enterText(folder);
            ReportService.reportAction("'" + folder + "' is set to the field 'Name' in pop-up 'New folder'.", true);
            editorPage.clickButtonOnPopUp("Create");
            ReportService.reportAction("'Create' button clicked in pop-up 'New folder'.", true);
            CommonHelper.testFolderName = folder;
            Thread.sleep(1000);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}