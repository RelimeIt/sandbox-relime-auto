package stepdefinition.FeaturesPage.FeaturesPanel;

import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Label;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.CommonHelper;
import arp.CucumberArpReport;
import arp.ReportService;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;
import pages.relime.EditorPage;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class EditFeature extends PageInstance{
    @Autowired
    DashboardPage dashboardPage;

    @Autowired
    EditorPage editorPage;

    @Then("^I see that tag \"([^\"]*)\" is added to feature info under the tree$")
    public void iSeeThatTagIsAddedOnPopupNewFeature(String tag) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean contains = false;
            for (Element e :editorPage.featureAddedTags) {
                if(e.findElement(By.xpath(".//span")).getText().equals(tag)){
                    contains = true;
                    break;
                }
            }
            ReportService.reportAction("Tag '" + tag + "' is added on pop-up.", contains);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that tag \"([^\"]*)\" is absent on feature info under the tree$")
    public void iSeeThatTagIsAbsentOnPopupNewFeature(String tag) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean contains = false;
            for (Element e :editorPage.featureAddedTags) {
                if(e.findElement(By.xpath(".//span")).getText().equals(tag)){
                    contains = true;
                    break;
                }
            }
            ReportService.reportAction("Tag '" + tag + "' is absent under the tree.", !contains);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I click button 'Delete' for tag \"([^\"]*)\" under the tree$")
    public void iClickButtonDeleteForTag(String tag) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean contains = false;
            for (Element e :editorPage.featureAddedTags) {
                if(e.findElement(By.xpath(".//span")).getText().equals(tag)){
                    e.findElement(By.xpath(".//a")).click();
                    contains = true;
                    break;
                }
            }
            ReportService.reportAction("Tag '" + tag + "' is added on pop-up.", contains);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click button 'Backspace' for tags input under the tree$")
    public void iClickButtonBackspace() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            try{
                editorPage.featureTagsInput.sendKeys(Keys.BACK_SPACE);
                Thread.sleep(500);
            }
            catch (Exception ex) {
                CommonHelper.sendKeyWithAWT(KeyEvent.VK_BACK_SPACE);
                ReportService.reportAction("'Backspace' button was pressed.", true);
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I change textbox 'Feature' value to \"([^\"]*)\" in feature info block under the tree$")
    public void iChangeTextboxFeatureValueToInFeatureInfoBlockUnderTheTree(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(CommonHelper.delay, editorPage.changeFeatureNameBeforeFocusInput.getWrappedElement());
            editorPage.changeFeatureNameBeforeFocusInput.click();
            ReportService.reportAction("Feature name input was selected.", true);
            Waiters.waitAppearanceOf(CommonHelper.delay, editorPage.changeFieldAfterFocusInput.getWrappedElement());
            editorPage.changeFieldAfterFocusInput.clear();
            editorPage.changeFieldAfterFocusInput.sendKeys(arg0);
            CommonHelper.testFeatureName = arg0;
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that changes are successfully saved in textbox 'Feature' in feature info block under the tree$")
    public void iSeeThatChangesAreSuccessfullySavedInTextboxFeatureInFeatureInfoBlockUnderTheTree() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(CommonHelper.delay, editorPage.changeFeatureNameBeforeFocusInput.getWrappedElement());
            String s = editorPage.getChangeFeatureInputBeforeFocusText("feature");
            ReportService.reportAction("Changes in 'Feature' textbox were successfully saved.", editorPage.getChangeFeatureInputBeforeFocusText("feature").equals(CommonHelper.testFeatureName));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I change textbox 'File name' value to \"([^\"]*)\" in feature info block under the tree$")
    public void iChangeTextboxFileNameValueToInFeatureInfoBlockUnderTheTree(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(editorPage.changeFileNameBeforeFocusInput.isDisplayed()) {
                Waiters.waitAppearanceOf(CommonHelper.delay, editorPage.changeFileNameBeforeFocusInput.getWrappedElement());
                editorPage.changeFileNameBeforeFocusInput.click();
            }
            ReportService.reportAction("File name input was selected.", true);
            Waiters.waitAppearanceOf(CommonHelper.delay, editorPage.changeFieldAfterFocusInput.getWrappedElement());
            editorPage.changeFieldAfterFocusInput.clear();
            editorPage.changeFieldAfterFocusInput.sendKeys(arg0);
            CommonHelper.testFeatureFileName = arg0;
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that changes are successfully saved in textbox 'File name' in feature info block under the tree$")
    public void iSeeThatChangesAreSuccessfullySavedInTextboxFileNameInFeatureInfoBlockUnderTheTree() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(CommonHelper.delay, editorPage.changeFileNameBeforeFocusInput.getWrappedElement());
            ReportService.reportAction("Changes in 'File name' textbox were successfully saved.", editorPage.getChangeFeatureInputBeforeFocusText("file name").equals(CommonHelper.testFeatureFileName));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I change textbox 'Description' value to \"([^\"]*)\" in feature info block under the tree$")
    public void iChangeTextboxDescriptionValueToInFeatureInfoBlockUnderTheTree(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(CommonHelper.delay, editorPage.changeDescriptionBeforeFocusInput.getWrappedElement());
            editorPage.changeDescriptionBeforeFocusInput.click();
            ReportService.reportAction("Description input was selected.", true);
            editorPage.changeFeatureDescriptionAfterFocusInput.clear();
            editorPage.changeFeatureDescriptionAfterFocusInput.sendKeys(arg0);
            ReportService.reportAction("'" + arg0 + "' value was sent to description input.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I add text \"([^\"]*)\" to field 'Description' in feature info block under the tree$")
    public void iAddTextToFieldDescription(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(!editorPage.changeFeatureDescriptionAfterFocusInput.isDisplayed()) {
                Waiters.waitAppearanceOf(CommonHelper.delay, editorPage.changeDescriptionBeforeFocusInput.getWrappedElement());
                editorPage.changeDescriptionBeforeFocusInput.click();
                Waiters.waitAppearanceOf(CommonHelper.delay, editorPage.changeFeatureDescriptionAfterFocusInput.getWrappedElement());
            }
            editorPage.changeFeatureDescriptionAfterFocusInput.sendKeys(arg0);
            ReportService.reportAction("'" + arg0 + "' value was sent to description input.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that textbox 'Description' text equals \"([^\"]*)\" in feature info block under the tree$")
    public void iSeeThatChangesAreSuccessfullySavedInTextboxDescriptionInFeatureInfoBlockUnderTheTree(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(CommonHelper.delay, editorPage.changeDescriptionBeforeFocusInput.getWrappedElement());
            arg0 = arg0.replace("\\n","\n");
            ReportService.reportAction("Changes in 'File name' textbox were successfully saved.", editorPage.getChangeFeatureInputBeforeFocusText("description").equals(arg0));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }


    @When("^I type \"([^\"]*)\" into tags input textbox in feature info block under the tree$")
    public void iTypeIntoTagsInputTextboxInFeatureInfoBlockUnderTheTree(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(CommonHelper.testFeatureTags==null)
                CommonHelper.testFeatureTags = new ArrayList<>();
            editorPage.featureTagsInput.clear();
            editorPage.featureTagsInput.sendKeys(arg0);
            CommonHelper.testFeatureTags.add(arg0);
            ReportService.reportAction("'" + arg0 + "' value was sent to tags input.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I add tag \"([^\"]*)\" for feature via comma")
    public void iAddTagViaComma(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.addTagViaCommaForFeature(arg0);
            ReportService.reportAction("Tag '" + arg0 + "' was added.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I add tag \"([^\"]*)\" for feature via Enter$")
    public void iAddTagViaEnter(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.addTagViaEnterForFeature(arg0);
            ReportService.reportAction("Tag '" + arg0 + "' was added.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I verify invalid tags for feature")
    public void iVerifyInvalidTags(DataTable tags) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            for (List<String> line : tags.raw()) {
                boolean messageDisplayed = false;
                editorPage.addTagViaEnterForFeature(line.get(0));
                Thread.sleep(500);
                if (editorPage.inputNotificationMessage.isDisplayed() && editorPage.inputNotificationMessage.getText().equals(line.get(1))) {
                    messageDisplayed = true;
                }
                ReportService.reportAction("Error '" + line.get(1) + "' appeared for tag '" + line.get(0) + "' when entered via Enter.", messageDisplayed);
                messageDisplayed = false;
                editorPage.addTagViaCommaForFeature(line.get(0));
                Thread.sleep(500);
                if (editorPage.inputNotificationMessage.isDisplayed() && editorPage.inputNotificationMessage.getText().equals(line.get(1))) {
                    messageDisplayed = true;
                }
                ReportService.reportAction("Error '" + line.get(1) + "' appeared for tag '" + line.get(0) + "' when entered via comma.", messageDisplayed);
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}