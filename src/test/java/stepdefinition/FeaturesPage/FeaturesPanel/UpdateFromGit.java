package stepdefinition.FeaturesPage.FeaturesPanel;

import arp.CucumberArpReport;
import arp.ReportService;
import cucumber.api.java.en.*;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;
import pages.relime.EditorPage;

/**
 * Created by kozlov on 11/24/2016.
 */
public class UpdateFromGit extends PageInstance {
    @Autowired
    DashboardPage dashboardPage;

    @Autowired
    EditorPage editorPage;

    @Then("^I can see that button 'Upload from Git' is not displayed$")
    public void iCanSeeThatButtonUploadFromGitIsNotDisplayed() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("'Upload from Git' button is not displayed.", !editorPage.updateFromGit.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that button 'Upload from Jira' is not displayed$")
    public void iCanSeeThatButtonUploadFromJiraIsNotDisplayed() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("'Upload from Jira' button is not displayed.", !editorPage.updateFromJira.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see that option \"([^\"]*)\" is not enabled in drop-down 'Actions' in scenario accordion$")
    public void iSeeThatOptionIsNotEnabledUnderDropDownActionsInScenarioAccordion(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("'" + arg0 + "' option in 'Actions' is disabled.", editorPage.actionsMenuOptionIsNotEnabled(arg0));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see that option \"([^\"]*)\" is enabled in drop-down 'Actions' in scenario accordion$")
    public void iSeeThatOptionIsEnabledUnderDropDownActionsInScenarioAccordion(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("'" + arg0 + "' option in 'Actions' is enabled.", editorPage.actionsMenuOptionIsEnabled(arg0));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
