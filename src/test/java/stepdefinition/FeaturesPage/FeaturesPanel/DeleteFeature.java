package stepdefinition.FeaturesPage.FeaturesPanel;

import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.CommonHelper;
import arp.CucumberArpReport;
import arp.ReportService;
import org.openqa.selenium.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;
import pages.relime.EditorPage;

public class DeleteFeature extends PageInstance{

    @Autowired
    EditorPage editorPage;

    @When("^I click icon 'Delete' in panel 'Features'$")
    public void iClickIconDeleteInPanelFeatures() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(CommonHelper.delay, editorPage.deleteTreeNodeButton.getWrappedElement());
            editorPage.deleteTreeNodeButton.click();
            ReportService.reportAction("'Delete' in panel 'Features' was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that the feature was successfully deleted$")
    public void iSeeThatTheFeatureWasSuccessfullyDeleted() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitWhileConditionIsTrue(CommonHelper.delay, editorPage.firstTreeNode.isDisplayed());
            ReportService.reportAction("Folder was deleted.", !editorPage.treeElementExists(CommonHelper.testFeatureName));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I delete feature \"([^\"]*)\" in the tree$")
    public void iDeleteFolderInTheTree(String name) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.selectFeatureByName(name);
            Waiters.waitAppearanceOf(CommonHelper.delay, editorPage.deleteTreeNodeButton.getWrappedElement());
            editorPage.deleteTreeNodeButton.click();
            ReportService.reportAction("'Delete' in panel 'Features' was clicked.", true);
            editorPage.clickButtonOnPopUp("Delete");
            ReportService.reportAction("'Delete' button was clicked on pop-up.", true);
            Thread.sleep(1000);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}