package stepdefinition.Routing;

import arp.CucumberArpReport;
import arp.ReportService;
import com.sun.webkit.network.CookieManager;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.*;
import cucumber.api.java.en.Then;
import helpers.CommonHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.EditorPage;

/**
 * Created by kozlov on 6/30/2016.
 */
public class ScenarioURL extends PageInstance {

    @Autowired
    EditorPage editorPage;

    @Then("^I select \"([^\"]*)\" scenario in the table")
    public void iSelectScenarioInTheTable(String scenario) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(1000);
            Waiters.waitAppearanceOf(10, editorPage.firstScenario.getWrappedElement());
            boolean elementFound = false;
            for (Link l : editorPage.scenariosList) {
                String s = l.getText();
                if(l.getText().equals(scenario)){
                    l.click();
                    elementFound = true;
                    CommonHelper.testScenarioName = scenario;
                    break;
                }
            }
            ReportService.reportAction("'" + scenario + "' scenario was selected in the table.", elementFound);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that scenario \"([^\"]*)\" is Jira-synced in the table")
    public void iCanSeeThatScenarioIsJiraSynced(String scenario) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(1000);
            Waiters.waitAppearanceOf(10, editorPage.firstScenario.getWrappedElement());
            boolean synced = false;
            for (Link l : editorPage.scenariosList) {
                if(l.getText().equals(scenario)){
                    WebElement icon = l.findElement(By.xpath("./parent::div//li[contains(@class, 'status-link')][2]/i"));
                    synced = icon.getAttribute("class").contains("synced");
                    break;
                }
            }
            ReportService.reportAction("'" + scenario + "' scenario is Jira-synced.", synced);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that all scenarios are Jira-synced in the table")
    public void iCanSeeThatAllScenariosAreJiraSynced() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(1000);
            Waiters.waitAppearanceOf(10, editorPage.firstScenario.getWrappedElement());
            boolean synced = false;
            for (Link l : editorPage.scenariosList) {
                WebElement icon = l.findElement(By.xpath("./parent::div//li[contains(@class, 'status-link')][2]/i"));
                synced = icon.getAttribute("class").contains("synced");
                if(!synced)
                    break;
            }
            ReportService.reportAction("All scenarios are Jira-synced.", synced);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that scenario \"([^\"]*)\" is not Jira-synced in the table")
    public void iCanSeeThatScenarioIsNotJiraSynced(String scenario) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(1000);
            if(scenario.equals("current")){
                scenario = CommonHelper.testScenarioName;
            }
            Waiters.waitAppearanceOf(10, editorPage.firstScenario.getWrappedElement());
            boolean synced = false;
            for (Link l : editorPage.scenariosList) {
                String s = l.getText();
                if(l.getText().startsWith(scenario)){
                    WebElement icon = l.findElement(By.xpath("./parent::div//li[contains(@class, 'status-link')][2]/i"));
                    synced = !icon.getAttribute("class").contains("synced");
                    break;
                }
            }
            ReportService.reportAction("'" + scenario + "' scenario is not Jira-synced.", !synced);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see scenario is expanded")
    public void iSelectScenarioInTheTable() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, driver.findElement(By.xpath(".//div[contains(@class, 'accordion-content')]")));
            ReportService.reportAction("Scenario is expanded.", editorPage.verifyIfScenarioAccordionIsExpanded());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
