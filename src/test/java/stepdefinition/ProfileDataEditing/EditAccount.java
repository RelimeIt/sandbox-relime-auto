package stepdefinition.ProfileDataEditing;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import cucumber.api.java.en.*;
import cucumber.runtime.Runtime;
import helpers.CommonHelper;
import helpers.DBHelper;
import helpers.SystemHelper;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.sikuli.api.robot.Keyboard;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.ProfilePage;
import pages.relime.SettingsPage;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

/**
 * Created by kozlov on 6/29/2016.
 */
public class EditAccount extends PageInstance {

    @Autowired
    ProfilePage profilePage;

    String iconAddress = "";

    @When("^I change textbox 'Account name' value to \"([^\"]*)\" in table 'Credentials'$")
    public void iChangeTextboxAccountNameValueToInPopupEditAccount(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            profilePage.editCredentialForm.findElement(By.xpath(profilePage.CREDENTIALS_FORM_ACCOUNT_FIELD)).clear();
            profilePage.editCredentialForm.findElement(By.xpath(profilePage.CREDENTIALS_FORM_ACCOUNT_FIELD)).sendKeys(arg0);
            CommonHelper.testAccountName = arg0;
            ReportService.reportAction("Textbox 'Account name' value was changed in pop-up 'Edit account'.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I change textbox 'User name' value to \"([^\"]*)\" in table 'Credentials'$")
    public void iChangeTextboxUserNameValueToInPopupEditAccount(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            profilePage.editCredentialForm.findElement(By.xpath(profilePage.CREDENTIALS_FORM_NAME_FIELD)).clear();
            profilePage.editCredentialForm.findElement(By.xpath(profilePage.CREDENTIALS_FORM_NAME_FIELD)).sendKeys(arg0);
            CommonHelper.testAccountUserName = arg0;
            ReportService.reportAction("Textbox 'User name' value was changed in pop-up 'Edit account'.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I change textbox 'Password' value to \"([^\"]*)\" in table 'Credentials'$")
    public void iChangeTextboxpasswordValueToInPopupEditAccount(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            profilePage.editCredentialForm.findElement(By.xpath(profilePage.CREDENTIALS_FORM_PASSWORD_FIELD)).clear();
            profilePage.editCredentialForm.findElement(By.xpath(profilePage.CREDENTIALS_FORM_PASSWORD_FIELD)).sendKeys(arg0);
            CommonHelper.testAccountPassword = arg0;
            ReportService.reportAction("Textbox 'Password' value was changed in table 'Credantials'.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that dialog 'Update picture' has error \"([^\"]*)\"$")
    public void iCanSeeThatDialogHasError(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, profilePage.dialogErrorField.getWrappedElement());
            ReportService.reportAction("Dialog 'Update picture' has error '" + arg0 + "'.", profilePage.dialogErrorField.getText().equals(arg0));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I click button 'Save' for edited account$")
    public void iClickButtonSaveForEditedAccount() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            profilePage.editCredentialForm.findElement(By.xpath(profilePage.CREDENTIALS_FORM_UPDATE)).click();
            Thread.sleep(1000);
            ReportService.reportAction("Button 'Save' was clicked for edited account.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I click button 'Update photo' on Profile page$")
    public void iClickButtonUpdatePhoto() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, profilePage.updatePhotoButton.getWrappedElement());
            profilePage.updatePhotoButton.click();
            Thread.sleep(1000);
            ReportService.reportAction("Button 'Update photo' was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I click button 'Select photo' on pop-up 'Update Photo'$")
    public void iClickButtonSelectPhoto() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, profilePage.selectPhotoButton.getWrappedElement());
            profilePage.selectPhotoButton.click();
            Thread.sleep(1000);
            ReportService.reportAction("Button 'Update photo' was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Given("^I check that user \"([^\"]*)\" has no avatar$")
    public void iCheckThatUserHasNoAvatar(String user) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            user = CommonHelper.replaceUserWithActualEmail(user);
            DBHelper.executeStatement("DELETE FROM [Avatar] WHERE User_Id IN (SELECT UserId FROM [User] WHERE UserName = '" + user + "')");
            ReportService.reportAction("Button 'Update photo' was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I select account picture \"([^\"]*)\" in upload pop-up on Profile page$")
    public void iSelectAccountPicture(String pic) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            switch (pic){
                case "default":
                    pic = "\\pictures\\icon.png";
                    break;
                case "big":
                    pic = "\\pictures\\icon_big.jpg";
                    break;
                default:
                    ReportService.reportAction("Wrong parameter. Available options: 'default', 'big'", true);
            }
            pic = System.getProperty("user.dir") + pic;
            StringSelection ss = new StringSelection(pic);
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
            Robot r = new Robot();
            r.keyPress(KeyEvent.VK_CONTROL);
            r.keyPress(KeyEvent.VK_V);
            r.keyRelease(KeyEvent.VK_V);
            r.keyRelease(KeyEvent.VK_CONTROL);
            Thread.sleep(1000);
            r.keyPress(KeyEvent.VK_ENTER);
            r.keyRelease(KeyEvent.VK_ENTER);
            Thread.sleep(2000);
            ReportService.reportAction("Picture was selected.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I can see that selected account picture's name is \"([^\"]*)\" in upload pop-up on Profile page$")
    public void iCanSeeThatSelectedPictureNameIs(String pic) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            switch (pic){
                case "default":
                    pic = "icon.png";
                    break;
                case "big":
                    pic = "icon_big.jpg";
                    break;
                default:
                    ReportService.reportAction("Wrong parameter. Available options: 'default', 'big'", true);
            }
            String s = profilePage.selectedPhotoNameLabel.getText();
            ReportService.reportAction("Picture '" + pic + "' is selected.", profilePage.selectedPhotoNameLabel.getText().equals(pic));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that 'Update photo' dialog is opened$")
    public void iCanSeePopUpUpdatePhoto() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, profilePage.dialogHeader.getWrappedElement());
            ReportService.reportAction("Button 'Update photo' was clicked.", profilePage.dialogHeader.getText().toUpperCase().equals("PROFILE PHOTO UPDATING"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I click button 'Update' in dialog$")
    public void iClickUpdateButton() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, profilePage.dialogUpdateButton.getWrappedElement());
            profilePage.dialogUpdateButton.click();
            Thread.sleep(1000);
            ReportService.reportAction("Button 'Update' was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }


    @When("^I close dialog$")
    public void iCloseDialog() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, profilePage.closeDialogButton.getWrappedElement());
            profilePage.closeDialogButton.click();
            Waiters.waitDisappearsOf(5, profilePage.dialogHeader.getWrappedElement(), 1);
            ReportService.reportAction("Dialog was closed.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I save current user icon address$")
    public void iSaveCurrentIcon() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, profilePage.accountIcon.getWrappedElement());
            iconAddress = profilePage.accountIcon.getAttribute("src");
            ReportService.reportAction("Current icon address saved.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that icon address hasn't changed$")
    public void iCheckIconHasntChanged() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, profilePage.accountIcon.getWrappedElement());
            ReportService.reportAction("Icon address hasn't changed.", profilePage.accountIcon.getAttribute("src").equals(iconAddress));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that icon address has changed$")
    public void iCheckIconHasChanged() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, profilePage.accountIcon.getWrappedElement());
            ReportService.reportAction("Icon address has changed.", !profilePage.accountIcon.getAttribute("src").equals(iconAddress));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I click button 'Add' for new credential$")
    public void iClickButtonAddForNewCredential() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            profilePage.createNewCredentialForm.findElement(By.xpath(profilePage.CREDENTIALS_FORM_UPDATE)).click();
            Thread.sleep(1000);
            ReportService.reportAction("Button 'Save' was clicked for edited account.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I click button 'Cancel' for new credential$")
    public void iClickButtonCancelForNewCredential() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            profilePage.createNewCredentialForm.findElement(By.xpath(profilePage.CREDENTIALS_FORM_CANCEL)).click();
            Thread.sleep(1000);
            ReportService.reportAction("Button 'Save' was clicked for edited account.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
