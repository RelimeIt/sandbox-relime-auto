package stepdefinition;

import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.*;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.CommonHelper;
import helpers.DBHelper;
import helpers.SystemHelper;
import arp.CucumberArpReport;
import arp.ReportService;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.*;

import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

public class Smoke extends PageInstance {

    @Autowired
    AdminPage adminPage;

    @Autowired
    DashboardPage dashboardPage;

    @Autowired
    EditorPage editorPage;

    @Autowired
    SettingsPage settingsPage;

    @Autowired
    ProfilePage profilePage;

    @And("^I type \"([^\"]*)\" as background name in textbox 'Name' in pop-up 'New Scenario'$")
    public void iTypeAsBackgroundNameInTextboxNameInPopUpNewScenario(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.newScenarioNameInput.enterText(arg0);
            CommonHelper.testBackgroundName = arg0;
            ReportService.reportAction("'" + arg0 + "' is set to the field 'Name' for background in pop-up 'New Scenario'.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I type \"([^\"]*)\" as background description in textbox 'Description' for background in pop-up 'New Scenario'$")
    public void iTypeAsBackgroundDescriptionInTextboxDescriptionInPopUpNewScenario(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.descriptionInput.enterText(arg0);
            CommonHelper.testBackgroundDescription = arg0;
            ReportService.reportAction("'" + arg0 + "' is set to the field 'Description' in pop-up 'New Scenario'.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that new background is successfully created$")
    public void iSeeThatNewBackgroundIsSuccessfullyCreated() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Scenario name is correct.", editorPage.verifyBackgroundName());
            ReportService.reportAction("Scenario description is correct.", editorPage.verifyBackgroundDescription());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that the new background is expanded$")
    public void iSeeThatTheNewBackgroundIsExpanded() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("The scenario is expanded.", editorPage.verifyIfBackgroundAccordionIsExpanded());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that the background is collapsed")
    public void iSeeThatTheNewBackgroundIsCollapsed() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("The scenario is collapsed.", !editorPage.verifyIfBackgroundAccordionIsExpanded());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that background is displayed in separated control in the first position in Scenario Editor$")
    public void iSeeThatBackgroundIsDisplayedInSeparatedControlInTheFirstPositionInScenarioEditor() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(3, editorPage.backgroundElement.getWrappedElement());
            ReportService.reportAction("The scenario is expanded.", editorPage.verifyIfBackgroundIsDisplayedInSeparatedControl());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that new background is successfully created and entered data matches data in Scenario Editor for the new background$")
    public void andISeeThatNewBackgroundIsSuccessfullyCreatedAndEnteredDataMatchesDataInScenarioEditorForTheNewBackground() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Background name is correct.", editorPage.verifyBackgroundName());
            ReportService.reportAction("Background description is correct.", editorPage.verifyBackgroundDescription());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that example table is not displayed in the new background in Scenario Editor$")
    public void iSeeThatExampleTableIsNotDisplayedInTheNewBackgroundInScenarioEditor() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("The scenario is expanded.", !editorPage.verifyIfExampleTableIsDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I select folder \"([^\"]*)\" in the tree$")
    public void iSelectElementInTheTree(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.selectFolderByName(arg0);
            ReportService.reportAction("Folder was selected.", editorPage.verifyIfTreeElementIsSelected(CommonHelper.testFolderName));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I change textbox 'Folder name' value to \"([^\"]*)\" in folder info block under the tree$")
    public void iChangeTextboxFolderNameValueToInFolderInfoBlockUnderTheTree(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(!editorPage.changeFieldAfterFocusInput.isDisplayed())
                editorPage.changeFolderNameInputBeforeFocus.click();
            editorPage.changeFieldAfterFocusInput.clear();
            editorPage.changeFieldAfterFocusInput.sendKeys(arg0);
            CommonHelper.testFolderName = arg0;
            ReportService.reportAction("'" + arg0 + "' is set to the field 'Folder name' in folder info block under the tree.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click button 'Enter'$")
    public void iClickButtonEnter() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            try{
                Thread.sleep(500);
                CommonHelper.sendKeyWithActions(Keys.ENTER);
                ReportService.reportAction("'Enter' button was pressed.", true);
            }
            catch (Exception ex) {
                CommonHelper.sendKeyWithAWT(KeyEvent.VK_ENTER);
                ReportService.reportAction("'Enter' button was pressed.", true);
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that changes are successfully saved in textbox 'Folder name' in folder info block under the tree$")
    public void iSeeThatChangesAreSuccessfullySavedInTextboxFolderNameInFolderInfoBlockUnderTheTree() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Changes were successfully saved in textbox 'Folder name' in folder info block under the tree.",
            editorPage.changeFolderNameInputBeforeFocus.isDisplayed()&&editorPage.changeFolderNameInputBeforeFocus.getText().equals(CommonHelper.testFolderName));
            /*ReportService.reportAction("Changes were successfully saved in textbox 'Folder name' in folder info block under the tree.",
            editorPage.getChangeFolderNameInputBeforeFocus().isDisplayed()&&editorPage.getChangeFolderNameInputBeforeFocus().getText().equals(CommonHelper.testFolderName));*/
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I lose focus$")
    public void iLoseFocus() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            SystemHelper.waitAppearanceByElement(driver, 100, 30, editorPage.headerLabel.getWrappedElement());
            editorPage.headerLabel.click();
            ReportService.reportAction("Focus was lost.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that new tag is added in tags input textbox in feature info block under the tree$")
    public void iSeeThatNewTagIsAddedInTagsInputTextboxInFeatureInfoBlockUnderTheTree() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Scenario tags are correct.", editorPage.verifyFeatureTags());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I type \"([^\"]*)\" in textbox 'Name' in pop-up 'New Scenario'$")
    public void iTypeInTextboxNameInPopUpNewScenario(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.newScenarioNameInput.enterText(arg0);
            CommonHelper.testScenarioName = arg0;
            ReportService.reportAction("'" + arg0 + "' is set to the field 'Name' in pop-up 'New Scenario'.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I type \"([^\"]*)\" in textbox 'Description' in pop-up 'New Scenario'$")
    public void iTypeInTextboxDescriptionInPopUpNewScenario(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.descriptionInput.enterText(arg0);
            CommonHelper.testScenarioDescription = arg0;
            ReportService.reportAction("'" + arg0 + "' is set to the field 'Description' in pop-up 'New Scenario'.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I type \"([^\"]*)\" in the first string in Scenario Editor in pop-up 'New Scenario'$")
    public void iTypeInTheFirstStringInScenarioEditorInPopUpNewScenario(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.formStepsTextarea.click();
            TextInput t = new TextInput(editorPage.formStepsTextarea.findElement(By.xpath("./textarea")));
            for (char c : arg0.toCharArray()) {
                String s = String.valueOf(c);
                t.sendKeys(s);
            }
            Thread.sleep(1000);
            ReportService.reportAction("'" + arg0 + "' is set to the first string in Scenario Editor in pop-up 'New Scenario'.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I type \"([^\"]*)\" into string \"([^\"]*)\" in Scenario Editor in pop-up 'New Scenario'$")
    public void iTypeIntoStringInScenarioEditorInScenarioAccordion(String arg0, String arg1) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(arg0.equals("comment")){
                arg0 = "\"\"\" comment";
            }
            if(editorPage.formStepsTextarea.getAttribute("class").contains("ace_focus"))
                editorPage.formStepsTextarea.click();
            TextInput t = new TextInput(editorPage.formStepsTextarea.findElement(By.xpath("./textarea")));
            t.sendKeys("\n");
            for (char c : arg0.toCharArray()) {
                String s = String.valueOf(c);
                t.sendKeys(s);
            }
            Thread.sleep(1000);
            ReportService.reportAction("'" + arg0 + "' is set to the " + arg1 + " string in Scenario Editor in pop-up 'New Scenario'.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    /*@When("^I click option 'Delete' under drop-down 'Actions' in scenario accordion$")
    public void iClickOptionDeleteUnderDropDownActionsInScenarioAccordion() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            SystemHelper.waitAppearanceByXpath(driver, 100, 30, "(.//div[contains(@class, 'ace_line')])[1]");
            editorPage.actionsButton.click();
            editorPage.actionDeleteInScenarioBlockButton.click();
            ReportService.reportAction("Button 'Delete' under dropdown actions was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }*/

    @And("^I expand scenario \"([^\"]*)\"$")
    public void iExpandScenario(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(CommonHelper.delay, editorPage.firstScenario.getWrappedElement());
            SystemHelper.waitAppearanceByElement(driver, 30, 100, editorPage.firstScenario.getWrappedElement());
            editorPage.selectScenarioByName(arg0);
            ReportService.reportAction("'" + arg0 + "' scenario was selected.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I change textbox 'Name' value to \"([^\"]*)\" in scenario accordion$")
    public void iChangeTextboxNameValueToInScenarioAccordion(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.changeScenarioName(arg0);
            CommonHelper.testScenarioName = arg0;
            ReportService.reportAction("Scenario name was changed.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I change textbox 'Description' value to \"([^\"]*)\" in scenario accordion$")
    public void iChangeTextboxDescriptionValueToInScenarioAccordion(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.changeScenarioDescription(arg0);
            CommonHelper.testScenarioDescription = arg0;
            ReportService.reportAction("Scenario description was changed.", true);
        } catch (AssertionError e) {
            throw e;

        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see that textbox 'Description' value equals \"([^\"]*)\" in scenario accordion$")
    public void iCanSeeThatTextboxDescriptionValueEqualsInScenarioAccordion(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(CommonHelper.delay, editorPage.changeScenarioDescriptionInputBeforeFocus.getWrappedElement());
            arg0 = arg0.replace("\\n","\n");
            ReportService.reportAction("Scenario description was changed.", editorPage.changeScenarioDescriptionInputBeforeFocus.getText().equals(arg0));
        } catch (AssertionError e) {
            throw e;

        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I click button 'Save' for edited field$")
    public void iClickButtonSaveForEditedFieldUnderTheTree() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(CommonHelper.delay, editorPage.saveFeatureEditedFieldButton.getWrappedElement());
            editorPage.saveFeatureEditedFieldButton.click();
            ReportService.reportAction("Button 'Save' for edited field was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I click button 'Cancel' for edited field$")
    public void iClickButtonCancelForEditedFieldUnderTheTree() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(CommonHelper.delay, editorPage.cancelEditedFieldButton.getWrappedElement());
            editorPage.cancelEditedFieldButton.click();
            ReportService.reportAction("Button 'Cancel' for edited field was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I add text \"([^\"]*)\" for textbox 'Description' in scenario accordion$")
    public void iAddTextToTextboxDescriptionValueToInScenarioAccordion(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.addToScenarioDescription(arg0);
            CommonHelper.testScenarioDescription = arg0;
            ReportService.reportAction("Scenario description was changed.", true);
        } catch (AssertionError e) {
            throw e;

        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I type \"([^\"]*)\" into tags input textbox in scenario accordion$")
    public void iTypeIntoTagsInputTextboxInScenarioAccordion(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            //editorPage.deleteTagsFromScenario();
            editorPage.changeScenarioTags(arg0);
            if(CommonHelper.testScenarioTags == null){
                CommonHelper.testScenarioTags = new ArrayList<>();
            }
            CommonHelper.testScenarioTags.add(arg0);
            ReportService.reportAction("Scenario tags were changed.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click button 'Save scenarios'$")
    public void iClickButtonSaveScenarios() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, editorPage.saveScenariosButton.getWrappedElement());
            //This is kostyl, comrade! Remove when not needed
            try {
                WebElement e = driver.findElement(By.xpath(".//div[contains(@class,'ui-notification ng-scope wait')]"));
                CommonHelper.clickWithActions(e);
            }
            catch (Exception e){}
            editorPage.saveScenariosButton.click();
            //CommonHelper.clickWithActions(editorPage.saveScenariosButton.getWrappedElement());
            ReportService.reportAction("Save scenarios button was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click option \"([^\"]*)\" under drop-down 'Actions' in scenario accordion$")
    public void iClickOptionUnderDropDownActionsInScenarioAccordion(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.clickActionsMenuOption(arg0);
            ReportService.reportAction("'" + arg0 + "' option in 'Actions' menu was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I delete scenario in scenario accordion$")
    public void iDeleteScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.clickActionsMenuOption("Delete");
            editorPage.clickButtonOnPopUp("Delete");
            ReportService.reportAction("Scenario was deleted.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that the scenario is collapsed$")
    public void iSeeThatTheScenarioIsCollapsed() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("The scenario is collapsed.", !editorPage.verifyIfScenarioAccordionIsExpanded());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that the new scenario is expanded$")
    public void iSeeThatTheNewScenarioIsExpanded() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("The scenario is expanded.", editorPage.verifyIfScenarioAccordionIsExpanded());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I type \"([^\"]*)\" into tags input textbox in pop-up 'New Scenario'$")
    public void iTypeIntoTagsInputTextboxInPopUpNewScenario(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.tagsInputOnNewScenarioPopup.click();
            editorPage.tagsInputOnNewScenarioPopup.clear();
            CommonHelper.sendKeysWithActions(editorPage.tagsInputOnNewScenarioPopup.getWrappedElement(), arg0);
            if(CommonHelper.testScenarioTags == null){
                CommonHelper.testScenarioTags = new ArrayList<>();
            }
            CommonHelper.testScenarioTags.add(arg0);
            ReportService.reportAction("Valid automation tags were populated.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I add tag \"([^\"]*)\" via comma on pop-up")
    public void iAddTagViaComma(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.addTagViaCommaOnPopUp(arg0);
            ReportService.reportAction("Tag '" + arg0 + "' was added.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I add tag \"([^\"]*)\" via Enter on pop-up$")
    public void iAddTagViaEnter(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.addTagViaEnterOnPopUp(arg0);
            ReportService.reportAction("Tag '" + arg0 + "' was added.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I verify invalid tags on pop-up")
    public void iVerifyInvalidTags(DataTable tags) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            for (List<String> line : tags.raw()) {
                boolean messageDisplayed = false;
                editorPage.addTagViaEnterOnPopUp(line.get(0));
                Thread.sleep(500);
                for (Label e : dashboardPage.formFieldWarnings) {
                    if (e.isDisplayed() && e.getText().equals(line.get(1))){
                        messageDisplayed = true;
                        break;
                    }
                }
                ReportService.reportAction("Error '" + line.get(1) + "' appeared for tag '" + line.get(0) + "' when entered via Enter.", messageDisplayed);
                messageDisplayed = false;
                editorPage.addTagViaCommaOnPopUp(line.get(0));
                Thread.sleep(500);
                for (Label e : dashboardPage.formFieldWarnings) {
                    if (e.isDisplayed() && e.getText().equals(line.get(1))){
                        messageDisplayed = true;
                        break;
                    }
                }
                ReportService.reportAction("Error '" + line.get(1) + "' appeared for tag '" + line.get(0) + "' when entered via comma.", messageDisplayed);
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that example table is displayed in the new scenario in Scenario Editor$")
    public void iSeeThatExampleTableIsDisplayedInTheNewScenarioInScenarioEditor() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Example table is displayed.", editorPage.verifyIfExampleTableIsDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that new scenario is created and entered data matches data in Scenario Editor for the new scenario$")
    public void iSeeThatNewScenarioIsCreatedAndEnteredDataMatchesDataInScenarioEditorForTheNewScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Scenario name is correct.", editorPage.verifyScenarioName());
            ReportService.reportAction("Scenario description is correct.", editorPage.verifyScenarioDescription());
            ReportService.reportAction("Scenario tags are correct.", editorPage.verifyScenarioTags());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that changes are successfully saved and changed data matches data in Scenario Editor for the edited scenario$")
    public void iSeeThatChangesAreSuccessfullySavedAndChangedDataMatchesDataInScenarioEditorForTheEditedScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            editorPage.selectScenarioByName(CommonHelper.testScenarioName);
            ReportService.reportAction("Scenario name is correct.", editorPage.verifyScenarioName());
            ReportService.reportAction("Scenario description is correct.", editorPage.verifyScenarioDescription());
            ReportService.reportAction("Scenario tags are correct.", editorPage.verifyScenarioTags());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
	
    @When("^I save changes to Git$")
    public void iClickOnButtonSaveChangesToGit() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(30, editorPage.saveChangesToGit.getWrappedElement());
            editorPage.saveChangesToGit.click();
            ReportService.reportAction("'Save changes to Git' button in page 'Feature Management' was clicked.", true);
            Waiters.waitAppearanceOf(5, editorPage.selectAllTreeButton.getWrappedElement());
            editorPage.selectAllTreeButton.click();
            ReportService.reportAction("All features were selected.", true);
            Thread.sleep(500);
            editorPage.confirmTreeButton.click();
            ReportService.reportAction("'Confirm' button was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see wait notification is displayed in page 'Feature Management'$")
    public void iSeeWaitNotificationIsDispayed() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(CommonHelper.delay, editorPage.infoNotification.getWrappedElement());
            ReportService.reportAction("Wait notification was displayed.", editorPage.infoNotification.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that changes are sucessfully saved in textbox 'Name' in page 'Project settings'$")
    public void iSeeChangesAreSuccessfulySavedInTextboxNameInPageProjectSettings() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Project name successfule changed.", settingsPage.NameField.getText().equals(CommonHelper.testProjectName));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

	@When("^I click checkbox 'background' in pop-up 'New Scenario'$")
    public void iClickCheckboxBackgroundInPopUpNewScenario() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            SystemHelper.waitAppearanceByElement(driver, 100, 30, editorPage.backgroundOnNewScenarioPopupCheckbox.getWrappedElement());
            //editorPage.backgroundOnNewScenarioPopupCheckbox.sendKeys(Keys.SPACE);
            //editorPage.backgroundOnNewScenarioPopupCheckbox.click();
            CommonHelper.clickWithJavascript(editorPage.backgroundOnNewScenarioPopupCheckbox.getWrappedElement());
            ReportService.reportAction("'Background' checkbox was clicked", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can delete \"([^\"]*)\" from setting 'Task tracking system' in page 'Project settings'$")
    public void icanDeleteOptionFromSettingTTS(String name) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            SystemHelper.waitAppearanceByXpath(driver, 100, 30, "//div[@class= 'system-control-row']//a");
            boolean elementFound = false;
            for (Element e : settingsPage.jiraDivs) {
                if(e.findElement(By.xpath(".//div[@class= 'system-control-row']//a")).getText().toLowerCase().equals(name.toLowerCase())){
                    e.findElement(By.xpath(".//*[@data-uib-tooltip= 'Delete Jira path']")).click();
                }
            }
            Waiters.waitAppearanceOf(5, settingsPage.formHeader.getWrappedElement());
            boolean buttonClicked = false;
            for(Button e : settingsPage.FormButtons){
                String s = e.getText();
                if(s.toLowerCase().equals("delete")) {
                    e.click();
                    buttonClicked = true;
                }
            }
            if(!buttonClicked)
                ReportService.reportAction("\"" + "Delete" + "\" button not found!", false);
            Waiters.waitDisappearsOf(5, settingsPage.formHeader.getWrappedElement(), 10);
            for (Element e : settingsPage.jiraDivs) {
                String s = e.findElement(By.xpath(".")).getText();
                if(s.equals("")){break;}
                else if(e.findElement(By.xpath(".//div[@class= 'system-control-row']//a")).getText().toLowerCase().equals(name.toLowerCase())){
                    elementFound = true;
                }
            }
            ReportService.reportAction(name + " deleted from setting 'Task tracking system'", !elementFound);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I tie \"([^\"]*)\" account in TTS settings$")
    public void iTieAccountInTTSSettings(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(arg0.equals("default"))
                arg0 = SystemHelper.DEFAULTJIRAACCOUNT;
            settingsPage.selectTTSAccount(arg0);
            ReportService.reportAction("Account was selected.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that data from linked Jira issue matches data in new feature in feature info block under the tree$")
    public void iSeeThatDataFromLinkedJiraIssueMatchesDataInNewFeatureInFeatureInfoBlockUnderTheTree() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Feature name is correct.", editorPage.changeFeatureNameBeforeFocusInput.getWrappedElement().getText().equals(CommonHelper.testJiraLinkedFeatureName));
            ReportService.reportAction("Feature filename is correct.", editorPage.changeFileNameBeforeFocusInput.getWrappedElement().getText().equals(CommonHelper.testJiraLinkedFeatureFilename));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that new jira-linked feature is successfully created$")
    public void iSeeThatNewJiraLinkedFeatureIsSuccessfullyCreated() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitWhileConditionIsTrue(CommonHelper.delay, editorPage.firstTreeNode.isDisplayed());
            ReportService.reportAction("Feature was created.", editorPage.treeElementExists(CommonHelper.testJiraLinkedFeatureFilename));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I update all features from Git on Editor page$")
    public void iClickIconUpdateFromgitInPanelFeatures() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, editorPage.updateFromGit.getWrappedElement());
            editorPage.updateFromGit.click();
            ReportService.reportAction("'Update from Git' button in panel 'Features' was clicked.", true);
            Waiters.waitAppearanceOf(5, editorPage.selectAllTreeButton.getWrappedElement());
            editorPage.selectAllTreeButton.click();
            ReportService.reportAction("All features were selected.", true);
            Thread.sleep(500);
            editorPage.confirmTreeButton.click();
            ReportService.reportAction("'Confirm' button was clicked.", true);
            ReportService.reportAction("Notification window appeared.", settingsPage.waitForSuccessNotification(""));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I update all projects from Jira on Editor page$")
    public void iUpdateAllProjectFromJira() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, editorPage.updateFromJira.getWrappedElement());
            editorPage.updateFromJira.click();
            ReportService.reportAction("'Update from Jira' button in panel 'Features' was clicked.", true);
            Waiters.waitAppearanceOf(5, editorPage.selectAllTreeButton.getWrappedElement());
            editorPage.selectAllTreeButton.click();
            ReportService.reportAction("All features were selected.", true);
            Thread.sleep(500);
            editorPage.confirmTreeButton.click();
            ReportService.reportAction("'Confirm' button was clicked.", true);
            ReportService.reportAction("Notification window appeared.", settingsPage.waitForSuccessNotification(""));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I export all Jira-linked projects to Jira on Editor page$")
    public void iUploadProjectsToJira() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, editorPage.uploadToJira.getWrappedElement());
            editorPage.uploadToJira.click();
            ReportService.reportAction("'Export to Jira' button in panel 'Features' was clicked.", true);
            Waiters.waitAppearanceOf(5, editorPage.selectAllTreeButton.getWrappedElement());
            int number = editorPage.treeElementCheckboxes.size();
            editorPage.selectAllTreeButton.click();
            ReportService.reportAction("All features were selected.", true);
            Thread.sleep(500);
            editorPage.confirmTreeButton.click();
            ReportService.reportAction("'Confirm' button was clicked.", true);
            if(number>1)
                ReportService.reportAction("Notification window appeared.", settingsPage.waitForSuccessNotification(number + " features have been exported successfully to Jira repository"));
            else
                ReportService.reportAction("Notification window appeared.", settingsPage.waitForSuccessNotification("The feature has been exported successfully to Jira repository"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I export \"([1-9]*)\" Jira-linked projects to Jira on Editor page$")
    public void iUploadSomeProjectsToJira(int number) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, editorPage.uploadToJira.getWrappedElement());
            editorPage.uploadToJira.click();
            ReportService.reportAction("'Export to Jira' button in panel 'Features' was clicked.", true);
            Waiters.waitAppearanceOf(5, editorPage.selectAllTreeButton.getWrappedElement());
            if(number>editorPage.treeElementCheckboxes.size())
                ReportService.reportAction("Not enough Jira-Linked features.", false);
            for (int i = 0; i < number; i++) {
                editorPage.treeElementCheckboxes.get(i).click();
                Thread.sleep(200);
            }
            ReportService.reportAction("All features were selected.", true);
            Thread.sleep(500);
            editorPage.confirmTreeButton.click();
            ReportService.reportAction("'Confirm' button was clicked.", true);
            if(number>1)
                ReportService.reportAction("Notification window appeared.", settingsPage.waitForSuccessNotification(number + " features have been exported successfully to Jira repository"));
            else
                ReportService.reportAction("Notification window appeared.", settingsPage.waitForSuccessNotification("The feature has been exported successfully to Jira repository"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I update all Jira-linked projects from Jira on Editor page$")
    public void iUpdateProjectsToJira() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, editorPage.updateFromJira.getWrappedElement());
            editorPage.updateFromJira.click();
            ReportService.reportAction("'Update from Jira' button in panel 'Features' was clicked.", true);
            Waiters.waitAppearanceOf(5, editorPage.selectAllTreeButton.getWrappedElement());
            int number = editorPage.treeElementCheckboxes.size();
            editorPage.selectAllTreeButton.click();
            ReportService.reportAction("All features were selected.", true);
            Thread.sleep(500);
            editorPage.confirmTreeButton.click();
            ReportService.reportAction("'Confirm' button was clicked.", true);
            if(number>1)
                ReportService.reportAction("Notification window appeared.", settingsPage.waitForSuccessNotification(number + " features have been successfully updated from Jira repository"));
            else
                ReportService.reportAction("Notification window appeared.", settingsPage.waitForSuccessNotification("The feature has been updated successfully from Jira repository"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I update \"([1-9]*)\" Jira-linked projects from Jira on Editor page$")
    public void iUpdateSomeProjectsToJira(int number) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, editorPage.updateFromJira.getWrappedElement());
            editorPage.updateFromJira.click();
            ReportService.reportAction("'Update from Jira' button in panel 'Features' was clicked.", true);
            Waiters.waitAppearanceOf(5, editorPage.selectAllTreeButton.getWrappedElement());
            if(number>editorPage.treeElementCheckboxes.size())
                ReportService.reportAction("Not enough Jira-Linked features.", false);
            for (int i = 0; i < number; i++) {
                editorPage.treeElementCheckboxes.get(i).click();
                Thread.sleep(200);
            }
            ReportService.reportAction("All features were selected.", true);
            Thread.sleep(500);
            editorPage.confirmTreeButton.click();
            ReportService.reportAction("'Confirm' button was clicked.", true);
            if(number>1)
                ReportService.reportAction("Notification window appeared.", settingsPage.waitForSuccessNotification(number + " features have been successfully updated from Jira repository"));
            else
                ReportService.reportAction("Notification window appeared.", settingsPage.waitForSuccessNotification("The feature has been updated successfully from Jira repository"));

        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that the tree is refreshed$")
    public void iSeeThatTheTreeIsRefreshed() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(500, editorPage.treeUpdating.getWrappedElement());
            Waiters.waitDisappearsOf(20000, editorPage.treeUpdating.getWrappedElement(), 500);
            ReportService.reportAction("Tree successfully updated.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see that Actions column is not displayed for projects")
    public void iCanSeeThatActionsColumnIsNotDisplayedForProjects() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean correct = true;
            for (Label l : dashboardPage.projectTableColumnLabels) {
                if(l.getText().toLowerCase().equals("actions")){
                    correct = false;
                }
            }
            if(correct){
                for (Element row : dashboardPage.projectTableRows) {
                    WebElement buttons = null;
                    try {
                        buttons = row.getWrappedElement().findElement(By.xpath("./div[@class = 'btn-holder']"));
                    } catch (Throwable ex) {}
                    if (buttons != null) {
                        correct = false;
                        break;
                    }
                }
            }
            ReportService.reportAction("Actions column is not displayed.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see dialog \"([^\"]*)\" is opened on Profile page")
    public void dialogIsOpened(String name) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(15, profilePage.dialogHeader.getWrappedElement());
            Thread.sleep(1000);
            boolean popupAppeared = profilePage.dialogHeader.isDisplayed();
            boolean popupNameMatches = true;
            if (!name.equals("")) {
                popupNameMatches = profilePage.dialogHeader.getText().toLowerCase().equals(name.toLowerCase());
            }
            ReportService.reportAction(name + " dialog is opened.", popupAppeared && popupNameMatches);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I click \"Create Domain\" button")
    public void iClickCreateDomainButton() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(15, profilePage.createDomainButton.getWrappedElement());
            profilePage.createDomainButton.click();
            ReportService.reportAction("\"Create Domain\" button is clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I check that domain \"([^\"]*)\" is absent in DB")
    public void iDeleteDomainFromDB(String domain) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            DBHelper.executeStatement("DELETE FROM [RelimeDb].[dbo].[Domain] WHERE Name = '" + domain + "';");
            ReportService.reportAction("Domain '" + domain + "' is absent in DB.", true);
            ReportService.reportAction("\"Create Domain\" button is clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I change \"Domain Name\" field value to \"([^\"]*)\" on dialog")
    public void iChangeDomainNameValue(String domain) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(15, profilePage.domainNameDialogField.getWrappedElement());
            profilePage.domainNameDialogField.click();
            Thread.sleep(500);
            profilePage.domainNameDialogField.clear();
            Thread.sleep(500);
            profilePage.domainNameDialogField.sendKeys(domain);
            ReportService.reportAction("\"Domain Name\" field value was changed to '" + domain + "'.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I click \"([^\"]*)\" button on dialog")
    public void iClickCreateButtonOnDialog(String button) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(15, profilePage.dialogHeader.getWrappedElement());
            boolean clicked = false;
            for (Element e : profilePage.dialogButtons) {
                if(e.getText().toLowerCase().equals(button.toLowerCase())){
                    e.click();
                    clicked = true;
                    break;
                }
            }
            ReportService.reportAction(button + " button was clicked.", clicked);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that domain \"([^\"]*)\" is displayed on profile page")
    public void iCanSeeThatDomainIsDisplayed(String domain) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean domainPresent = false;
            Thread.sleep(1000);
            for (Element e : profilePage.domainsList) {
                if(e.findElement(By.xpath("./a")).getText().equals(domain)){
                    domainPresent = true;
                    break;
                }
            }
            ReportService.reportAction("Domain '" + domain + "' is present.", domainPresent);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I change \"([^\"]*)\" domain type to \"([^\"]*)\" on profile page")
    public void iChangeDomainType(String domain, String type) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean domainPresent = false;
            boolean typeChanged = false;
            Waiters.waitAppearanceOf(15, profilePage.domainsList.get(0).getWrappedElement());
            for (Element e : profilePage.domainsList) {
                if(e.findElement(By.xpath("./a")).getText().equals(domain)){
                    domainPresent = true;
                    e.findElement(By.xpath(".//button[@aria-label='domain-type']")).click();
                    Thread.sleep(500);
                    for (Element t : profilePage.domainTypeDropdownOptions) {
                        if(t.getText().toLowerCase().equals(type)){
                            t.click();
                            typeChanged = true;
                            break;
                        }
                    }
                    break;
                }
            }
            ReportService.reportAction("Domain type was changed to '" + type + "' for domain '" + domain + "'.", domainPresent&&typeChanged);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that \"([^\"]*)\" domain type is \"([^\"]*)\" on profile page")
    public void iCanSeeThatDomainTypeIs(String domain, String type) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean domainPresent = false;
            boolean typeCorrect = false;
            Thread.sleep(1000);
            for (Element e : profilePage.domainsList) {
                if(e.findElement(By.xpath("./a")).getText().equals(domain)){
                    domainPresent = true;
                    typeCorrect = e.findElement(By.xpath(".//button[@aria-label='domain-type']")).getText().equals(type);
                    break;
                }
            }
            ReportService.reportAction("Domain type is '" + type + "' for domain '" + domain + "'.", domainPresent&&typeCorrect);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I select domain \"([^\"]*)\" on profile page")
    public void iSelectDomainOnProfilePage(String domain) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean domainSelected = false;
            Waiters.waitAppearanceOf(15, profilePage.domainsList.get(0).getWrappedElement());
            for (Element e : profilePage.domainsList) {
                if(e.findElement(By.xpath("./a")).getText().equals(domain)){
                    e.findElement(By.xpath("./a")).click();
                    Thread.sleep(1000);
                    ngDriver.waitForAngularRequestsToFinish();
                    domainSelected = true;
                    break;
                }
            }
            ReportService.reportAction("Domain '" + domain + "' was selected on profile page.", domainSelected);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I select domain \"([^\"]*)\" by direct link")
    public void iSelectDomainByDirectLink(String domain) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if (domain.equals("public")){
                driver.get(SystemHelper.PUBLIC_DOMAIN);
                CommonHelper.currentDomain = SystemHelper.PUBLIC_DOMAIN;
                CommonHelper.publicProject = SystemHelper.PUBLIC_PROJECT_ON_PUBLIC_DOMAIN;
                CommonHelper.privateProject = SystemHelper.PRIVATE_PROJECT_ON_PUBLIC_DOMAIN;
            }
            else if (domain.equals("private")){
                driver.get(SystemHelper.PRIVATE_DOMAIN);
                CommonHelper.currentDomain = SystemHelper.PRIVATE_DOMAIN;
                CommonHelper.publicProject = SystemHelper.PUBLIC_PROJECT_ON_PRIVATE_DOMAIN;
                CommonHelper.privateProject = SystemHelper.PRIVATE_PROJECT_ON_PRIVATE_DOMAIN;
            }
            else if (domain.equals("domain")){
                driver.get(CommonHelper.currentDomain);
            }
            else{
                driver.get(SystemHelper.URL.replace("://", "://" + domain.toLowerCase() + "."));
            }
            Thread.sleep(500);
            if(!driver.getCurrentUrl().contains("profile")){
                ngDriver.waitForAngularRequestsToFinish();
                Thread.sleep(500);
                ngDriver.waitForAngularRequestsToFinish();
            }
            ReportService.reportAction("Domain '" + domain + "' was selected on profile page.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I change Git path to \"([^\"]*)\"")
    public void iChangeGitPath(String path) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(path.equals("default"))
                path = SystemHelper.DEFAULTGIT;
            else if(path.equals("smoke"))
                path = SystemHelper.SMOKEGIT;
            if(settingsPage.VCSAddressLabel.isDisplayed())
                settingsPage.VCSAddressLabel.click();
            Waiters.waitAppearanceOf(5, settingsPage.VCSAddressInput.getWrappedElement());
            settingsPage.VCSAddressInput.click();
            settingsPage.VCSAddressInput.clear();
            CommonHelper.sendKeysWithActions(settingsPage.VCSAddressInput.getWrappedElement(), path);
            settingsPage.saveChangesButton.click();
            ReportService.reportAction("Git address was changed to '" + path + "'", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I change Jira path to \"([^\"]*)\"")
    public void iChangeJiraPath(String path) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(settingsPage.jiraAddressLabel.isDisplayed())
                settingsPage.jiraAddressLabel.click();
            Waiters.waitAppearanceOf(5, settingsPage.jiraAddressInput.getWrappedElement());
            settingsPage.jiraAddressInput.click();
            settingsPage.jiraAddressInput.clear();
            CommonHelper.sendKeysWithActions(settingsPage.jiraAddressInput.getWrappedElement(), path);
            settingsPage.saveChangesButton.click();
            ReportService.reportAction("Jira address was changed to '" + path + "'", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I click button delete for domain \"([^\"]*)\" on profile page")
    public void iClickButtonDeleteForDomainOnProfilePage(String domain) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean domainSelected = false;
            Waiters.waitAppearanceOf(15, profilePage.domainsList.get(0).getWrappedElement());
            for (Element e : profilePage.domainsList) {
                if(e.findElement(By.xpath("./a")).getText().equals(domain)){
                    e.findElement(By.xpath("./div/button")).click();
                    ngDriver.waitForAngularRequestsToFinish();
                    domainSelected = true;
                    break;
                }
            }
            ReportService.reportAction("Domain '" + domain + "' was selected on profile page.", domainSelected);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I set tariff plan to \"([0-9]+)\"")
    public void iSetTariffPlanToValue(int tariff) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean elementFound = false;
            for (Element e : profilePage.tariffPlanOptions) {
                if(e.getText().equals(String.valueOf(tariff))){
                    e.click();
                    elementFound = true;
                    Thread.sleep(1000);
                    break;
                }
            }
            ReportService.reportAction("Tariff plan is set to " + tariff + ".", elementFound);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that tariff plan is set to \"([0-9]+)\"")
    public void iSeeThatTariffPlanIsSetToValue(int tariff) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Tariff plan is set to " + tariff + ".", profilePage.currentTariffPlan.getText().equals(String.valueOf(tariff)));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that section 'VALID TILL' date is 1 year from current date")
    public void iSeeThatSectionValidTillDateIsCorrect() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(10, profilePage.validTariffTermLabel.getWrappedElement());
            Date current = new Date();
            current.setYear(current.getYear() + 1);
            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
            ReportService.reportAction("Tariff plan is valid till " + sdf.format(current) + ".", profilePage.validTariffTermLabel.getText().equals(sdf.format(current)));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I click 'Update' button for tariff")
    public void iClickUpdateTariffButton() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(3, profilePage.updateTariffPlanButton.getWrappedElement());
            profilePage.updateTariffPlanButton.click();
            ReportService.reportAction("'Update' button for tariff was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I set tariff plan to default if it is not")
    public void iSetTariffPlanToDefault() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            for (Element e : adminPage.allInvoicesRows) {
                if(e.findElement(By.xpath(adminPage.INVOICE_EMAIL_FIELD)).getText().equals(SystemHelper.DOUSERNAME)){
                    e.findElement(By.xpath(adminPage.INVOICE_SET_TO_DEFAULT)).click();
                    Thread.sleep(500);
                    e.findElement(By.xpath(adminPage.INVOICE_CONFIRM_BUTTON)).click();
                    Thread.sleep(500);
                    break;
                }
            }
            ReportService.reportAction("Tariff plan is set to default.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I cancel tariff change request if it was sent")
    public void iCancelTariffRequest() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(profilePage.cancelTariffPlanRequestButton.isDisplayed()){
                profilePage.cancelTariffPlanRequestButton.click();
            }
            ReportService.reportAction("Tariff change request was cancelled.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that 'New Invoices' table contains request to change tariff for current account to \"([0-9]+)\"")
    public void iSeeThatNewInvoicesTableContainsAccountWithTariff(int tariff) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean invoiceCorrect = false;
            Waiters.waitAppearanceOf(5, adminPage.adminPageHeader.getWrappedElement());
            for (Element e : adminPage.newInvoicesRows) {
                if(e.findElement(By.xpath(adminPage.INVOICE_EMAIL_FIELD)).getText().equals(SystemHelper.DOUSERNAME)
                        && e.findElement(By.xpath(adminPage.INVOICE_TARIFF_FIELD)).getText().equals(tariff + " users")){
                    invoiceCorrect = true;
                    break;
                }
            }
            ReportService.reportAction("Tariff plan is set to " + tariff + ".", invoiceCorrect);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I click button 'Confirm' in 'New Invoices' table for current account")
    public void iClickButtonConfirmForCurrentAccount() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean invoiceCorrect = false;
            Waiters.waitAppearanceOf(5, adminPage.adminPageHeader.getWrappedElement());
            for (Element e : adminPage.newInvoicesRows) {
                if(e.findElement(By.xpath(adminPage.INVOICE_EMAIL_FIELD)).getText().equals(SystemHelper.DOUSERNAME)){
                    e.findElement(By.xpath(adminPage.INVOICE_CONFIRM_BUTTON)).click();
                    invoiceCorrect = true;
                    break;
                }
            }
            ReportService.reportAction("Button 'Confirm' in 'New Invoices' table for current account was clicked.", invoiceCorrect);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
