package stepdefinition.ProjectDataEditing.ProjectSettings;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import helpers.DBHelper;
import helpers.SystemHelper;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;
import pages.relime.SettingsPage;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by kozlov on 6/27/2016.
 */
public class VCSSettings extends PageInstance {

    @Autowired
    SettingsPage settingsPage;

    @And("^I am not able to add or delete VCS$")
    public void iAmNotAbleToAddOrDeleteVCS() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitDisappearsOf(0, settingsPage.AddVCSButton.getWrappedElement(),2);
            ReportService.reportAction("Add button is absent", !settingsPage.AddVCSButton.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click button 'Plus' near setting 'Version control system' in page 'Project settings'$")
    public void iClickButtonPlusNearVCSInPageProjectSettings() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(10, settingsPage.AddVCSButton.getWrappedElement());
            settingsPage.AddVCSButton.click();
            ReportService.reportAction("Button 'Plus' near setting 'Version control system' in page 'Project settings' was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I check that VCS \"([^\"]*)\" is absent in DB$")
    public void iCheckThatVCSIsAbsentInDB(String Key) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(Key.equals("default"))
                Key = SystemHelper.DEFAULTGIT;
            if(Key.equals("smoke"))
                Key = SystemHelper.SMOKEGIT;
            DBHelper.executeStatement("DELETE FROM [RelimeDb].[dbo].[Vcs] WHERE Url = '" + Key + "';");
            ReportService.reportAction("VCS '" + Key + "' is absent in DB.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that Git \"([^\"]*)\" is added in page 'Project settings'$")
    public void iSeeThatJiraIsAddedToSettingVCS(String name) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(name.equals("smoke"))
                name = SystemHelper.SMOKEGIT;
            if(name.equals("default"))
                name = SystemHelper.DEFAULTGIT;
            Thread.sleep(1000);
            Waiters.waitAppearanceOf(10, settingsPage.VCSAddressLabel.getWrappedElement());
            ReportService.reportAction("Git is added to setting 'Version control system'", settingsPage.VCSAddressLabel.getText().toLowerCase().equals(name.toLowerCase()));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
