package stepdefinition.ProjectDataEditing.ProjectSettings;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import cucumber.api.java.en.Then;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.SettingsPage;

/**
 * Created by kozlov on 9/27/2016.
 */
public class SelectAccount extends PageInstance {
    @Autowired
    SettingsPage settingsPage;

    @Then("^I see \"([^\"]*)\" warning under VCS account field")
    public void iSeeWarningUnderVCSAccountField(String warning) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(10, settingsPage.VCSAccountWarning.getWrappedElement());
            if (settingsPage.VCSAccountWarning.isDisplayed()) {
                if (warning.equals("")) {
                    ReportService.reportAction("Warning field appeared.", true);
                } else {
                    String s = settingsPage.VCSAccountWarning.getText().toLowerCase();
                    ReportService.reportAction("\"" + warning + "\" warning field appeared.", s.equals(warning.toLowerCase()));
                }
            } else {
                ReportService.reportAction("Warning field didn't appear.", false);
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see \"([^\"]*)\" warning under TTS account field")
    public void iSeeWarningUnderTTSAccountField(String warning) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(10, settingsPage.jiraAccountWarning.getWrappedElement());
            if (settingsPage.jiraAccountWarning.isDisplayed()) {
                if (warning.equals("")) {
                    ReportService.reportAction("Warning field appeared.", true);
                } else {
                    String s = settingsPage.jiraAccountWarning.getText().toLowerCase();
                    ReportService.reportAction("\"" + warning + "\" warning field appeared.", s.equals(warning.toLowerCase()));
                }
            } else {
                ReportService.reportAction("Warning field didn't appear.", false);
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
