package stepdefinition.ProjectDataEditing;

import com.jcraft.jsch.*;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.*;
import cucumber.api.DataTable;
import cucumber.api.java.en.*;
import helpers.CommonHelper;
import arp.CucumberArpReport;
import arp.ReportService;
import helpers.DBHelper;
import helpers.SystemHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.*;
import pages.base.PageInstance;
import pages.relime.DashboardPage;
import pages.relime.SettingsPage;

import java.security.Permissions;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.*;
import java.util.Random;
import java.util.regex.Pattern;

//import java.sql.Connection;
//import java.sql.DriverManager;


/**
 * Created by kozlov on 6/24/2016.
 */
public class CreateNewProject extends PageInstance {

    @Autowired
    SettingsPage settingsPage;

    @Autowired
    DashboardPage dashboardPage;

    @And("^I see a notification message \"([^\"]*)\" under second field")
    public void iSeeNotificationMessages(String message) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(2, dashboardPage.SecondFieldWarning.getWrappedElement());
            boolean WarningVisible = false;
            if(dashboardPage.SecondFieldWarning.isDisplayed()&&dashboardPage.SecondFieldWarning.getText().equals(message)) {
                WarningVisible = true;
            }
            ReportService.reportAction("Notification appeared.", WarningVisible);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
    /*default = publicProjectOnPublicDomain
    sDefault = defaultSmokeProject
    rDefaultOne = defaultRegressionProjectOne
    rDefaultTwo = defaultRegressionProjectTwo*/
    @And("^I check that project \"([^\"]*)\" is absent in DB$")
    public void iCheckThatProjectIsAbsentInDB(String Key) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            DBHelper.executeStatement("DELETE FROM [RelimeDb].[dbo].[Project] WHERE ProjectKey = '" + Key + "';");
            ReportService.reportAction("Project '" + Key + "' is absent in DB.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    public static void RemoveFolderViaSSH(ChannelSftp session, String directory) throws SftpException {
        Vector v = session.ls(directory);
        for (Object e : v) {
            ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) e;
            entry.getFilename();
            if(entry.getLongname().startsWith("-")){
                session.chmod(511, directory + "/" + entry.getFilename());
                session.rm(directory + "/" + entry.getFilename());
            }
            else{
                RemoveFolderViaSSH(session, directory + "/" + entry.getFilename());
            }
        }
        session.chmod(511, directory);
        session.rmdir(directory);
    }


    public static void RemoveTemporaryFoldersViaSSH(ChannelSftp session, String directory) throws SftpException {
        Vector v = session.ls(directory);
        for (Object e : v) {
            ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) e;
            Pattern p1 = Pattern.compile("F[0-9][0-9][0-9]");
            Pattern p2 = Pattern.compile("F[0-9][0-9]");
            if(p1.matcher(entry.getFilename()).matches()||p2.matcher(entry.getFilename()).matches()){
                RemoveFolderViaSSH(session, directory + "/" + entry.getFilename());
            }
        }
    }

    @And("^I open pop-up 'New Project' in panel 'Projects' in page 'Dashboard'$")
    public void iOpenPopUpNewProject() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Pop-up 'New Project' in panel 'Projects' was opened.", dashboardPage.openPopUpNewProject());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I fill in data in pop-up 'New project'$")
    public void iFillInDataInPopUpNewProject(DataTable table) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            dashboardPage.fillInDataForProject(table);
            ReportService.reportAction("'Feature' textbox in pop-up 'New feature' was filled with data.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that characters in 'Project key' field in pop-up 'New Project' are in upper-case$")
    public void iSeeThatCharactersInProjectKeyAreInUpperCase() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean isUpperScale = false;
            for (Element e : dashboardPage.formFields) {
                if(e.findElement(By.xpath(dashboardPage.FormFieldName)).getText().toLowerCase().contains("project key")){
                    String value = e.findElement(By.xpath(dashboardPage.AddProjectFormFieldInput)).getText();
                    isUpperScale = e.findElement(By.xpath(dashboardPage.AddProjectFormFieldInput)).getText().toUpperCase().equals(value);
                }
            }
            ReportService.reportAction("'Project key' is in upper case.", isUpperScale);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I create project in page 'Dashboard'$")
    public void iCreateFeature(DataTable table) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Pop-up 'New Project' in panel 'Projects' was opened.",dashboardPage.openPopUpNewProject());
            dashboardPage.fillInDataForProject(table);
            settingsPage.clickButtonOnPopUp("Create");
            settingsPage.waitForSuccessNotification("");
            Thread.sleep(1000);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that the project was successfully deleted")
    public void iSeeProjectIsDeleted() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean elementFound = false;
            if(dashboardPage.findRowInProjectPanelByProjectKey(CommonHelper.testProjectKey)!=null){
                elementFound = true;
            }
            ReportService.reportAction("Project is deleted from the table 'Projects'.", !elementFound);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that project is added to panel 'Projects'")
    public void iSeeProjectDataIsCorrect() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean elementFound = false;
            if(dashboardPage.findRowInProjectPanelByProjectKey(CommonHelper.testProjectKey)!=null){
                elementFound = true;
            }
            ReportService.reportAction("Created project is in the table.", elementFound);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I select project in panel 'Projects' in page \"Dashboard\"$")
    public void iSelectProjectInPanelProjects() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Element row = dashboardPage.findRowInProjectPanelByProjectKey(CommonHelper.testProjectKey);
            boolean elementFound = false;
            if(row!=null){
                row.findElement(By.xpath(dashboardPage.EDIT_BUTTON)).click();
                elementFound = true;
                ngDriver.waitForAngularRequestsToFinish();
                Thread.sleep(1000);
                ngDriver.waitForAngularRequestsToFinish();
                Thread.sleep(1000);
                ngDriver.waitForAngularRequestsToFinish();
            }
            ReportService.reportAction("Project selected from the table.", elementFound);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I select project \"([^\"]*)\" in panel 'Projects' in page \"Dashboard\"$")
    public void iSelectProjectInPanelProjects(String project) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(project.equals("public"))
                project = CommonHelper.publicProject;
            if(project.equals("private"))
                project = CommonHelper.privateProject;
            if(project.equals("sDefault"))
                project = SystemHelper.DEFAULTSMOKEPROJECT;
            if(project.equals("rDefaultOne"))
                project = SystemHelper.DEFAULTREGRESSIONPROJECTONE;
            if(project.equals("rDefaultTwo"))
                project = SystemHelper.DEFAULTREGRESSIONPROJECTTWO;
            Element row = dashboardPage.findRowInProjectPanelByProjectKey(project);
            boolean elementFound = false;
            if(row!=null){
                try {
                    row.findElement(By.xpath(dashboardPage.EDIT_BUTTON)).click();
                }catch (Error e){
                    row.click();
                }
                elementFound = true;
                CommonHelper.testProjectKey = project;
                ngDriver.waitForAngularRequestsToFinish();
                Thread.sleep(1000);
                ngDriver.waitForAngularRequestsToFinish();
                Thread.sleep(1000);
                ngDriver.waitForAngularRequestsToFinish();
            }
            ReportService.reportAction("Project selected from the table.", elementFound);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I delete project in panel 'Projects' in page \"Dashboard\"$")
    public void iDeleteProjectInPanelProjects() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Element row = dashboardPage.findRowInProjectPanelByProjectKey(CommonHelper.testProjectKey);
            boolean elementFound = false;
            if(row!=null){
                elementFound = true;
                row.findElement(By.xpath(dashboardPage.DELETE_BUTTON)).click();
            }
            Thread.sleep(1000);
            dashboardPage.clickButtonOnPopUp("Delete");
            ReportService.reportAction("Project deleted from the table.", elementFound);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I select item \"([^\"]*)\" under drop-down 'Name' in pop-up \"([^\"]*)\"$")
    public void iSelectItemUnderDropdownNameInPopup(String arg0, String popup) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean elementFound = false;
            for (Element e : settingsPage.formFields) {
                if(e.findElement(By.xpath(settingsPage.FormFieldName)).getText().contains("NAME")){
                    e.findElement(By.xpath(settingsPage.AddProjectFormFieldInput)).click();
                    for (WebElement li : e.findElements(By.xpath(settingsPage.FormFieldDropdownElements))) {
                        if(li.getText().toLowerCase().equals(arg0.toLowerCase())) {
                            li.click();
                            elementFound = true;
                            break;
                        }
                    }
                    break;
                }
            }
            ReportService.reportAction("'" + arg0 + "item was selected under drop-down 'Name' in pop-up '" + popup + "'.", elementFound);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see item \"([^\"]*)\" is absent under drop-down 'Name' in pop-up \"([^\"]*)\"$")
    public void iSeeItemIsAbsentUnderDropdownNameInPopup(String arg0, String popup) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean elementFound = false;
            for (Element e : settingsPage.formFields) {
                if(e.findElement(By.xpath(settingsPage.FormFieldName)).getText().contains("NAME")){
                    e.findElement(By.xpath(settingsPage.AddProjectFormFieldInput)).click();
                    for (WebElement li : e.findElements(By.xpath(settingsPage.FormFieldDropdownElements))) {
                        if(li.getText().toLowerCase().equals(arg0.toLowerCase())) {
                            elementFound = true;
                            break;
                        }
                    }
                    e.findElement(By.xpath(settingsPage.AddProjectFormFieldInput)).submit();
                    break;
                }
            }
            ReportService.reportAction("Element '" + arg0 + "' is absent.", !elementFound);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
