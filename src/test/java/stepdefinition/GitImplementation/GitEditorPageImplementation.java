package stepdefinition.GitImplementation;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import cucumber.api.java.en.*;
import helpers.CommonHelper;
import helpers.SystemHelper;
import org.apache.commons.collections.map.HashedMap;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;
import pages.relime.EditorPage;
import pages.relime.GitHubPage;
import pages.relime.SettingsPage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by kozlov on 7/4/2016.
 */
public class GitEditorPageImplementation extends PageInstance {

    @Autowired
    EditorPage editorPage;

    @Autowired
    GitHubPage gitHubPage;

    @And("^I read initial IDs from database")
    public void iReadInitialIDsFromDB() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            CommonHelper.DBFeaturesTreeElements = gitHubPage.getFeatureIdNameMapFromDatabaseByProjectKey(CommonHelper.testProjectKey);
            ReportService.reportAction("Initial IDs were read database.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see IDs is changed in ng-ispector")
    public void iCanSeeIDsIsChanged() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Map<Integer, String> newTree = gitHubPage.getFeatureIdNameMapFromDatabaseByProjectKey(CommonHelper.testProjectKey);
            boolean a = CommonHelper.DBFeaturesTreeElements!=null;
            if(CommonHelper.DBFeaturesTreeElements!=null&&CommonHelper.DBFeaturesTreeElements.size() == newTree.size()) {
                boolean IDsChanged = true;
                for (Map.Entry e : CommonHelper.DBFeaturesTreeElements.entrySet()) {
                    if(!newTree.keySet().contains(e.getKey())||!newTree.get(e.getKey()).equals(e.getValue())){
                        IDsChanged = false;
                        break;
                    }
                }
                ReportService.reportAction("All IDs changed in the database.", IDsChanged);
            }
            else{
                ReportService.reportAction("Number of elements in the database changed", false);
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I read folder structure from GIT \"([^\"]*)\"")
    public void iReadFolderStructureFromGit(String url) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(url.equals("default")) {
                url = SystemHelper.DEFAULTGIT;
            }
            if(url.equals("smoke")) {
                url = SystemHelper.SMOKEGIT;
            }
            if(url.contains("github")) {
                url += "tree/master/src/test/resources/feature";
                CommonHelper.gitHubFileTree = gitHubPage.getFolderStructureFromGit(url);
            }
            else if(url.contains("bitbucket")) {
                url += "src/";
                CommonHelper.gitHubFileTree = gitHubPage.getFolderStructureFromBitbucket(url);
            }
            ReportService.reportAction("Folder structure was read from Git.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I read folder structure from Editor page")
    public void iReadFolderStructureFromEditorPage() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            CommonHelper.editorPageFileTree = editorPage.getFolderStructureFromEditPage();
            ReportService.reportAction("Folder structure was read from Editor Page.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I check that GitHub and ReLime trees are the same")
    public void iCheckThatTreesAreTheSame() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("GitHub file tree is the same as ReLime tree", CommonHelper.gitHubFileTree.equals(CommonHelper.editorPageFileTree));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see that structures in scenario accordion for scenario \"([^\"]*)\" and Git file \"([^\"]*)\" are the same")
    public void iCanSeeThatStructuresInScenarioAccordionAndGitFileAreTheSame(String scenario, String url) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(url.contains("[default]")){
                url = url.replace("[default]", SystemHelper.DEFAULTGIT);
            }
            boolean contains = false;
            ArrayList<String> scenarioLines = new ArrayList<>();
            for (Element e : editorPage.scenarioStepLines) {
                scenarioLines.add(e.getText());
            }
            ArrayList<String> fileLines = new ArrayList<>();
            if(url.contains("github")) {
                fileLines = gitHubPage.readContentsOfTheGitFile(url);
            }
            else if(url.contains("bitbucket")) {
                fileLines = gitHubPage.readContentsOfTheBitbucketFile(url);
            }
            outer:
            for (int i = 0; i < fileLines.size(); i++) {
                if(fileLines.get(i).contains(scenario)){
                    contains = true;
                    i++;
                    for (int j = 0; j < scenarioLines.size(); j++, i++) {
                        if(!scenarioLines.get(j).equals(fileLines.get(i))) {
                            contains = false;
                            break outer;
                        }
                    }
                }
            }
            ReportService.reportAction("Structures in scenario accordion for scenario and Git file are the same.", contains);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see that tags are present for scenarios and feature in Git file \"([^\"]*)\"")
    public void iSeeThatTagsArePresentForScenarioInGitFile(String url) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try{
            if(url.contains("[default]")){
                url = url.replace("[default]", SystemHelper.DEFAULTGIT);
            }
            boolean tagsPresent = true;
            ArrayList<String> lines = new ArrayList<>();
            if(url.contains("github")) {
                lines = gitHubPage.readContentsOfTheGitFile(url);
            }
            else if(url.contains("bitbucket")) {
                lines = gitHubPage.readContentsOfTheBitbucketFile(url);
            }
            for (int i = 0; i < lines.size(); i++) {
                String scenario = lines.get(i);
                if(scenario.startsWith("Scenario")){
                    String tag = lines.get(i-1);
                    if(!tag.contains("@SC_")) {
                        tagsPresent = false;
                        break;
                    }
                }
                else if(scenario.startsWith("Feature")){
                    String tag = lines.get(i-1);
                    if(!tag.contains("@ST_")) {
                        tagsPresent = false;
                        break;
                    }
                }
            }
            ReportService.reportAction("Tags are present for scenarios and feature in Git file '" + url + "'.",  tagsPresent);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I delete tags for scenarios and feature in Git file \"([^\"]*)\"")
    public void iDeleteTagsForScenarioInGitFile(String url) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try{
            if(url.contains("[default]")){
                url = url.replace("[default]", SystemHelper.DEFAULTGIT);
            }
            if(url.contains("github"))
                gitHubPage.deleteTagsForGit(url);

            else if(url.contains("bitbucket"))
                gitHubPage.deleteTagsForBitbucket(url);
            ReportService.reportAction("Tags were deleted for git file.",  true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
