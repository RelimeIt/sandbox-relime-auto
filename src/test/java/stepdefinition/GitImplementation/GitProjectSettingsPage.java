package stepdefinition.GitImplementation;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Button;
import cucumber.api.java.en.*;
import helpers.SystemHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;
import pages.relime.EditorPage;
import pages.relime.GitHubPage;
import pages.relime.SettingsPage;

import java.util.regex.Pattern;

/**
 * Created by kozlov on 7/4/2016.
 */
public class GitProjectSettingsPage extends PageInstance {

    @Autowired
    SettingsPage settingsPage;

    @Autowired
    EditorPage editorPage;

    @Autowired
    GitHubPage gitHubPage;

    @Then("^I can see that block 'Version control system' is empty")
    public void iSeeBlockVCSIsEmpty() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("'Version control system' block is empty.", settingsPage.VCSAddressLabel.getText().equals("Enter your repository link"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can't see button 'Load/Reload project from GIT' in block \"Version control system'")
    public void iCantSeeButtonReloadProjectFromGitInBlockVCS() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(10000);
            WebElement e = driver.findElement(By.xpath(".//button[contains(@class, 'btn-download')]"));
            ReportService.reportAction("'Load/Reload project from GIT' button is not displayed in block VCS.", !e.isEnabled());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see button 'Load/Reload project from GIT' in block \"Version control system'")
    public void iCanSeeButtonReloadProjectFromGitInBlockVCS() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(10000);
            WebElement e = driver.findElement(By.xpath(".//button[contains(@class, 'btn-download')]"));
            ReportService.reportAction("'Load/Reload project from GIT' button is displayed in block VCS.", e.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see tooltip \"([^\"]*)\" on button 'Load/Reload project from GIT' in block \"Version control system'")
    public void iCanSeeButtonReloadProjectFromGitInBlockVCS(String tooltip) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(10000);
            WebElement e = driver.findElement(By.xpath(".//button[contains(@class, 'btn-download')]"));
            String s = e.getAttribute("data-uib-tooltip");
            ReportService.reportAction("'Load/Reload project from GIT' button is displayed in block VCS.", e.getAttribute("data-uib-tooltip").equals(tooltip));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I click on button 'Load from Git' on page 'Settings'")
    public void iClickOnButtonLoadFromGit() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitForCustomCondition(5, new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    boolean b = false;
                    try {
                        b = settingsPage.reloadProjectFromGit.isEnabled();
                    } catch (Exception e) {
                    }
                    return b;
                }
            });
            settingsPage.reloadProjectFromGit.click();
            ReportService.reportAction("Button 'Load from Git' was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^Unique tags appear in Git repository \"([^\"]*)\"")
    public void uniqueTagsAppearInGitRepository(String url) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if (url.contains("[default]")) {
                url = url.replace("[default]", SystemHelper.DEFAULTGIT);
            }
            ReportService.reportAction("Unique tags appear in Git repository", gitHubPage.uniqueTagsArePresentInProjectFeatures(url));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see project was uploaded")
    public void iCanSeeProjectUploaded() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, editorPage.firstTreeNode.getWrappedElement());
            ReportService.reportAction("Project was uploaded.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see \"([^\"]*)\" warning under VCS Path field")
    public void iSeeWarningUnderVCSPathField(String warning) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(10, settingsPage.VCSAddressWarning.getWrappedElement());
            if (settingsPage.VCSAddressWarning.isDisplayed()) {
                if (warning.equals("")) {
                    ReportService.reportAction("Warning field appeared.", true);
                } else {
                    String s = settingsPage.VCSAddressWarning.getText().toLowerCase();
                    ReportService.reportAction("\"" + warning + "\" warning field appeared.", s.equals(warning.toLowerCase()));
                }
            } else {
                ReportService.reportAction("Warning field didn't appear.", false);
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
