package stepdefinition;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.*;
import cucumber.api.java.en.*;
import helpers.CommonHelper;
import helpers.DBHelper;
import helpers.SystemHelper;
import arp.CucumberArpReport;
import arp.ReportService;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.*;
import stepdefinition.ProjectDataEditing.CreateNewProject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;

public class CommonStepDefinition extends PageInstance {

    @Autowired
    AdminPage adminPage;

    @Autowired
    DashboardPage dashboardPage;

    @Autowired
    EditorPage editorPage;

    @Autowired
    ProfilePage profilePage;

    @Autowired
    SettingsPage settingsPage;

    @Autowired
    StatisticsPage statisticsPage;

    @Given("^I am logged in as Product Owner$")
    public void iAmLoggedInAsProductOwner() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean flag = true;
            ReportService.reportAction("User was logged in.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    /*default = publicProjectOnPublicDomain
    sDefault = defaultSmokeProject
    rDefaultOne = defaultRegressionProjectOne
    rDefaultTwo = defaultRegressionProjectTwo*/
    @And("^I navigate to page \"([^\"]*)\" for project \"([^\"]*)\"$")
    public void iSelectProjectByURL(String page, String project) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            project = CommonHelper.replaceProjectWithActualProjectKey(project);
            String URL = "";
            Element checkElement = null;
            switch (page) {
                case "Statistics Page":
                    URL = CommonHelper.currentDomain + project + "/activity";
                    checkElement = statisticsPage.generalProjectStatisticsLabel;
                    break;
                case "Editor":
                    URL = CommonHelper.currentDomain + project + "/editor";
                    checkElement = editorPage.addNewFolderButton;
                    break;
                case "Project Settings":
                    URL = CommonHelper.currentDomain + project + "/settings";
                    checkElement = settingsPage.NameField;
                    break;
                case "Feature Management":
                    URL = CommonHelper.currentDomain + project + "/management";
                    checkElement = editorPage.saveChangesToGit;
                    break;
                default:
                    ReportService.reportAction("No page found - '" + page + "'.", false);
                    break;
            }
            if(project.equals("WRONG")){
                checkElement = dashboardPage.dashboardElement;
            }
            CommonHelper.testProjectKey = project;
            CommonHelper.navigateToPage(URL, checkElement);
            Thread.sleep(4000);
            CommonHelper.testProjectKey = project;
            ReportService.reportAction("Project was selected.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I select feature \"([^\"]*)\" in the tree$")
    public void iSelectFeatureInTheTree(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(arg0.equals("jira"))
                arg0 = SystemHelper.DEFAULTJIRALINKEDFEATURE;
            editorPage.selectFeatureByName(arg0);
            Thread.sleep(1000);
            ReportService.reportAction("'" + arg0 + "' feature was selected.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click on button \"([^\"]*)\" on page")
    public void clickButton(String name) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(1000);
            editorPage.clickButton(name);
            ReportService.reportAction("Button '" + name + "' was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see a notification message \"([^\"]*)\" under \"([0-9])\" required fields on pop-up \"([^\"]*)\"")
    public void iSeeNotificationMessages(String message, int quantity, String form) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(1000);
            int messages = 0;
            if (dashboardPage.formFieldWarnings != null && dashboardPage.formFieldWarnings.size() > 0) {
                if (!message.equals("")) {
                    for (Label e : dashboardPage.formFieldWarnings) {
                        if (e.isDisplayed() && e.getText().equals(message))
                            messages++;
                    }
                } else {
                    for (Label e : dashboardPage.formFieldWarnings) {
                        if (e.isDisplayed())
                            messages++;
                    }
                }
            }
            ReportService.reportAction(quantity + "'" + message + "' notifications are displayed.", messages == quantity);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I see a notification message \"([^\"]*)\" under \"([0-9])\" required fields on Editor page")
    public void iSeeNotificationMessagesUnderTheTree(String message, int quantity) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(1000);
            int messages = 0;
            if (dashboardPage.EditorFieldWarnings != null && dashboardPage.EditorFieldWarnings.size() > 0) {
                if (!message.equals("")) {
                    for (Label e : dashboardPage.EditorFieldWarnings) {
                        String s = e.getText();
                        if (e.isDisplayed() && e.getText().equals(message))
                            messages++;
                    }
                } else {
                    for (Label e : dashboardPage.EditorFieldWarnings) {
                        if (e.isDisplayed())
                            messages++;
                    }
                }
            }
            ReportService.reportAction(quantity + "'" + message + "' notifications are displayed.", messages == quantity);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I am signed in as \"([^\"]*)\"")
    public void iAmSignedIn(String user) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(3000);
            user = CommonHelper.replaceUserWithActualEmail(user);
            boolean signInElementDisplayed = false;
            try {
                signInElementDisplayed = dashboardPage.signInButton.isDisplayed();
            }catch (Exception e){}
            if(signInElementDisplayed) {
                CommonHelper.clickWithActions(editorPage.signInButton.getWrappedElement());
                Waiters.waitAppearanceOf(120, editorPage.signInDialog.getWrappedElement());
                Thread.sleep(1000);
                editorPage.signInDialogEmailField.sendKeys(user);
                Thread.sleep(500);
                editorPage.signInDialogPasswordField.sendKeys(SystemHelper.PASSWORD);
                Thread.sleep(500);
                editorPage.signInDialogSignInButton.click();
                Waiters.waitDisappearsOf(10, editorPage.signInDialog.getWrappedElement(), 2);
                ngDriver.waitForAngularRequestsToFinish();
            }
            else{
                driver.get(SystemHelper.URL + "profile");
                ngDriver.waitForAngularRequestsToFinish();
                Waiters.waitAppearanceOf(20, profilePage.profileUserEmail.getWrappedElement());
                if(!profilePage.profileUserEmail.getText().equals(user)){
                    profilePage.profileDropdownButton.click();
                    Thread.sleep(500);
                    profilePage.profileDropdownElements.get(1).click();
                    ngDriver.waitForAngularRequestsToFinish();
                    Waiters.waitAppearanceOf(10, profilePage.signInButton.getWrappedElement());
                    profilePage.signInButton.click();
                    Waiters.waitAppearanceOf(120, editorPage.signInDialog.getWrappedElement());
                    Thread.sleep(1000);
                    profilePage.signInDialogEmailField.sendKeys(user);
                    Thread.sleep(500);
                    profilePage.signInDialogPasswordField.sendKeys(SystemHelper.PASSWORD);
                    Thread.sleep(500);
                    profilePage.signInDialogSignInButton.click();
                    Waiters.waitDisappearsOf(10, editorPage.signInDialog.getWrappedElement(), 2);
                    ngDriver.waitForAngularRequestsToFinish();
                }
            }
            ReportService.reportAction("Signed in successfully.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I sign out")
    public void iSignOut() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean dashboardElementDisplayed = false;
            boolean profileElementDisplayed = false;
            try {
                dashboardElementDisplayed = dashboardPage.dashboardDropdownButton.isDisplayed();
            }catch (Exception e){}
            try {
                profileElementDisplayed = dashboardPage.profileDropdownButton.isDisplayed();
            }catch (Exception e){}
            if(dashboardElementDisplayed) {
                dashboardPage.dashboardDropdownButton.click();
                Thread.sleep(1000);
                for (WebElement e : dashboardPage.dashboardDropdownElements) {
                    if (e.getText().toLowerCase().equals("sign out")) {
                        e.click();
                        ngDriver.waitForAngularRequestsToFinish();
                        Thread.sleep(3000);
                        break;
                    }
                }
            }
            else if(profileElementDisplayed){
                profilePage.profileDropdownButton.click();
                Thread.sleep(500);
                profilePage.profileDropdownElements.get(1).click();
                ngDriver.waitForAngularRequestsToFinish();
            }
            Waiters.waitAppearanceOf(10, profilePage.signInButton.getWrappedElement());
            ReportService.reportAction("Signed out successfully.", profilePage.signInButton.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I am in page \"([^\"]*)\"$")
    public void iAmInPage(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean match = false;
            switch (arg0) {
                case "Profile":
                    ReportService.reportAction("User is in page '" + arg0 + "'.", profilePage.profileUserInfo.isDisplayed());
                    break;
                case "Default":
                    ReportService.reportAction("User is in page '" + arg0 + "'.", dashboardPage.pageHeader.isDisplayed());
                    break;
                case "About":
                    Thread.sleep(4000);
                    ReportService.reportAction("User is in page '" + arg0 + "'.", driver.getCurrentUrl().endsWith("/about"));
                    break;
                case "Dashboard":
                    ReportService.reportAction("User is in page '" + arg0 + "'.", dashboardPage.dashboardElement.isDisplayed());
                    break;
                case "Statistics Page":
                    ReportService.reportAction("User is in page '" + arg0 + "'.", statisticsPage.generalProjectStatisticsLabel.isDisplayed());
                    break;
                case "Editor":
                    ReportService.reportAction("User is in page '" + arg0 + "'.", editorPage.storiesExplorer.isDisplayed());
                    break;
                case "Project Settings":
                    ReportService.reportAction("User is in page '" + arg0 + "'.", settingsPage.NameField.isDisplayed());
                    break;
                case "Feature Management":
                    ReportService.reportAction("User is in page '" + arg0 + "'.", editorPage.saveChangesToGit.isDisplayed());
                    break;
                default:
                    if(!match) {
                        ReportService.reportAction("No page found - '" + arg0 + "'.", false);
                        break;
                    }
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Given("^I navigate to page \"([^\"]*)\"$")
    public void iNavigateToPage(String arg0) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            String URL = "";
            Element checkElement = null;
            switch (arg0) {
                case "Default":
                    URL = SystemHelper.URL;
                    checkElement = dashboardPage.pageHeader;
                    break;
                case "Dashboard":
                    URL = CommonHelper.currentDomain + "dashboard";
                    checkElement = dashboardPage.dashboardElement;
                    break;
                case "Statistics Page":
                    URL = CommonHelper.currentDomain + CommonHelper.testProjectKey + "/activity";
                    checkElement = statisticsPage.generalProjectStatisticsLabel;
                    break;
                case "Editor":
                    URL = CommonHelper.currentDomain + CommonHelper.testProjectKey + "/editor";
                    checkElement = editorPage.addNewFolderButton;
                    break;
                case "Project Settings":
                    URL = CommonHelper.currentDomain + CommonHelper.testProjectKey + "/settings";
                    checkElement = settingsPage.NameField;
                    break;
                case "Feature Management":
                    URL = CommonHelper.currentDomain + CommonHelper.testProjectKey + "/management";
                    checkElement = editorPage.saveChangesToGit;
                    break;
                case "Profile":
                    URL = SystemHelper.URL + "profile";
                    checkElement = profilePage.profileUserInfo;
                    break;
                default:
                        ReportService.reportAction("No page found - '" + arg0 + "'.", false);
                        break;
            }
            try {
                ReportService.reportAction("User navigated to '" + arg0 + "' page.", CommonHelper.navigateToPage(URL, checkElement));
            } catch (Exception ex) {
                ReportService.reportAction("User couldn't navigate to '" + arg0 + "' page. Inner exception: " + ex.getMessage(), false);
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see pop up \"([^\"]*)\" is opened")
    public void popUpWindowIsOpened(String name) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(15, settingsPage.formHeader.getWrappedElement());
            Thread.sleep(1000);
            boolean popupAppeared = settingsPage.formHeader.isDisplayed();
            boolean popupNameMatches = true;
            if (!name.equals("")) {
                popupNameMatches = settingsPage.formHeader.getText().toLowerCase().equals(name.toLowerCase());
            }
            ReportService.reportAction(name + " popup window is opened.", popupAppeared && popupNameMatches);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see pop up message equals \"([^\"]*)\"")
    public void popUpMessageEquals(String message) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(3, settingsPage.formHeader.getWrappedElement());
            boolean popupAppeared = settingsPage.formHeader.isDisplayed();
            boolean popupNameMatches = settingsPage.formMessage.getText().toLowerCase().equals(message.toLowerCase());
            ReportService.reportAction("Popup message equals'" + message + "'.", popupAppeared && popupNameMatches);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I type \"([^\"]*)\" into textbox \"([^\"]*)\" in pop-up \"([^\"]*)\"")
    public void iTypeTextIntoTextboxInPoUp(String value, String field, String popup) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(500);
            if(value.contains("quote"))
                value = "\"";
            boolean elementFound = false;
            for (PlaceHolder e : dashboardPage.formFields) {
                if (e.findElement(By.xpath(dashboardPage.FormFieldName)).getText().toLowerCase().contains(field.toLowerCase())) {
                    e.findElement(By.xpath(dashboardPage.AddProjectFormFieldInput)).clear();
                    e.findElement(By.xpath(dashboardPage.AddProjectFormFieldInput)).sendKeys(value);
                    elementFound = true;
                    break;
                }
            }
            ReportService.reportAction("Textbox '" + field + "' value was changed to '" + value + "' in pop-up '" + popup + "'.", elementFound);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I select item \"([^\"]*)\" in drop-down \"([^\"]*)\" in pop-up \"([^\"]*)\"")
    public void iSelectValueInDropdownInPoUp(String value, String field, String popup) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(500);
            boolean elementFound = false;
            for (PlaceHolder e : dashboardPage.formFields) {
                if (e.findElement(By.xpath(dashboardPage.FormFieldName)).getText().toLowerCase().contains(field.toLowerCase())) {
                    e.findElement(By.xpath(dashboardPage.AddProjectFormFieldInput)).click();
                    for (WebElement listElement : e.findElements(By.xpath(dashboardPage.FormFieldDropdownElements))) {
                        String s = listElement.getText();
                        if (s.equals(value)) {
                            listElement.click();
                            elementFound = true;
                            break;
                        }
                    }
                }
            }
            ReportService.reportAction("Dropdown '" + field + "' item '" + value + "' was selected in pop-up '" + popup + "'.", elementFound);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click on button \"([^\"]*)\" on pop-up \"([^\"]*)\"")
    public void iClickButtonOnPopUp(String button, String popup) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            settingsPage.clickButtonOnPopUp(button);
            ReportService.reportAction("\"" + button + "\" button was clicked", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see button \"([^\"]*)\" on pop-up \"([^\"]*)\"")
    public void iSeeButtonOnPopUp(String button, String popup) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(1000);
            boolean buttonExists = false;
            for (Button e : settingsPage.FormButtons) {
                String s = e.getText();
                if (s.toLowerCase().equals(button.toLowerCase())) {
                    buttonExists = true;
                }
            }
            Thread.sleep(1000);
            ReportService.reportAction("\"" + button + "\" button was clicked", buttonExists);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click on 'Select All' checkbox on pop-up \"([^\"]*)\"")
    public void iClickOnSelectAllCheckboxOnPopUp(String popup) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see success notification \"([^\"]*)\"")
    public void iSeeSuccessNotification(String text) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Notification window appeared.", settingsPage.waitForSuccessNotification(text));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see error notification \"([^\"]*)\"")
    public void iSeeErrorNotification(String text) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Notification window appeared.", settingsPage.waitForErrorNotification(text));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see info notification \"([^\"]*)\"")
    public void iSeeInfoNotification(String text) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(10, settingsPage.infoNotificationWindow.getWrappedElement());
            if (settingsPage.infoNotificationWindow.isDisplayed()) {
                if (text.equals("")) {
                    ReportService.reportAction("Notification window appeared.", true);
                } else {
                    String s = settingsPage.infoNotificationWindow.getText();
                    ReportService.reportAction("\"" + text + "\" notification window appeared.", s.equals(text));
                }
            } else {
                ReportService.reportAction("Notification window appeared.", false);
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see warning notification \"([^\"]*)\"")
    public void iSeeWarningNotification(String text) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(30, settingsPage.warningNotificationWindow.getWrappedElement());
            if (settingsPage.warningNotificationWindow.isDisplayed()) {
                if (text.equals("")) {
                    ReportService.reportAction("Warning window appeared.", true);
                } else {
                    String s = settingsPage.warningNotificationWindow.getText();
                    ReportService.reportAction("\"" + text + "\" warning window appeared.", s.equals(text));
                }
            } else {
                ReportService.reportAction("Notification window appeared.", false);
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I navigate to URL \"([^\"]*)\"")
    public void iNavigateToURL(String url) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            driver.navigate().to(url);
            ngDriver.waitForAngularRequestsToFinish();
            ReportService.reportAction("Navigated to page '" + url + "' successfully.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I save current link")
    public void iSelectFeatureByDirectLink() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            CommonHelper.savedURL = driver.getCurrentUrl();
            ReportService.reportAction("Current URL '" + CommonHelper.savedURL + "' saved successfully.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I navigate by saved link")
    public void iSelectScenarionByDirectLink() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            driver.navigate().to(CommonHelper.savedURL);
            ngDriver.waitForAngularRequestsToFinish();
            ReportService.reportAction("Navigated saved URL '" + CommonHelper.savedURL + "' successfully.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that dashboard page is displayed$")
    public void dashboardPageIsDisplayed() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(CommonHelper.delay, dashboardPage.dashboardElement.getWrappedElement());
            ReportService.reportAction("User navigated to 'Dashboard' page.", dashboardPage.dashboardElement.isDisplayed() && driver.getCurrentUrl().endsWith("/dashboard"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I navigate to admin page$")
    public void iNavigateToAdminPage() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            driver.get(SystemHelper.URL.substring(0, SystemHelper.URL.length() - 1) + ":35353/");
            ngDriver.waitForAngularRequestsToFinish();
            Waiters.waitAppearanceOf(20, adminPage.adminPageHeader.getWrappedElement());
            ReportService.reportAction("User navigated to admin page.", adminPage.adminPageHeader.isDisplayed() && driver.getCurrentUrl().startsWith(SystemHelper.URL.substring(0, SystemHelper.URL.length() - 1) + ":35353/"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I am signed in on admin page$")
    public void iSignInOnAdminPage() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, adminPage.adminPageHeader.getWrappedElement());
            if(adminPage.adminLogInButton.isDisplayed()) {
                adminPage.adminLoginField.clear();
                adminPage.adminLoginField.sendKeys("mail@example.com");
                adminPage.adminPasswordField.clear();
                adminPage.adminPasswordField.sendKeys("$implepass!");
                adminPage.adminLogInButton.click();
                Waiters.waitAppearanceOf(5, adminPage.allInvoicesTable.getWrappedElement());
            }
            ReportService.reportAction("User signed in on admin page.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that profile page is displayed$")
    public void profilePageIsDisplayed() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(CommonHelper.delay, profilePage.profileUserInfo.getWrappedElement());
            ReportService.reportAction("User navigated to 'Dashboard' page.", profilePage.profileUserInfo.isDisplayed() && driver.getCurrentUrl().endsWith("/profile"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I clean up$")
    public void cleanup() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(SystemHelper.SERVERLOGIN, SystemHelper.DATABASEIP, 22);
            session.setPassword(SystemHelper.SERVERPASS);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
            ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
            sftpChannel.connect();
            try {
                if(!sftpChannel.isConnected())
                    sftpChannel.connect();
                DBHelper.executeStatement("DELETE FROM [RelimeDb].[dbo].[Project] WHERE ProjectKey LIKE 'F[0-9][0-9][0-9]' OR ProjectKey LIKE 'F[0-9][0-9]';");
                String domain = SystemHelper.PUBLIC_DOMAIN.substring(SystemHelper.PUBLIC_DOMAIN.indexOf("//") + 2, SystemHelper.PUBLIC_DOMAIN.indexOf("."));
                CreateNewProject.RemoveTemporaryFoldersViaSSH(sftpChannel, "/C:/Saved repositories/" + domain);
                domain = SystemHelper.PRIVATE_DOMAIN.substring(SystemHelper.PRIVATE_DOMAIN.indexOf("//") + 2, SystemHelper.PRIVATE_DOMAIN.indexOf("."));
                CreateNewProject.RemoveTemporaryFoldersViaSSH(sftpChannel, "/C:/Saved repositories/" + domain);
            }
            catch (Exception e){
                String s = e.getMessage();
            }
            sftpChannel.disconnect();
            session.disconnect();
            ReportService.reportAction("Cleanup ended.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I clean up project \"([^\"]*)\"$")
    public void cleanupProject(String project) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(project.equals("public"))
                project = CommonHelper.publicProject;
            if(project.equals("private"))
                project = CommonHelper.privateProject;
            if(project.equals("sDefault"))
                project = SystemHelper.DEFAULTSMOKEPROJECT;
            if(project.equals("rDefaultOne"))
                project = SystemHelper.DEFAULTREGRESSIONPROJECTONE;
            if(project.equals("rDefaultTwo"))
                project = SystemHelper.DEFAULTREGRESSIONPROJECTTWO;
            JSch jsch = new JSch();
            Session session = jsch.getSession(SystemHelper.SERVERLOGIN, SystemHelper.DATABASEIP, 22);
            session.setPassword(SystemHelper.SERVERPASS);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
            ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
            sftpChannel.connect();
            try {
                if(!sftpChannel.isConnected())
                    sftpChannel.connect();
                String domain = SystemHelper.PUBLIC_DOMAIN.substring(SystemHelper.PUBLIC_DOMAIN.indexOf("//") + 2, SystemHelper.PUBLIC_DOMAIN.indexOf("."));
                CreateNewProject.RemoveFolderViaSSH(sftpChannel, "/C:/Saved repositories/" + domain + "/" + project);
                domain = SystemHelper.PRIVATE_DOMAIN.substring(SystemHelper.PRIVATE_DOMAIN.indexOf("//") + 2, SystemHelper.PRIVATE_DOMAIN.indexOf("."));
                CreateNewProject.RemoveFolderViaSSH(sftpChannel, "/C:/Saved repositories/" + domain + "/" + project);
            } catch (Exception e)
            {String s = e.getMessage();}
            sftpChannel.disconnect();
            session.disconnect();
            ReportService.reportAction("Cleanup ended.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I test staff$")
    public void test() throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            WebElement e = driver.findElement(By.xpath(".//span[@id='recaptcha-anchor']"));
            e.click();
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
