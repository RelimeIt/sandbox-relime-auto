package stepdefinition.Dashboard;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import cucumber.api.java.en.And;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;

/**
 * Created by kozlov on 11/9/2016.
 */
public class PublicDashboardForNonSubscribedUser extends PageInstance {

    @Autowired
    DashboardPage dashboardPage;

    @And("^I can see explanation text \"([^\"]*)\"")
    public void iCanSeeExplanationText(String text) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.greetingBlockText.getWrappedElement());
            String s = dashboardPage.greetingBlockText.getText();
            s = s.replace("\n", " ");
            ReportService.reportAction("Explanation text  " + text + " is displayed.", s.equals(text));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see 'Sign in' button in Greetings block")
    public void iCanSeeSignInButton() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.greetingBlockSignInButton.getWrappedElement());
            ReportService.reportAction("'Sing in' button is displayed.", dashboardPage.greetingBlockSignInButton.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see 'Sign in' dialog is opened")
    public void iCanSeeSignInDialog() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.signInDialog.getWrappedElement());
            ReportService.reportAction("'Sing in' button was clicked.", dashboardPage.signInDialog.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click 'Sign in' button in Greetings block")
    public void iclickSignInButton() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(10, dashboardPage.greetingBlockSignInButton.getWrappedElement());
            dashboardPage.greetingBlockSignInButton.click();
            ReportService.reportAction("'Sing in' button was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click 'Sign in' button in Top panel")
    public void iclickSignInButtonInTopPanel() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(10, dashboardPage.signInButton.getWrappedElement());
            dashboardPage.greetingBlockSignInButton.click();
            ReportService.reportAction("'Sing in' button was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see 'Create an account' link in Greetings block")
    public void iCanSeeCreateAnAccountLink() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.greetingBlockCreateAnAccountLink.getWrappedElement());
            ReportService.reportAction("'Create an account' button is displayed.", dashboardPage.greetingBlockCreateAnAccountLink.isDisplayed()&&dashboardPage.greetingBlockCreateAnAccountLink.getText().contains("Create an account"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click 'Create an account' link in Greetings block")
    public void iclickCreateAnAccountLink() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.greetingBlockCreateAnAccountLink.getWrappedElement());
            dashboardPage.greetingBlockCreateAnAccountLink.click();
            ReportService.reportAction("'Careate an account' button was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see 'Careate an account' dialog is opened")
    public void iCanSeeCreateAnAccountDialog() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.createAnAccountDialog.getWrappedElement());
            ReportService.reportAction("'Careate an account' dialog is displayed.", dashboardPage.createAnAccountDialog.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click 'Profile page' link in Greetings block")
    public void iclickProfilePageLink() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.profilePageLink.getWrappedElement());
            dashboardPage.profilePageLink.click();
            ReportService.reportAction("'Profile page' button was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see 'Create domain' button in Greetings block")
    public void iCanSeeCreateDomainButton() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.greetingBlockCreateDomainButton.getWrappedElement());
            ReportService.reportAction("'Sing in' button is displayed.", dashboardPage.greetingBlockCreateDomainButton.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click 'Create domain' button in Greetings block")
    public void iClickCreateDomainButton() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.greetingBlockCreateDomainButton.getWrappedElement());
            dashboardPage.greetingBlockCreateDomainButton.click();
            ReportService.reportAction("'Sing in' button was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see 'Careate domain' dialog is opened")
    public void iCanSeeCreateDomainDialog() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.createDomainDialog.getWrappedElement());
            ReportService.reportAction("'Careate domain' dialog is displayed.", dashboardPage.createDomainDialog.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see 'Learn more about Relime' link in Greetings block")
    public void iCanSeeLearnMoreLink() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.learnMoreLink.getWrappedElement());
            ReportService.reportAction("'Sing in' button is displayed.", dashboardPage.learnMoreLink.isDisplayed()&&dashboardPage.learnMoreLink.getText().equals("Learn more about Relime"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click 'Learn more about Relime' link in Greetings block")
    public void iclickLearnMoreLink() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.learnMoreLink.getWrappedElement());
            dashboardPage.learnMoreLink.click();
            ReportService.reportAction("'Sing in' button was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
