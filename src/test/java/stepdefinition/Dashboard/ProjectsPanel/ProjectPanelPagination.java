package stepdefinition.Dashboard.ProjectsPanel;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import cucumber.api.java.en.Then;
import helpers.CommonHelper;
import org.apache.commons.lang3.math.NumberUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;

import java.util.List;

/**
 * Created by kozlov on 10/26/2016.
 */
public class ProjectPanelPagination extends PageInstance {

    @Autowired
    DashboardPage dashboardPage;

    @Then("^I can see only 10 projects on page")
    public void iCanSeeOnlyTenProjects() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Only 10 projects are displayed.", dashboardPage.projectTableRows.size()<=10);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see navigation panel under list of projects")
    public void iCanSeeNavigationPanel() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Navigation panel is displayed.", dashboardPage.projectTablePages.size() > 0);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that pagination panel is displayed correctly in Projects table")
    public void iCanSeeNavigationPanelIsCorrect() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean correct = true;
            if(!dashboardPage.projectTablePages.get(0).getText().equals("&lt;"))
                correct = false;
            if(!dashboardPage.projectTablePages.get(dashboardPage.projectTablePages.size() - 1).getText().equals("&gt;"))
                correct = false;
            for (int i = 1; i < dashboardPage.projectTablePages.size() - 1; i++) {
                if(dashboardPage.projectTablePages.get(i).getText().equals("...")){
                    try{
                        Integer.parseInt(dashboardPage.projectTablePages.get(i - 1).getText());
                        Integer.parseInt(dashboardPage.projectTablePages.get(i + 1).getText());
                    }
                    catch(NumberFormatException nfe)
                    {
                        correct = false;
                        break;
                    }
                }
                else {
                    try {
                        Integer.parseInt(dashboardPage.projectTablePages.get(i).getText());
                    } catch (NumberFormatException nfe) {
                        correct = false;
                        break;
                    }
                }
            }
            ReportService.reportAction("Pagination panel is displayed correctly.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that pagination element is absent in Projects table")
    public void iCanSeeThatPaginationIsAbsent() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Pagination element is absent in Projects table.", dashboardPage.projectTablePages.size() == 0);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that pagination element is present in Projects table")
    public void iCanSeeThatPaginationIsPresent() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Pagination element is present in Projects table.", dashboardPage.projectTablePages.size() >= 4);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I am in page \"([^\"]*)\" on pagination panel in Projects table")
    public void iAmOnTheRightPage(String page) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.projectTablePages.get(0).getWrappedElement());
            boolean buttonFound = false;
            if(page.equals("last")) {
                for (int i = 0; i < dashboardPage.projectTablePages.size(); i++){
                    Element e = dashboardPage.projectTablePages.get(i);
                    if (e.getText().equals("&gt;")) {
                        Element last = dashboardPage.projectTablePages.get(i - 1);
                        if(NumberUtils.isNumber(last.getText()) && last.getAttribute("class").contains("active")) {
                            buttonFound = true;
                            break;
                        }
                    }
                }
            }
            else if(NumberUtils.isNumber(page)){
                for (Element e : dashboardPage.projectTablePages) {
                    if(e.getText().equals(page)&&e.getAttribute("class").contains("active")){
                        buttonFound = true;
                        break;
                    }
                }
            }
            else{
                Throwable t = new Throwable("You should provide a number or 'last' as a parameter");
                throw t;
            }
            ReportService.reportAction("Page " + page + " selected.", buttonFound);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I click button 'Next' on pagination panel in Projects table")
    public void iClickNext() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.projectTablePages.get(dashboardPage.projectTablePages.size() - 1).getWrappedElement());
            dashboardPage.projectTablePages.get(dashboardPage.projectTablePages.size() - 1).click();
            ReportService.reportAction("Button 'Next' clicked on pagination panel", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I click button 'Previous' on pagination panel in Projects table")
    public void iClickPrevious() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.projectTablePages.get(0).getWrappedElement());
            dashboardPage.projectTablePages.get(0).click();
            ReportService.reportAction("Button 'Previous' clicked on pagination panel", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I click button \"([^\"]*)\" on pagination panel in Projects table")
    public void iClickPaginationButton(String button) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.projectTablePages.get(0).getWrappedElement());
            ReportService.reportAction("Button '" + button + "' clicked on pagination panel", CommonHelper.selectElementByText(dashboardPage.projectTablePages, button));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
