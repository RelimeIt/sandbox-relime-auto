package stepdefinition.Dashboard.ProjectsPanel;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import cucumber.api.java.en.*;
import helpers.CommonHelper;
import helpers.DBHelper;
import helpers.SystemHelper;
import helpers.TableVerifiers.ProjectsVerifiers.ProjectsPresentVerifier;
import helpers.TableVerifiers.TableReader;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kozlov on 11/3/2016.
 */
public class ProjectPanleSorting extends PageInstance {

    @Autowired
    DashboardPage dashboardPage;

    @And("^I can see that domain \"([^\"]*)\" contains only \"([^\"]*)\" projects")
    public void iCanSeeThatDomainContainsOnlyRelatedProjects(String domain, String type) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(type.equals("public") || type.equals("private")) {
                if (domain.equals("public")){
                    domain = SystemHelper.PUBLIC_DOMAIN.substring(SystemHelper.PUBLIC_DOMAIN.indexOf("//") + 2, SystemHelper.PUBLIC_DOMAIN.indexOf("."));
                }
                else if (domain.equals("private")){
                    domain = SystemHelper.PUBLIC_DOMAIN.substring(SystemHelper.PRIVATE_DOMAIN.indexOf("//") + 2, SystemHelper.PRIVATE_DOMAIN.indexOf("."));
                }
                else if (domain.equals("default")){
                    domain = CommonHelper.currentDomain.substring(CommonHelper.currentDomain.indexOf("//") + 2, CommonHelper.currentDomain.indexOf("."));
                }
                if((type.equals("public"))) {
                    type = "FALSE";
                }
                else {
                    type = "TRUE";
                }
                List<String> projects = new ArrayList<>();
                //reading projects related to domain
                ResultSet set = DBHelper.executeQuery("SELECT [Id] FROM [RelimeDb].[dbo].[Domain] WHERE Name = '" + domain + "'");
                set.next();
                int domainId = set.getInt("Id");
                set = DBHelper.executeQuery("SELECT [ProjectKey] FROM [RelimeDb].[dbo].[Project] WHERE Domain_Id = '" + domainId + "' AND IsPrivate = '" + type + "'");
                while (set.next()) {
                    set.getMetaData();
                    String key = set.getString("ProjectKey");
                    projects.add(key);
                }
                //checking that only related projects are present
                TableReader reader = new TableReader();
                ProjectsPresentVerifier verifier = new ProjectsPresentVerifier(projects, dashboardPage);
                ReportService.reportAction("Domain '" + domain + "' contains only projects related to this domain.", reader.checkProjectsTable(verifier, dashboardPage));
            }
            else
                ReportService.reportAction("'Private' or 'public' values should be used as second parameter only.", false);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I click checkbox 'Public' above Projects table")
    public void iClickCheckboxPublic() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.projectTablePublicCheckbox.getWrappedElement());
            CommonHelper.clickWithActions(dashboardPage.projectTablePublicCheckbox.getWrappedElement());
            Thread.sleep(4000);
            ReportService.reportAction("Public checkbox was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I click checkbox 'Private' above Projects table")
    public void iClickCheckboxPrivate() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.projectTablePrivateCheckbox.getWrappedElement());
            CommonHelper.clickWithActions(dashboardPage.projectTablePrivateCheckbox.getWrappedElement());
            Thread.sleep(4000);
            ReportService.reportAction("Private checkbox was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I can see that checkbox 'Public' above Projects table is enabled")
    public void iCanSeeThatCheckboxPublicIsEnabled() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Public checkbox is enabled.", dashboardPage.projectTablePublicCheckbox.findElement(By.xpath("./input")).getAttribute("class").contains("ng-not-empty"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I can see that checkbox 'Private' above Projects table is enabled")
    public void iCanSeeThatCheckboxPrivateIsEnabled() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Private checkbox is enabled.", dashboardPage.projectTablePrivateCheckbox.findElement(By.xpath("./input")).getAttribute("class").contains("ng-not-empty"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I can see placeholder \"([^\"]*)\" above Projects table")
    public void iCanSeeThatCheckboxPrivateIsEnabled(String placeholder) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            placeholder = placeholder.replace("\\n", "\n");
            Waiters.waitAppearanceOf(5, dashboardPage.noProjectsResultsLabel.getWrappedElement());
            ReportService.reportAction("Placeholder '" + placeholder + "' is displayed.", dashboardPage.noProjectsResultsLabel.getText().equals(placeholder));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
