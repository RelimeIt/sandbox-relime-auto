package stepdefinition.Dashboard.ProjectsPanel;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import cucumber.api.java.en.*;
import gherkin.lexer.Th;
import helpers.SystemHelper;
import helpers.TableVerifiers.ProjectsVerifiers.ProjectPresentVerifier;
import helpers.TableVerifiers.ProjectsVerifiers.ProjectsPresentVerifier;
import helpers.TableVerifiers.TableReader;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kozlov on 11/4/2016.
 */
public class ProjectsPanelSearch extends PageInstance {

    @Autowired
    DashboardPage dashboardPage;

    @Then("^I can see block 'Search' in Projects panel")
    public void iCanSeeBlockSearch() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.projectTableSearchField.getWrappedElement());
            ReportService.reportAction("Block 'Search' is displayed in Projects panel.", dashboardPage.projectTableSearchField.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see block 'Search' is displayed correctly in Projects panel")
    public void iCanSeeBlockSearchIsDisplayedCorrectly() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.projectTableSearchField.getWrappedElement());
            boolean correct = dashboardPage.projectTableSearchField.isDisplayed()&&
                    dashboardPage.projectTableSearchIcon.isDisplayed()&&
                    dashboardPage.projectTableSearchField.getAttribute("placeholder").equals("Search for projects in current domain...");
            ReportService.reportAction("Block 'Search' is displayed correctly in Projects panel.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I type \"([^\"]*)\" into 'Search' field in Projects panel")
    public void iTypeIntoSearchField(String query) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.projectTableSearchField.getWrappedElement());
            dashboardPage.projectTableSearchField.click();
            dashboardPage.projectTableSearchField.clear();
            dashboardPage.projectTableSearchField.sendKeys(query);
            Thread.sleep(1000);
            ReportService.reportAction("Block 'Search' is displayed in Projects panel.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that 'Search' field text equals \"([^\"]*)\"  in Projects panel")
    public void iTypeIntoSearchFieldTextEquals(String query) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.projectTableSearchField.getWrappedElement());
            ReportService.reportAction("Block 'Search' text equals '" + query + "' in Projects panel.", dashboardPage.projectTableSearchField.getAttribute("value").equals(query));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see that Projects panel contains only projects that contain \"([^\"]*)\"")
    public void iCanSeeThatDomainContainsOnlySearchedProjects(String query) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            TableReader reader = new TableReader();
            ProjectPresentVerifier verifier = new ProjectPresentVerifier(query, dashboardPage);
            ReportService.reportAction("Projects panel contains only projects that contain '" + query + "'.", reader.checkProjectsTable(verifier, dashboardPage));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

}
