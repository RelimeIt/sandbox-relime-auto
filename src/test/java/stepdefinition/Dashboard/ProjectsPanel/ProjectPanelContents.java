package stepdefinition.Dashboard.ProjectsPanel;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.CommonHelper;
import helpers.DBHelper;
import helpers.SystemHelper;
import helpers.TableVerifiers.ProjectsVerifiers.ProjectsPresentVerifier;
import helpers.TableVerifiers.TableReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kozlov on 9/27/2016.
 */
public class ProjectPanelContents extends PageInstance {

    @Autowired
    DashboardPage dashboardPage;

    @And("^I can see that domain \"([^\"]*)\" contains only related projects")
    public void iCanSeeThatDomainContainsOnlyRelatedProjects(String domain) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if (domain.equals("public")){
                domain = SystemHelper.PUBLIC_DOMAIN.substring(SystemHelper.PUBLIC_DOMAIN.indexOf("//") + 2, SystemHelper.PUBLIC_DOMAIN.indexOf("."));
            }
            else if (domain.equals("private")){
                domain = SystemHelper.PUBLIC_DOMAIN.substring(SystemHelper.PRIVATE_DOMAIN.indexOf("//") + 2, SystemHelper.PRIVATE_DOMAIN.indexOf("."));
            }
            else if (domain.equals("default")){
                domain = CommonHelper.currentDomain.substring(CommonHelper.currentDomain.indexOf("//") + 2, CommonHelper.currentDomain.indexOf("."));
            }
            List<String> projects = new ArrayList<>();
            //reading projects related to domain
            ResultSet set = DBHelper.executeQuery("SELECT [Id] FROM [RelimeDb].[dbo].[Domain] WHERE Name = '" + domain + "'");
            set.next();
            int domainId = set.getInt("Id");
            set = DBHelper.executeQuery("SELECT [ProjectKey] FROM [RelimeDb].[dbo].[Project] WHERE Domain_Id = '" + domainId + "'");
            while(set.next()){
                String key = set.getString("ProjectKey");
                projects.add(key);
            }
            //checking that only related projects are present
            TableReader reader = new TableReader();
            ProjectsPresentVerifier verifier = new ProjectsPresentVerifier(projects, dashboardPage);
            ReportService.reportAction("Domain '" + domain + "' contains only projects related to this domain.", reader.checkProjectsTable(verifier, dashboardPage));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that Actions column has \"([0-2])\" icons")
    public void iCanSeeThatActionsColumnHasIcons(int number) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean correct = true;
            for (Element row : dashboardPage.projectTableRows) {
                List<WebElement> buttons = row.findElements(By.xpath("./div[@class = 'btn-holder']/button"));
                if(buttons.size()!=number){
                    correct = false;
                    break;
                }
            }
            ReportService.reportAction("Actions column has " + number + " icons.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see tab \"([^\"]*)\" is active")
    public void iSeeThatTabIsActive(String name) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean correct = false;
            for (Element tab : dashboardPage.dashboardSubscribedUserTabs) {
                if(tab.getText().equals(name)){
                    correct = tab.getAttribute("class").contains("active");
                    break;
                }
            }
            ReportService.reportAction("Tab '" + name + "' is active.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see no tabs on dashboard page")
    public void iSeeNoTabs() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("No tabs are present on dashboard page.", dashboardPage.dashboardSubscribedUserTabs.size()==0);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see button ‘Create new project’")
    public void iSeebuttonCreateNewProject() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("No tabs are present on dashboard page.", dashboardPage.addNewProjectButton.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see message 'No projects exist for the current domain'")
    public void iSeeMessageNoProjectsExist() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("'No projects exist for the current domain' message is displayed on dashboard page.", dashboardPage.noProjectsExistLabel.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I select tab \"([^\"]*)\" on dashboard page")
    public void iSelectTab(String name) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Tab '" + name + "' was selected.", CommonHelper.selectElementByText(dashboardPage.dashboardSubscribedUserTabs, name));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see Project table column \"([^\"]*)\" is present")
    public void iSeeTableColumn(String col) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Column '" + col + "' is present.", CommonHelper.elementWithTextIsPresent(dashboardPage.projectTableColumnLabels, col));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see Project table column \"([^\"]*)\" is not present")
    public void iSeeTableColumnIsNotPresent(String col) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean correct = false;
            for (Element tab : dashboardPage.projectTableColumnLabels) {
                if(tab.getText().toLowerCase().equals(col.toLowerCase())){
                    correct = true;
                    break;
                }
            }
            ReportService.reportAction("Column '" + col + "' is present.", !correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
