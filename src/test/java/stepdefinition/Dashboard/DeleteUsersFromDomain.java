package stepdefinition.Dashboard;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import cucumber.api.java.en.Then;
import helpers.CommonHelper;
import helpers.SystemHelper;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;
import pages.relime.ProfilePage;

/**
 * Created by kozlov on 1/6/2017.
 */
public class DeleteUsersFromDomain extends PageInstance {

    @Autowired
    DashboardPage dashboardPage;

    @Autowired
    ProfilePage profilePage;

    @Then("^I delete user \"([^\"]*)\" from domain")
    public void iDeleteUserFromDomain(String user) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            user = CommonHelper.replaceUserWithActualEmail(user);
            boolean correct = false;
            for (Element tab : dashboardPage.teamTableRows) {
                boolean expanded = false;
                try{
                    if(tab.findElement(By.xpath(".//div[contains(@class, 'accordion-content')]//span[contains(@class, 'user-email')]")).isDisplayed())
                        expanded = true;
                }catch (AssertionError e){}
                if(!expanded)
                    tab.click();
                Thread.sleep(200);
                if(tab.findElement(By.xpath(".//div[contains(@class, 'accordion-content')]//span[contains(@class, 'user-email')]")).getText().toLowerCase().equals(user.toLowerCase())){
                    tab.findElement(By.xpath(".//button[@data-uib-tooltip='Delete']")).click();
                    correct = true;
                    break;
                }
            }
            ReportService.reportAction("User '" + user + "' was deleted from domain.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that user \"([^\"]*)\" is subscribed on current domain on Dashboard")
    public void iSeeUserIsSubscribedOnDomain(String user) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            user = CommonHelper.replaceUserWithActualEmail(user);
            Thread.sleep(200);
            boolean correct = false;
            for (Element tab : dashboardPage.teamTableRows) {
                boolean expanded = false;
                try{
                    if(tab.findElement(By.xpath(".//div[contains(@class, 'accordion-content')]//span[contains(@class, 'user-email')]")).isDisplayed())
                        expanded = true;
                }catch (AssertionError e){}
                if(!expanded)
                    tab.click();
                Thread.sleep(200);
                if(tab.findElement(By.xpath(".//div[contains(@class, 'accordion-content')]//span[contains(@class, 'user-email')]")).getText().toLowerCase().equals(user.toLowerCase())){
                    correct = true;
                    break;
                }
            }
            ReportService.reportAction("User '" + user + "' is subscribed on current domain on Dashboard.", correct);
            Thread.sleep(200);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that user \"([^\"]*)\" is not subscribed on current domain on Dashboard")
    public void iSeeUserIsNotSubscribedOnDomain(String user) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            user = CommonHelper.replaceUserWithActualEmail(user);
            boolean correct = false;
            for (Element tab : dashboardPage.teamTableRows) {
                boolean expanded = false;
                try{
                    if(tab.findElement(By.xpath(".//div[contains(@class, 'accordion-content')]//span[contains(@class, 'user-email')]")).isDisplayed())
                        expanded = true;
                }catch (AssertionError e){}
                if(!expanded)
                    tab.click();
                Thread.sleep(200);
                if(tab.findElement(By.xpath(".//div[contains(@class, 'accordion-content')]//span[contains(@class, 'user-email')]")).getText().toLowerCase().equals(user.toLowerCase())){
                    correct = true;
                    break;
                }
            }
            ReportService.reportAction("User '" + user + "' is not subscribed on current domain on Dashboard.", !correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that domain \"([^\"]*)\" is not displayed on profile page")
    public void iSeeDomainIsNotDisplayedOnProfile(String domain) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if (domain.equals("public")){
                domain = SystemHelper.PUBLIC_DOMAIN;
            }
            else if (domain.equals("private")){
                domain = SystemHelper.PRIVATE_DOMAIN;
            }
            else if (domain.equals("domain")){
                domain = CommonHelper.currentDomain;
            }
            boolean correct = true;
            for (Element tab : profilePage.domainsList) {
                if(tab.findElement(By.xpath("/a")).getText().toLowerCase().equals(domain)){
                    correct = false;
                    break;
                }
            }
            ReportService.reportAction("Domain '" + domain + "' is not displayed on Profile page.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
