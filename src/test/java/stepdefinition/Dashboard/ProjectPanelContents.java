package stepdefinition.Dashboard;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Button;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import cucumber.api.java.en.And;
import helpers.CommonHelper;
import helpers.SystemHelper;
import org.apache.commons.collections.map.HashedMap;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;
import pages.relime.SettingsPage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by kozlov on 9/27/2016.
 */
public class ProjectPanelContents extends PageInstance {

    @Autowired
    SettingsPage settingsPage;

    @Autowired
    DashboardPage dashboardPage;

    @And("^I can see that domain \"([^\"]*)\" contains only related projects")
    public void iCanSeeThatDomainContainsOnlyRelatedProjects(String domain) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(domain.equals("default"))
                domain = SystemHelper.DOMAIN.substring(SystemHelper.DOMAIN.indexOf("//") + 2, SystemHelper.DOMAIN.indexOf("."));
            List<String> projects = new ArrayList<>();
            //reading projects related to domain
            String connectionString = "jdbc:sqlserver://" + SystemHelper.DATABASEIP + ";databaseName=RelimeDb;user=" + SystemHelper.DATABASELOGIN + ";password=" + SystemHelper.DATABASEPASS + ";";
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            Connection con = DriverManager.getConnection(connectionString);
            ResultSet set = con.createStatement().executeQuery("SELECT [Id] FROM [RelimeDb].[dbo].[Domain] WHERE Name = '" + domain + "'");
            set.next();
            int domainId = set.getInt("Id");
            set = con.createStatement().executeQuery("SELECT [ProjectKey] FROM [RelimeDb].[dbo].[Project] WHERE Domain_Id = '" + domainId + "'");
            while(set.next()){
                String key = set.getString("ProjectKey");
                projects.add(key);
            }
            //checking that only related projects are present
            boolean projectsAreCorrect = true;
            boolean firstPage = true;
            if(dashboardPage.projectTablePages!=null&&dashboardPage.projectTablePages.size()>3) {
                outer:
                while (!dashboardPage.projectTablePages.get(dashboardPage.projectTablePages.size() - 2).getAttribute("class").contains("active"))
                {
                    for (int i = 0; i < dashboardPage.projectTablePages.size(); i++) {
                        Element e = dashboardPage.projectTablePages.get(i);
                        if (e.getAttribute("class").contains("active") && !dashboardPage.projectTablePages.get(i + 1).getText().equals("&gt;")) {
                            if (!firstPage) {
                                dashboardPage.projectTablePages.get(i + 1).click();
                                Thread.sleep(500);
                            } else {
                                firstPage = false;
                                i--;
                            }
                            for (Element row : dashboardPage.projectTableRows) {
                                String key = row.findElement(By.xpath(dashboardPage.PROJECTKEY)).getText();
                                boolean contains = false;
                                for (String s : projects) {
                                    if(s.equals(key)) {
                                        contains = true;
                                        break;
                                    }
                                }
                                if(!contains){
                                    projectsAreCorrect = false;
                                    break;
                                }
                            }
                            //break;
                        } else if (e.getAttribute("class").contains("active") && dashboardPage.projectTablePages.get(i + 1).getText().equals("&gt;")) {
                            break outer;
                        }
                    }
                }
            }
            else{
                for (Element row : dashboardPage.projectTableRows) {
                    String key = row.findElement(By.xpath(dashboardPage.PROJECTKEY)).getText();
                    boolean contains = false;
                    for (String s : projects) {
                        if(s.equals(key)) {
                            contains = true;
                            break;
                        }
                    }
                    if(!contains){
                        projectsAreCorrect = false;
                        break;
                    }
                }
            }
            ReportService.ReportAction("Domain '" + domain + "' contains only projects related to this domain.", projectsAreCorrect);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.ReportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see that Actions column has \"([1-2])\" icons")
    public void iCanSeeThatActionsColumnHasIcons(int number) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean correct = true;
            for (Element row : dashboardPage.projectTableRows) {
                List<WebElement> buttons = row.findElements(By.xpath("./div[@class = 'btn-holder']/button"));
                if(buttons.size()!=number){
                    correct = false;
                    break;
                }
            }
            ReportService.ReportAction("Actions column has " + number + " icons.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.ReportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
