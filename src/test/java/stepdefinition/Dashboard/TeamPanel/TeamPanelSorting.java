package stepdefinition.Dashboard.TeamPanel;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import cucumber.api.java.en.*;
import helpers.CommonHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ThemeResolver;
import pages.base.PageInstance;
import pages.relime.DashboardPage;

import java.util.ArrayList;

/**
 * Created by kozlov on 10/7/2016.
 */
public class TeamPanelSorting extends PageInstance {

    @Autowired
    DashboardPage dashboardPage;

    @Then("^I can see panel Team is visible")
    public void iCanSeeThatSubscribersTableIsVisible() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Subscribers table is visible.", dashboardPage.teamTable.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see panel Team is not visible")
    public void iCanSeeThatSubscribersTableIsNotVisible() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Subscribers table is visible.", !dashboardPage.teamTable.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that subscribers table has \"([0-9]+)\" rows")
    public void iCanSeeThatSubscribersTableHasNumberOfRows(int number) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Subscribers table has" + number + " rows.", dashboardPage.teamTableRows.size()==number);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that column header 'Name' has an arrow in subscribers table")
    public void iCanSeeThatColumnHeaderNameHasAnArrow() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Column header 'Name' has an arrow.", dashboardPage.teamTableColumns.get(2).findElement(By.xpath(".//i[@class='ico-caret']")).isEnabled());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I click column header 'Name' in subscribers table")
    public void iClickColumnHeaderName() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            CommonHelper.clickWithActions(dashboardPage.teamTableColumns.get(2).findElement(By.xpath("./button")));
            Thread.sleep(2000);
            ReportService.reportAction("Column header 'Name' was clicked.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I can see that subscribers are sorted by \"([^\"]*)\" order by first name")
    public void iCanSeeThatSubscribersAreSorted(String order) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(2000);
            ArrayList<String> unsorted = new ArrayList<>();
            for(Element e : dashboardPage.teamTableRows){
                unsorted.add(e.findElement(By.xpath("./div[@class='accordion-heading']//span[contains(@class, 'user-')]")).getText());
            }
            ArrayList<String> sorted = (ArrayList<String>) unsorted.clone();
            sorted.sort(null);
            boolean sortingCorrect = true;
            if(order.equals("descending")){
                for (int i = sorted.size() - 1; i >= 0; i--) {
                    if(!sorted.get(i).equals(unsorted.get(sorted.size() - 1 - i))){
                        sortingCorrect = false;
                        break;
                    }
                }
                ReportService.reportAction("Subscribers list is sorted by " + order + " order.", sortingCorrect);
            }
            else if(order.equals("ascending")){
                for (int i = 0; i < sorted.size(); i++) {
                    if(!sorted.get(i).equals(unsorted.get(i))){
                        sortingCorrect = false;
                        break;
                    }
                }
                ReportService.reportAction("Subscribers list is sorted by " + order + " order.", sortingCorrect);
            }
            else{
                ReportService.reportAction("Wrong order. Available types: 'ascending', 'descending'.", false);
            }
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I see that numeration in Subscribers panel is correct")
    public void iSeeThatSubscribersNumerationIsCorrect() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean correct = true;
            for (int i = 0; i < dashboardPage.teamTableRows.size(); i++) {
                WebElement e = dashboardPage.teamTableRows.get(i).findElement(By.xpath(".//span[contains(@class, 'counter')]"));
                if(!Integer.valueOf(e.getText()).equals(i + 1)){
                    correct = false;
                    break;
                }
            }
            ReportService.reportAction("Numeration in Subscribers panel is correct.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
