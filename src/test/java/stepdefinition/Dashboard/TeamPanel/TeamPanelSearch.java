package stepdefinition.Dashboard.TeamPanel;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import helpers.CommonHelper;
import helpers.DBHelper;
import helpers.SystemHelper;
import helpers.TableVerifiers.TableReader;
import helpers.TableVerifiers.TeamVerifiers.TeamEmailVerifier;
import helpers.TableVerifiers.TeamVerifiers.TeamNameVerifier;
import helpers.TableVerifiers.TeamVerifiers.TeamProjectVerifier;
import helpers.TableVerifiers.TeamVerifiers.TeamSubscribersVerifier;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kozlov on 11/7/2016.
 */
public class TeamPanelSearch extends PageInstance {

    @Autowired
    DashboardPage dashboardPage;

    @Then("^I can see block 'Search' in Team panel")
    public void iCanSeeBlockSearch() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.teamTableSearchField.getWrappedElement());
            ReportService.reportAction("Block 'Search' is displayed in Projects panel.", dashboardPage.teamTableSearchField.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see block 'Search' is displayed correctly in Team panel")
    public void iCanSeeBlockSearchIsDisplayedCorrectly() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.teamTableSearchField.getWrappedElement());
            boolean correct = dashboardPage.teamTableSearchField.isDisplayed()&&
                    dashboardPage.teamTableSearchIcon.isDisplayed()&&
                    dashboardPage.teamTableSearchField.getAttribute("placeholder").equals("Search for users in current domain...");
            ReportService.reportAction("Block 'Search' is displayed correctly in Team panel.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I type \"([^\"]*)\" into 'Search' field in Team panel")
    public void iTypeIntoSearchField(String query) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.teamTableSearchField.getWrappedElement());
            dashboardPage.teamTableSearchField.click();
            dashboardPage.teamTableSearchField.clear();
            dashboardPage.teamTableSearchField.sendKeys(query);
            Thread.sleep(1000);
            ReportService.reportAction("Block 'Search' is displayed in Team panel.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that 'Search' field text equals \"([^\"]*)\"  in Team panel")
    public void iTypeIntoSearchFieldTextEquals(String query) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.teamTableSearchField.getWrappedElement());
            ReportService.reportAction("Block 'Search' text equals '" + query + "' in Team panel.", dashboardPage.teamTableSearchField.getAttribute("value").equals(query));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see that Team panel contains only users that contain \"([^\"]*)\"")
    public void iCanSeeThatDomainContainsOnlySearchedUsers(String query) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            TableReader reader = new TableReader();
            TeamNameVerifier verifier = new TeamNameVerifier(query);
            ReportService.reportAction("Team panel contains only subscribers that contain '" + query + "'.", reader.checkTeamTable(verifier, dashboardPage));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see that Team panel contains only users that contain email \"([^\"]*)\"")
    public void iCanSeeThatDomainContainsOnlyProjectsWithEmail(String query) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            TableReader reader = new TableReader();
            TeamEmailVerifier verifier = new TeamEmailVerifier(query);
            ReportService.reportAction("Team panel contains only subscribers that contain email '" + query + "'.", reader.checkTeamTable(verifier, dashboardPage));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see that Team panel contains only subscribers that contain project \"([^\"]*)\"")
    public void iCanSeeThatDomainContainsOnlySubscribersWithProject (String query) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            TableReader reader = new TableReader();
            TeamProjectVerifier verifier = new TeamProjectVerifier(query);
            ReportService.reportAction("Team panel contains only subscribers that contain project '" + query + "'.", reader.checkTeamTable(verifier, dashboardPage));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I can see that only users of subscribed projects are displayed for user \"([^\"]*)\"")
    public void iCanSeeThatOnlyPublicProjectsDisplayedForDomain(String user) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            user = CommonHelper.replaceUserWithActualEmail(user);
            List<String> projects = new ArrayList<>();
            List<String> users = new ArrayList<>();
            List<String> userNames = new ArrayList<>();
            //reading projects related to domain
            ResultSet set = DBHelper.executeQuery("SELECT UserId FROM [dbo].[User] WHERE Email = '" + user + "'");
            set.next();
            int userId = set.getInt("UserId");
            set = DBHelper.executeQuery("SELECT Project_Id FROM [dbo].[Subscription] WHERE User_Id = '" + userId + "'");
            //set.next();
            while (set.next()) {
                String key = set.getString("Project_Id");
                projects.add(key);
            }
            for (String id : projects) {
                set = DBHelper.executeQuery("SELECT User_Id FROM [dbo].[Subscription] WHERE Project_Id = '" + id + "'");
                while (set.next()) {
                    String key = set.getString("User_Id");
                    if(!users.contains(key))
                        users.add(key);
                }
            }
            for (String id : users) {
                set = DBHelper.executeQuery("SELECT FirstName, LastName FROM [dbo].[User] WHERE UserId = '" + id + "'");
                set.next();
                String first = set.getString("FirstName");
                String last = set.getString("LastName");
                userNames.add(first + " " + last);
            }
            //checking that only related projects are present
            TableReader reader = new TableReader();
            TeamSubscribersVerifier verifier = new TeamSubscribersVerifier(userNames);
            ReportService.reportAction("Team panel contains only subscribers that are subscribed to the same project as '" + user + "'.", reader.checkTeamTable(verifier, dashboardPage));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
