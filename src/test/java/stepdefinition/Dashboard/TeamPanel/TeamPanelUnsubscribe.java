package stepdefinition.Dashboard.TeamPanel;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import cucumber.api.java.en.*;
import helpers.CommonHelper;
import helpers.DBHelper;
import helpers.SystemHelper;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kozlov on 11/15/2016.
 */
public class TeamPanelUnsubscribe extends PageInstance {

    @Autowired
    DashboardPage dashboardPage;

    @Given("^I check that user \"([^\"]*)\" is subscribed on project \"([^\"]*)\" as \"([^\"]*)\"")
    public void iCheckThatUserIsSubscribed(String user, String project, String role) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            user = CommonHelper.replaceUserWithActualEmail(user);
            switch (role){
                case "PM":
                    role = "1";
                    break;
                case "TM":
                    role = "2";
                    break;
                default:
                    ReportService.reportAction("Wrong role field. Available options: PM, TM, unsubscribed.", false);
            }
            project = CommonHelper.replaceProjectWithActualProjectKey(project);
            List<String> projects = new ArrayList<>();
            boolean subscribed = false;
            //reading projects related to domain
            ResultSet set = DBHelper.executeQuery("SELECT UserId FROM [dbo].[User] WHERE Email = '" + user + "'");
            set.next();
            int userId = set.getInt("UserId");
            set = DBHelper.executeQuery("SELECT Project_Id FROM [dbo].[Subscription] WHERE User_Id = '" + userId + "'");
            //set.next();
            while (set.next()) {
                String key = set.getString("Project_Id");
                projects.add(key);
            }
            set = DBHelper.executeQuery("SELECT Id FROM [dbo].[Project] WHERE ProjectKey = '" + project + "'");
            set.next();
            String projectId = set.getString("Id");
            if(projects.contains(projectId)) {
                subscribed = true;
            }
            if(!subscribed){
                DBHelper.executeStatement("INSERT INTO [dbo].[Subscription] (Project_Id, RoleId, User_Id) VALUES (" + projectId + ", " + role + ", " + userId + ")");
                subscribed = true;
            }
            //checking that only related projects are present
            ReportService.reportAction("'" + user + "' is subscribed on project '" + project + "'.", subscribed);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Given("^I check that user \"([^\"]*)\" is not subscribed on project \"([^\"]*)\"")
    public void iCheckThatUserIsNotSubscribed(String user, String project) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            user = CommonHelper.replaceUserWithActualEmail(user);
            switch (project){
                case "public":
                    project = CommonHelper.publicProject;
                    break;
                case "private":
                    project = CommonHelper.privateProject;
                    break;
                case "sDefault":
                    project = SystemHelper.DEFAULTSMOKEPROJECT;
                    break;
                case "rDefaultOne":
                    project = SystemHelper.DEFAULTREGRESSIONPROJECTONE;
                    break;
                case "rDefaultTwo":
                    project = SystemHelper.DEFAULTREGRESSIONPROJECTTWO;
                    break;
                default:
                    ReportService.reportAction("Wrong project field. Available options: public, private, sDefault, rDefaultOne, rDefaultTwo.", false);
            }
            List<String> projects = new ArrayList<>();
            //reading projects related to domain
            ResultSet set = DBHelper.executeQuery("SELECT UserId FROM [dbo].[User] WHERE Email = '" + user + "'");
            set.next();
            int userId = set.getInt("UserId");
            set = DBHelper.executeQuery("SELECT Project_Id FROM [dbo].[Subscription] WHERE User_Id = '" + userId + "'");
            //set.next();
            while (set.next()) {
                String key = set.getString("Project_Id");
                projects.add(key);
            }
            set = DBHelper.executeQuery("SELECT Id FROM [dbo].[Project] WHERE ProjectKey = '" + project + "'");
            set.next();
            String projectId = set.getString("Id");
            if(projects.contains(projectId)) {
                DBHelper.executeStatement("DELETE FROM [dbo].[Subscription] WHERE Project_Id = '" + projectId + "' AND User_Id = '" + userId + "'");
            }
            //checking that only related projects are present
            ReportService.reportAction("'" + user + "' is not subscribed on project '" + project + "'.", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that project \"([^\"]*)\" is absent")
    public void iSeeProjectIsDeleted(String project) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(project.equals("public"))
                project = CommonHelper.publicProject;
            if(project.equals("private"))
                project = CommonHelper.privateProject;
            if(project.equals("sDefault"))
                project = SystemHelper.DEFAULTSMOKEPROJECT;
            if(project.equals("rDefaultOne"))
                project = SystemHelper.DEFAULTREGRESSIONPROJECTONE;
            if(project.equals("rDefaultTwo"))
                project = SystemHelper.DEFAULTREGRESSIONPROJECTTWO;
            boolean elementFound = false;
            if(dashboardPage.findRowInProjectPanelByProjectKey(project)!=null){
                elementFound = true;
            }
            ReportService.reportAction("Project is deleted from the table 'Projects'.", !elementFound);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @When("^I click button 'Unsubscribe' for project \"([^\"]*)\"$")
    public void iUnsubscribe(String project) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            if(project.equals("public"))
                project = CommonHelper.publicProject;
            if(project.equals("private"))
                project = CommonHelper.privateProject;
            if(project.equals("sDefault"))
                project = SystemHelper.DEFAULTSMOKEPROJECT;
            if(project.equals("rDefaultOne"))
                project = SystemHelper.DEFAULTREGRESSIONPROJECTONE;
            if(project.equals("rDefaultTwo"))
                project = SystemHelper.DEFAULTREGRESSIONPROJECTTWO;
            Element row = dashboardPage.findRowInProjectPanelByProjectKey(project);
            boolean elementFound = false;
            if(row!=null){
                elementFound = true;
                row.findElement(By.xpath(dashboardPage.UNSUBSCRIBE_BUTTON)).click();
            }
            ReportService.reportAction("Project deleted from the table.", elementFound);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
