package stepdefinition.Dashboard.TeamPanel;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import helpers.CommonHelper;
import helpers.DBHelper;
import helpers.SystemHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kozlov on 10/21/2016.
 */
public class TeamPanelView extends PageInstance {

    @Autowired
    DashboardPage dashboardPage;

    @Then("^I see the list of subscribers on panel 'Team'")
    public void iCanSeeThatSubscribersTableIsVisible() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Subscribers table rows are visible.", dashboardPage.teamTableRows.size() > 0);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see Search block in panel 'Team'")
    public void iCanSeeSearchBlock() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Subscribers table rows are visible.", dashboardPage.subscribersTableSearchBlock.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that all table rows are collapsed")
    public void iSeeThatAllTableRowsAreCollapsed() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Subscribers table rows are visible.", dashboardPage.subscribersTableExpandedAccordions.size()==0);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see Team table column \"([^\"]*)\" is present")
    public void iSeeTableColumn(String col) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean correct = false;
            for (Element tab : dashboardPage.teamTableColumns) {
                String s = tab.getText();
                if(tab.getText().toLowerCase().equals(col.toLowerCase())){
                    correct = true;
                    break;
                }
            }
            ReportService.reportAction("Column '" + col + "' is present.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see Team table column \"([^\"]*)\" is not present")
    public void iSeeTableColumnIsNotPresent(String col) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            boolean correct = true;
            for (Element tab : dashboardPage.teamTableColumns) {
                String s = tab.getText();
                if(tab.getText().toLowerCase().equals(col.toLowerCase())){
                    correct = false;
                    break;
                }
            }
            ReportService.reportAction("Column '" + col + "' is present.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I select subscriber \"([^\"]*)\" in Team table")
    public void iSelectSubscriber(String user) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            user = CommonHelper.replaceUserWithActualEmail(user);
            boolean correct = false;
            boolean firstPage = true;
            Thread.sleep(2000);
            if (dashboardPage.teamTablePages != null && dashboardPage.teamTablePages.size() > 3) {
                outer:
                while (!dashboardPage.teamTablePages.get(dashboardPage.teamTablePages.size() - 2).getAttribute("class").contains("active")) {
                    for (int i = 0; i < dashboardPage.teamTablePages.size(); i++) {
                        Element e = dashboardPage.teamTablePages.get(i);
                        if (e.getAttribute("class").contains("active") && !dashboardPage.teamTablePages.get(i + 1).getText().equals("&gt;")) {
                            if (!firstPage) {
                                dashboardPage.teamTablePages.get(i + 1).click();
                                Thread.sleep(1000);
                            } else {
                                firstPage = false;
                            }
                            for (Element row : dashboardPage.teamTableRows) {
                                row.click();
                                Thread.sleep(500);
                                if(row.findElement(By.xpath(".//div[contains(@class, 'accordion-content')]//span[contains(@class, 'user-email')]")).getText().equals(user)){
                                    correct = true;
                                    break outer;
                                }
                                row.click();
                                Thread.sleep(500);
                            }
                            //break;
                        } else if (e.getAttribute("class").contains("active") && dashboardPage.teamTablePages.get(i + 1).getText().equals("&gt;")) {
                            break outer;
                        }
                    }
                }
            }
            else {
                for (Element row : dashboardPage.teamTableRows) {
                    row.click();
                    Thread.sleep(500);
                    if(row.findElement(By.xpath(".//div[contains(@class, 'accordion-content')]//span[contains(@class, 'user-email')]")).getText().equals(user)){
                        correct = true;
                        break;
                    }
                    row.click();
                    Thread.sleep(500);
                }
            }
            ReportService.reportAction("Subscriber '" + user + "' was selected.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that subscriber \"([^\"]*)\" is expanded")
    public void iSeeSubscriberIsExpanded(String user) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            user = CommonHelper.replaceUserWithActualEmail(user);
            boolean correct = false;
            for (Element tab : dashboardPage.teamTableRows) {
                if(tab.findElement(By.xpath(".//span[contains(@class, 'user-email')]")).getText().equals(user)){
                    correct = tab.findElement(By.xpath(".//span[contains(@class,'user-name')]")).isDisplayed()
                            &&tab.findElement(By.xpath(".//div[contains(@class, 'role-block')]")).isDisplayed()
                            &&tab.findElement(By.xpath(".//button[contains(@data-uib-tooltip, 'Unsubscribe')]")).isDisplayed();
                    break;
                }
            }
            ReportService.reportAction("Subscriber '" + user + "' is expanded.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that I can not change role for subscriber \"([^\"]*)\"")
    public void iSeeThatICanNotChangeRole(String user) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            user = CommonHelper.replaceUserWithActualEmail(user);
            boolean correct = false;
            for (Element tab : dashboardPage.teamTableRows) {
                if(tab.findElement(By.xpath(".//div[contains(@class, 'accordion-content')]//span[contains(@class, 'user-email')]")).getText().equals(user)){
                    try{
                        tab.findElement(By.xpath(".//div[contains(@class, 'role-block')][not(contains(@class, 'ng-hide'))]"));
                    }
                    catch (AssertionError e){
                        correct = true;
                    }
                    break;
                }
            }
            ReportService.reportAction("I can not change role for subscriber '" + user + "'.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I change role for user \"([^\"]*)\" in project \"([^\"]*)\" to \"([^\"]*)\"")
    public void iChangeRoleForUser(String user, String project, String role) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            user = CommonHelper.replaceUserWithActualEmail(user);
            switch (project){
                case "public":
                    project = "Public Automation Test Project";
                    break;
                case "private":
                    project = "Private Automation Test Project";
                    break;
                default:
                    ReportService.reportAction("Wrong project field. Available options: public, private.", false);
            }
            boolean correct = false;
            for (Element tab : dashboardPage.teamTableRows) {
                if(tab.findElement(By.xpath(".//div[contains(@class, 'accordion-content')]//span[contains(@class, 'user-email')]")).getText().toLowerCase().equals(user.toLowerCase())){
                    for (WebElement e : tab.findElements(By.xpath(".//span[@class = 'project-name ng-binding']"))) {
                        if(project.equals(e.getText())){
                            e.findElement(By.xpath("./parent::*//div/span[contains(@class, 'btn')]")).click();
                            Thread.sleep(5000);
                            for (Element r : dashboardPage.rolesDropdownElements) {
                                if(r.getText().equals(role.toUpperCase())){
                                    r.click();
                                    correct = true;
                                    break;
                                }
                            }
                        }
                    }
                    break;
                }
            }
            ReportService.reportAction("Role for user '" + user + "' in project '" + project + "' was changed to '" + role + "'.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I unsubscribe user \"([^\"]*)\" from project \"([^\"]*)\"")
    public void iUnsubscribeUserFromProject(String user, String project) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            user = CommonHelper.replaceUserWithActualEmail(user);
            switch (project){
                case "public":
                    project = "Public Automation Test Project";
                    break;
                case "private":
                    project = "Private Automation Test Project";
                    break;
                default:
                    ReportService.reportAction("Wrong project field. Available options: public, private.", false);
            }
            boolean correct = false;
            for (Element tab : dashboardPage.teamTableRows) {
                if(tab.findElement(By.xpath(".//div[contains(@class, 'accordion-content')]//span[contains(@class, 'user-email')]")).getText().toLowerCase().equals(user.toLowerCase())){
                    for (WebElement e : tab.findElements(By.xpath(".//span[@class = 'project-name ng-binding']"))) {
                        if(project.equals(e.getText())){
                            tab.findElement(By.xpath("./parent::*//div[contains(@class, 'btn-holder')]")).click();
                            correct = true;
                            Thread.sleep(500);
                            break;
                        }
                    }
                    break;
                }
            }
            ReportService.reportAction("User '" + user + "' was unsubscribed from project '" + project + "'.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that current user has role \"([^\"]*)\" in project \"([^\"]*)\"")
    public void iCanSeeUserHasRole(String role, String project) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            switch (project){
                case "public":
                    project = CommonHelper.publicProject;
                    break;
                case "private":
                    project = CommonHelper.privateProject;
                    break;
                case "sDefault":
                    project = SystemHelper.DEFAULTSMOKEPROJECT;
                    break;
                case "rDefaultOne":
                    project = SystemHelper.DEFAULTREGRESSIONPROJECTONE;
                    break;
                case "rDefaultTwo":
                    project = SystemHelper.DEFAULTREGRESSIONPROJECTTWO;
                    break;
                default:
                    ReportService.reportAction("Wrong project field. Available options: public, private, sDefault, rDefaultOne, rDefaultTwo.", false);
            }
            Element row = dashboardPage.findRowInProjectPanelByProjectKey(project);
            boolean correct = row.findElement(By.xpath(".//span[contains(@class, 'role')]")).getText().equals(role);
            ReportService.reportAction("Role for current user in project '" + project + "' is '" + role + "'.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that pagination element is present in Team table")
    public void iCanSeeThatPaginationIsPresent() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Pagination element is present in Projects table.", dashboardPage.subscribersTablePaginationElement.size() >= 4);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I can see that user \"([^\"]*)\" has only projects that he is subscribed to within current domain in Team panel")
    public void iCheckThatUserIsSubscribed(String user) throws Throwable {
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            user = CommonHelper.replaceUserWithActualEmail(user);
            String domain = CommonHelper.currentDomain.substring(CommonHelper.currentDomain.indexOf("//") + 2, CommonHelper.currentDomain.indexOf("."));
            List<String> projects = new ArrayList<>();
            List<String> projectNames = new ArrayList<>();
            boolean correct = true;
            //reading projects related to domain
            ResultSet set = DBHelper.executeQuery("SELECT UserId FROM [dbo].[User] WHERE Email = '" + user + "'");
            set.next();
            int userId = set.getInt("UserId");
            set = DBHelper.executeQuery("SELECT Project_Id FROM [dbo].[Subscription] WHERE User_Id = '" + userId + "'");
            while (set.next()) {
                String key = set.getString("Project_Id");
                projects.add(key);
            }
            set = DBHelper.executeQuery("SELECT Id FROM [dbo].[Domain] WHERE Name = '" + domain + "'");
            set.next();
            String domainId = set.getString("Id");
            for (String s : projects) {
                set = DBHelper.executeQuery("SELECT Name FROM [dbo].[Project] WHERE Id = '" + s + "' AND Domain_Id = '" + domainId + "'");
                if(set.next())
                    projectNames.add(set.getString("Name"));
            }
            //checking that only related projects are present
            for (Element tab : dashboardPage.teamTableRows) {
                if(tab.findElement(By.xpath(".//div[contains(@class, 'accordion-content')]//span[contains(@class, 'user-email')]")).getText().toLowerCase().equals(user.toLowerCase())){
                    for (WebElement e : tab.findElements(By.xpath(".//span[@class = 'project-name ng-binding']"))) {
                        String s = e.getText();
                        if(!projectNames.contains(e.getText())){
                            correct = false;
                            break;
                        }
                    }
                    break;
                }
            }
            ReportService.reportAction("Only subscribed projects are displayed for user'" + user + "' in team panel.", correct);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
