package stepdefinition.Dashboard.TeamPanel;

import arp.CucumberArpReport;
import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.tools.Waiters;
import cucumber.api.java.en.Then;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.relime.DashboardPage;

/**
 * Created by kozlov on 11/3/2016.
 */
public class TeamPanelEmpty extends PageInstance {

    @Autowired
    DashboardPage dashboardPage;

    @Then("^I can see message \"([^\"]*)\" on Team panel")
    public void iCanSeeNoSubscribersMessageOnTeamPanel(String message) throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Waiters.waitAppearanceOf(5, dashboardPage.noSubscribersLabel.getWrappedElement());
            ReportService.reportAction("Message '" + message + "' is displayed on Team panel.", dashboardPage.noSubscribersLabel.getText().equals(message));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that picture for empty Team panel is displayed")
    public void iCanSeeNoSubscribersImageOnTeamPanel() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Image for no subscribers is displayed on Team panel.", dashboardPage.noSubscribersImage.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I see that link 'Learn more about Relime' is displayed")
    public void iCanSeeNoSubscribersLinkOnTeamPanel() throws Throwable{
        if (!checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            ReportService.reportAction("Image for no subscribers is displayed on Team panel.", dashboardPage.noSubscribersLink.getText().equals("Learn more about Relime"));
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e)   {
            ReportService.reportAction("Error: " + e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
