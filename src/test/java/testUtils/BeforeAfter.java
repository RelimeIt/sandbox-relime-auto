package testUtils;

import arp.ReportClasses.Report;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;
import com.paulhammant.ngwebdriver.NgWebDriver;
import helpers.CommonHelper;
import helpers.DBHelper;
import helpers.SystemHelper;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import pages.base.PageInstance;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import arp.CucumberArpReport;

import java.sql.ResultSet;
import java.util.concurrent.TimeUnit;
import com.jcraft.jsch.JSch;
import stepdefinition.ProjectDataEditing.CreateNewProject;

import static com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory.setArpReportClient;
import static helpers.SystemHelper.MAINWINDOWHANDLER;
import static helpers.SystemHelper.Reset_Values;

public class BeforeAfter extends PageInstance {

    public static Scenario lastScenario;

    @Before
    public void setUp(Scenario scenario) {
        Reset_Values();
        try {
            setArpReportClient(new CucumberArpReport());
            if (!scenario.getId().startsWith(arpClient.getTestSuiteName().toLowerCase())) {
                if(!CommonHelper.firstRun)
                    arpClient.closeAndSendToAnotherURL(SystemHelper.ARP_URL);
                else
                    CommonHelper.firstRun = false;
                arpClient.open("02CBE32D-FC51-4812-98A7-DE012DFD1EC2", scenario.getId());
            }
            arpClient.addTestToTestSuite(scenario);
            CommonHelper.currentDomain = SystemHelper.PUBLIC_DOMAIN;
            CommonHelper.publicProject = SystemHelper.PUBLIC_PROJECT_ON_PUBLIC_DOMAIN;
            CommonHelper.privateProject = SystemHelper.PRIVATE_PROJECT_ON_PUBLIC_DOMAIN;
            driver.navigate().to(SystemHelper.URL);
            try {
                Alert alert = driver.switchTo().alert();
                alert.accept();
            } catch (Exception e){}
            driver.manage().window().maximize();
            driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
            ngDriver = new NgWebDriver((JavascriptExecutor) driver);
            ngDriver.waitForAngularRequestsToFinish();
            MAINWINDOWHANDLER = driver.getWindowHandle();
            ResultSet set = DBHelper.executeQuery("SELECT UserId FROM [dbo].[User] WHERE Email = 'domainuser981@gmail.com'");
            set.next();
            int userId = set.getInt("UserId");
            DBHelper.executeStatement("DELETE FROM [dbo].[Subscription] WHERE User_Id = '" + userId + "'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown(Scenario scenario) throws Exception {
        try {
            if(CommonHelper.createdProjectKeys.size()>0) {
                try {
                    JSch jsch = new JSch();
                    Session session = jsch.getSession(SystemHelper.SERVERLOGIN, SystemHelper.DATABASEIP, 22);
                    session.setPassword(SystemHelper.SERVERPASS);
                    session.setConfig("StrictHostKeyChecking", "no");
                    session.connect();
                    ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
                    sftpChannel.connect();
                    for (String key : CommonHelper.createdProjectKeys) {
                        try {
                            DBHelper.executeStatement("DELETE FROM [RelimeDb].[dbo].[Project] WHERE ProjectKey = '" + key + "';");
                            String domain = SystemHelper.PUBLIC_DOMAIN.substring(SystemHelper.PUBLIC_DOMAIN.indexOf("//") + 2, SystemHelper.PUBLIC_DOMAIN.indexOf("."));
                            CreateNewProject.RemoveFolderViaSSH(sftpChannel, "/C:/Saved repositories/" + domain + "/" + key);
                            domain = SystemHelper.PRIVATE_DOMAIN.substring(SystemHelper.PRIVATE_DOMAIN.indexOf("//") + 2, SystemHelper.PRIVATE_DOMAIN.indexOf("."));
                            CreateNewProject.RemoveFolderViaSSH(sftpChannel, "/C:/Saved repositories/" + domain + "/" + key);
                        } catch (Exception e) {
                            String s = e.getMessage();
                        }
                    }
                    sftpChannel.disconnect();
                    session.disconnect();
                } catch (Exception e) {
                }
            }
            arpClient.decideTestStatus();
            arpClient.finishTest();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}