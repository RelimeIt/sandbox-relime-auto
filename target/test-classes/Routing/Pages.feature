@Pages
Feature: Pages routing

  Background:
    Given I am in page "Default"
    And I am signed in
    Given I am in page "Dashboard"

  @REL-745 @ignore
  Scenario: Check Sign In/Out link on Profile page
    Then I can see path matches RegEx "http://auto.relime.ga/dashboard" in browser route

  @REL-744 @ignore
  Scenario: Check settings link on Profile page
    Then I can see path matches RegEx "http://auto.relime.ga/dashboard" in browser route

  @REL-747
  Scenario: Check link on Dashboard page
    Given I am in page "Dashboard"
    Then I can see path matches RegEx "http://auto.relime.ga/dashboard" in browser route

  @REL-748
  Scenario: Check link on Edit Features page
    And I select project "NATP"
    And I am in page "Editor"
    Then I can see path matches RegEx "http://auto.relime.ga/[A-Z,0-9]{3,4}/editor" in browser route

  @REL-749
  Scenario: Check link on Project Settings page
    And I select project "NATP"
    And I am in page "Project Settings"
    Then I can see path matches RegEx "http://auto.relime.ga/[A-Z,0-9]{3,4}/settings" in browser route

  @REL-750
  Scenario: Check link on Project Statistics page
    And I select project "NATP"
    And I am in page "Statistics Page"
    Then I can see path matches RegEx "http://auto.relime.ga/[A-Z,0-9]{3,4}/activity" in browser route

  @REL-751
  Scenario: Check link on Project Statistics page
    And I select project "NATP"
    And I am in page "Feature Management"
    Then I can see path matches RegEx "http://auto.relime.ga/[A-Z,0-9]{3,4}/management" in browser route

  @REL-813
  Scenario: Check dashboard page is displayed when redirected to non existing page
    And I select project "WRONG"
    Then I can see path matches RegEx "http://auto.relime.ga/dashboard" in browser route