$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("ProjectDataEditing/ProjectSettings/TTSSettings.feature");
formatter.feature({
  "line": 2,
  "name": "TTS settings",
  "description": "",
  "id": "tts-settings",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@TTSSettings"
    }
  ]
});
formatter.before({
  "duration": 6011742108,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "Error message when when adding new task tracker",
  "description": "",
  "id": "tts-settings;error-message-when-when-adding-new-task-tracker",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@REL-826"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "I am in page \"Default\"",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "I am signed in",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "I am in page \"Dashboard\"",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "I check that project \"F002\" is absent in DB",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I check that project \"F003\" is absent in DB",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I click button \u0027Plus\u0027 in panel \u0027Projects\u0027 in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I see pop up \"New Project\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "I type \"project_name2\" into textbox \u0027Name\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "I type \"F002\" into textbox \u0027Project key\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I type \"test\" into textbox \u0027Description\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I select item \"Cucumber-JVM\" in drop-down \u0027BDD-framework\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I click on button \"Create\" on pop-up \"New Project\"",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "I see success notification \"\"",
  "keyword": "Then "
});
formatter.step({
  "line": 19,
  "name": "I see that project is added to panel \u0027Projects\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "I select project in panel \u0027Projects\u0027 in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "I see that all the entered data matches data in page \u0027Project Settings\u0027 for the new project",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "I click button \u0027Plus\u0027 near setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "I see pop up \"Add task tracking system\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "I select item \"Jira\" under drop-down \u0027Name\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "I type \"https://jira.unitedsofthouse.com\" into textbox \u0027URL\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "I click on button \"Add\" on pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "I see success notification \"\"",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "I see that \"Jira\" is added to setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "Then "
});
formatter.step({
  "line": 29,
  "name": "I see that entered data matches data in setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027 for Jira",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "I am in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "I click button \u0027Plus\u0027 in panel \u0027Projects\u0027 in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "I see pop up \"New Project\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "I type \"project_name3\" into textbox \u0027Name\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 34,
  "name": "I type \"F003\" into textbox \u0027Project key\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "I type \"test\" into textbox \u0027Description\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "I select item \"Cucumber-JVM\" in drop-down \u0027BDD-framework\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "I click on button \"Create\" on pop-up \"New Project\"",
  "keyword": "And "
});
formatter.step({
  "line": 38,
  "name": "I see success notification \"\"",
  "keyword": "Then "
});
formatter.step({
  "line": 39,
  "name": "I see that project is added to panel \u0027Projects\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 40,
  "name": "I select project in panel \u0027Projects\u0027 in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 41,
  "name": "I see that all the entered data matches data in page \u0027Project Settings\u0027 for the new project",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "I click button \u0027Plus\u0027 near setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "I see pop up \"Add task tracking system\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 44,
  "name": "I select item \"Jira\" under drop-down \u0027Name\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "I type \"https://jira.unitedsofthouse.com/\" into textbox \u0027URL\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "I click on button \"Add\" on pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "I see error notification \"\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Default",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.iAmInPage(String)"
});
formatter.result({
  "duration": 1174811397,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDefinition.iAmSignedIn()"
});
formatter.result({
  "duration": 7801858957,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Dashboard",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.iAmInPage(String)"
});
formatter.result({
  "duration": 6067155343,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "F002",
      "offset": 22
    }
  ],
  "location": "CreateNewProject.iCheckThatProjectIsAbsentInDB(String)"
});
formatter.result({
  "duration": 712959798,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "F003",
      "offset": 22
    }
  ],
  "location": "CreateNewProject.iCheckThatProjectIsAbsentInDB(String)"
});
formatter.result({
  "duration": 323575249,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Dashboard",
      "offset": 51
    }
  ],
  "location": "CreateNewProject.iClickbuttonPlusInPanelProjects(String)"
});
formatter.result({
  "duration": 54858085,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "New Project",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "duration": 1112759073,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "project_name2",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxNameInPopupNewProject(String)"
});
formatter.result({
  "duration": 280706835,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "F002",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxKeyInPopupNewProject(String)"
});
formatter.result({
  "duration": 170632693,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxDescriptionInPopupNewProject(String)"
});
formatter.result({
  "duration": 174645630,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Cucumber-JVM",
      "offset": 15
    }
  ],
  "location": "CreateNewProject.iSelectItemInDropdownBDDInPopupNewProject(String)"
});
formatter.result({
  "duration": 213788479,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Create",
      "offset": 19
    },
    {
      "val": "New Project",
      "offset": 38
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "duration": 2089819121,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "duration": 614807217,
  "status": "passed"
});
formatter.match({
  "location": "CreateNewProject.iSeeProjectDataIsCorrect()"
});
formatter.result({
  "duration": 1569949284,
  "status": "passed"
});
formatter.match({
  "location": "CreateNewProject.iSelectProjectInPanelProjects()"
});
formatter.result({
  "duration": 900382260,
  "status": "passed"
});
formatter.match({
  "location": "TTSSettings.iSeeThatEnteredDataIsCorrect()"
});
formatter.result({
  "duration": 1162944885,
  "status": "passed"
});
formatter.match({
  "location": "TTSSettings.iClickButtonPlusNearTTSInPageProjectSettings()"
});
formatter.result({
  "duration": 59409377,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Add task tracking system",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "duration": 1098916728,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Jira",
      "offset": 15
    },
    {
      "val": "Add task tracking system",
      "offset": 55
    }
  ],
  "location": "CreateNewProject.iSelectItemUnderDropdownNameInPopup(String,String)"
});
formatter.result({
  "duration": 269996650,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "https://jira.unitedsofthouse.com",
      "offset": 8
    },
    {
      "val": "Add task tracking system",
      "offset": 72
    }
  ],
  "location": "TTSSettings.iTypeURLIntoURLTextboxInPopup(String,String)"
});
formatter.result({
  "duration": 244869378,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Add",
      "offset": 19
    },
    {
      "val": "Add task tracking system",
      "offset": 35
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "duration": 2079087788,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "duration": 604575675,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Jira",
      "offset": 12
    }
  ],
  "location": "TTSSettings.iSeeThatJiraIsAddedToSettingTTS(String)"
});
formatter.result({
  "duration": 588495315,
  "status": "passed"
});
formatter.match({
  "location": "TTSSettings.iSeeThatEnteredDataMatchesdataInSettingTTS()"
});
formatter.result({
  "duration": 335160920,
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertTrue(Assert.java:52)\r\n\tat arp.ReportService.ReportAction(ReportService.java:332)\r\n\tat stepdefinition.ProjectDataEditing.ProjectSettings.TTSSettings.iSeeThatEnteredDataMatchesdataInSettingTTS(TTSSettings.java:176)\r\n\tat ✽.And I see that entered data matches data in setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027 for Jira(ProjectDataEditing/ProjectSettings/TTSSettings.feature:29)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "Dashboard",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.iAmInPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Dashboard",
      "offset": 51
    }
  ],
  "location": "CreateNewProject.iClickbuttonPlusInPanelProjects(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "New Project",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "project_name3",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxNameInPopupNewProject(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "F003",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxKeyInPopupNewProject(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "test",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxDescriptionInPopupNewProject(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Cucumber-JVM",
      "offset": 15
    }
  ],
  "location": "CreateNewProject.iSelectItemInDropdownBDDInPopupNewProject(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Create",
      "offset": 19
    },
    {
      "val": "New Project",
      "offset": 38
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreateNewProject.iSeeProjectDataIsCorrect()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreateNewProject.iSelectProjectInPanelProjects()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TTSSettings.iSeeThatEnteredDataIsCorrect()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TTSSettings.iClickButtonPlusNearTTSInPageProjectSettings()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Add task tracking system",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Jira",
      "offset": 15
    },
    {
      "val": "Add task tracking system",
      "offset": 55
    }
  ],
  "location": "CreateNewProject.iSelectItemUnderDropdownNameInPopup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "https://jira.unitedsofthouse.com/",
      "offset": 8
    },
    {
      "val": "Add task tracking system",
      "offset": 73
    }
  ],
  "location": "TTSSettings.iTypeURLIntoURLTextboxInPopup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Add",
      "offset": 19
    },
    {
      "val": "Add task tracking system",
      "offset": 35
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 26
    }
  ],
  "location": "CommonStepDefinition.iSeeErrorNotification(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 825104,
  "status": "passed"
});
formatter.before({
  "duration": 1477795426,
  "status": "passed"
});
formatter.scenario({
  "line": 50,
  "name": "Check task tracker notifications",
  "description": "",
  "id": "tts-settings;check-task-tracker-notifications",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 49,
      "name": "@REL-825"
    }
  ]
});
formatter.step({
  "line": 51,
  "name": "I am in page \"Default\"",
  "keyword": "Given "
});
formatter.step({
  "line": 52,
  "name": "I am signed in",
  "keyword": "And "
});
formatter.step({
  "line": 53,
  "name": "I am in page \"Dashboard\"",
  "keyword": "Given "
});
formatter.step({
  "line": 54,
  "name": "I check that project \"F002\" is absent in DB",
  "keyword": "And "
});
formatter.step({
  "line": 55,
  "name": "I click button \u0027Plus\u0027 in panel \u0027Projects\u0027 in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 56,
  "name": "I see pop up \"New Project\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 57,
  "name": "I type \"project_name2\" into textbox \u0027Name\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 58,
  "name": "I type \"F002\" into textbox \u0027Project key\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 59,
  "name": "I type \"test\" into textbox \u0027Description\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 60,
  "name": "I select item \"Cucumber-JVM\" in drop-down \u0027BDD-framework\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 61,
  "name": "I click on button \"Create\" on pop-up \"New Project\"",
  "keyword": "And "
});
formatter.step({
  "line": 62,
  "name": "I see success notification \"\"",
  "keyword": "Then "
});
formatter.step({
  "line": 63,
  "name": "I see that project is added to panel \u0027Projects\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 64,
  "name": "I select project in panel \u0027Projects\u0027 in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 65,
  "name": "I see that all the entered data matches data in page \u0027Project Settings\u0027 for the new project",
  "keyword": "And "
});
formatter.step({
  "line": 66,
  "name": "I click button \u0027Plus\u0027 near setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 67,
  "name": "I see pop up \"Add task tracking system\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 68,
  "name": "I select item \"Jira\" under drop-down \u0027Name\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "When "
});
formatter.step({
  "line": 69,
  "name": "I type \"https://jira.unitedsofthouse.com\" into textbox \u0027URL\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 70,
  "name": "I click on button \"Add\" on pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 71,
  "name": "I see success notification \"\"",
  "keyword": "And "
});
formatter.step({
  "line": 72,
  "name": "I click button \u0027Plus\u0027 near setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 73,
  "name": "I see pop up \"Add task tracking system\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 74,
  "name": "I select item \"Redmine\" under drop-down \u0027Name\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "When "
});
formatter.step({
  "line": 75,
  "name": "I type \"https://redmine.unitedsofthouse.com/\" into textbox \u0027URL\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 76,
  "name": "I click on button \"Add\" on pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 77,
  "name": "I see success notification \"\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Default",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.iAmInPage(String)"
});
formatter.result({
  "duration": 1037861496,
  "status": "passed"
});
formatter.match({
  "location": "CommonStepDefinition.iAmSignedIn()"
});
formatter.result({
  "duration": 15374372,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Dashboard",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.iAmInPage(String)"
});
formatter.result({
  "duration": 5391933260,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "F002",
      "offset": 22
    }
  ],
  "location": "CreateNewProject.iCheckThatProjectIsAbsentInDB(String)"
});
formatter.result({
  "duration": 413860259,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Dashboard",
      "offset": 51
    }
  ],
  "location": "CreateNewProject.iClickbuttonPlusInPanelProjects(String)"
});
formatter.result({
  "duration": 55983313,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "New Project",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "duration": 1091894167,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "project_name2",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxNameInPopupNewProject(String)"
});
formatter.result({
  "duration": 277639673,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "F002",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxKeyInPopupNewProject(String)"
});
formatter.result({
  "duration": 172543837,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxDescriptionInPopupNewProject(String)"
});
formatter.result({
  "duration": 169401723,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Cucumber-JVM",
      "offset": 15
    }
  ],
  "location": "CreateNewProject.iSelectItemInDropdownBDDInPopupNewProject(String)"
});
formatter.result({
  "duration": 208137774,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Create",
      "offset": 19
    },
    {
      "val": "New Project",
      "offset": 38
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "duration": 2096195096,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "duration": 626989402,
  "status": "passed"
});
formatter.match({
  "location": "CreateNewProject.iSeeProjectDataIsCorrect()"
});
formatter.result({
  "duration": 1556131508,
  "status": "passed"
});
formatter.match({
  "location": "CreateNewProject.iSelectProjectInPanelProjects()"
});
formatter.result({
  "duration": 877166443,
  "status": "passed"
});
formatter.match({
  "location": "TTSSettings.iSeeThatEnteredDataIsCorrect()"
});
formatter.result({
  "duration": 1159026185,
  "status": "passed"
});
formatter.match({
  "location": "TTSSettings.iClickButtonPlusNearTTSInPageProjectSettings()"
});
formatter.result({
  "duration": 58666689,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Add task tracking system",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "duration": 1086887248,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Jira",
      "offset": 15
    },
    {
      "val": "Add task tracking system",
      "offset": 55
    }
  ],
  "location": "CreateNewProject.iSelectItemUnderDropdownNameInPopup(String,String)"
});
formatter.result({
  "duration": 187211909,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "https://jira.unitedsofthouse.com",
      "offset": 8
    },
    {
      "val": "Add task tracking system",
      "offset": 72
    }
  ],
  "location": "TTSSettings.iTypeURLIntoURLTextboxInPopup(String,String)"
});
formatter.result({
  "duration": 189866052,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Add",
      "offset": 19
    },
    {
      "val": "Add task tracking system",
      "offset": 35
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "duration": 2087845153,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "duration": 582429725,
  "status": "passed"
});
formatter.match({
  "location": "TTSSettings.iClickButtonPlusNearTTSInPageProjectSettings()"
});
formatter.result({
  "duration": 54715954,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Add task tracking system",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "duration": 1114798042,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Redmine",
      "offset": 15
    },
    {
      "val": "Add task tracking system",
      "offset": 58
    }
  ],
  "location": "CreateNewProject.iSelectItemUnderDropdownNameInPopup(String,String)"
});
formatter.result({
  "duration": 208787159,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "https://redmine.unitedsofthouse.com/",
      "offset": 8
    },
    {
      "val": "Add task tracking system",
      "offset": 76
    }
  ],
  "location": "TTSSettings.iTypeURLIntoURLTextboxInPopup(String,String)"
});
formatter.result({
  "duration": 189149490,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Add",
      "offset": 19
    },
    {
      "val": "Add task tracking system",
      "offset": 35
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "duration": 2083600826,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "duration": 15549540626,
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertTrue(Assert.java:52)\r\n\tat arp.ReportService.ReportAction(ReportService.java:332)\r\n\tat stepdefinition.CommonStepDefinition.iSeeSuccessNotification(CommonStepDefinition.java:503)\r\n\tat ✽.And I see success notification \"\"(ProjectDataEditing/ProjectSettings/TTSSettings.feature:77)\r\n",
  "status": "failed"
});
formatter.after({
  "duration": 98590,
  "status": "passed"
});
formatter.before({
  "duration": 80016396655,
  "status": "passed"
});
formatter.scenario({
  "line": 80,
  "name": "Make changes in task tracker",
  "description": "",
  "id": "tts-settings;make-changes-in-task-tracker",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 79,
      "name": "@REL-830"
    }
  ]
});
formatter.step({
  "line": 81,
  "name": "I am in page \"Dashboard\"",
  "keyword": "Given "
});
formatter.step({
  "line": 82,
  "name": "I check that project \"F002\" is absent in DB",
  "keyword": "And "
});
formatter.step({
  "line": 83,
  "name": "I click button \u0027Plus\u0027 in panel \u0027Projects\u0027 in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 84,
  "name": "I see pop up \"New Project\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 85,
  "name": "I type \"project_name2\" into textbox \u0027Name\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 86,
  "name": "I type \"F002\" into textbox \u0027Project key\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 87,
  "name": "I type \"test\" into textbox \u0027Description\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 88,
  "name": "I select item \"Cucumber-JVM\" in drop-down \u0027BDD-framework\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 89,
  "name": "I click on button \"Create\" on pop-up \"New Project\"",
  "keyword": "And "
});
formatter.step({
  "line": 90,
  "name": "I see success notification \"\"",
  "keyword": "Then "
});
formatter.step({
  "line": 91,
  "name": "I see that project is added to panel \u0027Projects\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 92,
  "name": "I select project in panel \u0027Projects\u0027 in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 93,
  "name": "I see that all the entered data matches data in page \u0027Project Settings\u0027 for the new project",
  "keyword": "And "
});
formatter.step({
  "line": 94,
  "name": "I click button \u0027Plus\u0027 near setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 95,
  "name": "I see pop up \"Add task tracking system\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 96,
  "name": "I select item \"Jira\" under drop-down \u0027Name\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "When "
});
formatter.step({
  "line": 97,
  "name": "I type \"https://jira.unitedsofthouse.com\" into textbox \u0027URL\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 98,
  "name": "I click on button \"Add\" on pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 99,
  "name": "I see success notification \"\"",
  "keyword": "And "
});
formatter.step({
  "line": 100,
  "name": "I change \"Jira\" \u0027Task tracking system\u0027 path setting to \"https://anotherjira.unitedsofthouse.com/\" in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 101,
  "name": "I see success notification \"\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Dashboard",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.iAmInPage(String)"
});
formatter.result({
  "duration": 5264392286,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "F002",
      "offset": 22
    }
  ],
  "location": "CreateNewProject.iCheckThatProjectIsAbsentInDB(String)"
});
formatter.result({
  "duration": 332952452,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Dashboard",
      "offset": 51
    }
  ],
  "location": "CreateNewProject.iClickbuttonPlusInPanelProjects(String)"
});
formatter.result({
  "duration": 55509337,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "New Project",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "duration": 1094809867,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "project_name2",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxNameInPopupNewProject(String)"
});
formatter.result({
  "duration": 263126794,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "F002",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxKeyInPopupNewProject(String)"
});
formatter.result({
  "duration": 171190953,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxDescriptionInPopupNewProject(String)"
});
formatter.result({
  "duration": 160215168,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Cucumber-JVM",
      "offset": 15
    }
  ],
  "location": "CreateNewProject.iSelectItemInDropdownBDDInPopupNewProject(String)"
});
formatter.result({
  "duration": 222514431,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Create",
      "offset": 19
    },
    {
      "val": "New Project",
      "offset": 38
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "duration": 2092329266,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "duration": 607606137,
  "status": "passed"
});
formatter.match({
  "location": "CreateNewProject.iSeeProjectDataIsCorrect()"
});
formatter.result({
  "duration": 1598551138,
  "status": "passed"
});
formatter.match({
  "location": "CreateNewProject.iSelectProjectInPanelProjects()"
});
formatter.result({
  "duration": 830073606,
  "status": "passed"
});
formatter.match({
  "location": "TTSSettings.iSeeThatEnteredDataIsCorrect()"
});
formatter.result({
  "duration": 1174229812,
  "status": "passed"
});
formatter.match({
  "location": "TTSSettings.iClickButtonPlusNearTTSInPageProjectSettings()"
});
formatter.result({
  "duration": 58135177,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Add task tracking system",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "duration": 1080568809,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Jira",
      "offset": 15
    },
    {
      "val": "Add task tracking system",
      "offset": 55
    }
  ],
  "location": "CreateNewProject.iSelectItemUnderDropdownNameInPopup(String,String)"
});
formatter.result({
  "duration": 193642311,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "https://jira.unitedsofthouse.com",
      "offset": 8
    },
    {
      "val": "Add task tracking system",
      "offset": 72
    }
  ],
  "location": "TTSSettings.iTypeURLIntoURLTextboxInPopup(String,String)"
});
formatter.result({
  "duration": 179370464,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Add",
      "offset": 19
    },
    {
      "val": "Add task tracking system",
      "offset": 35
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "duration": 2078542902,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "duration": 601087720,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Jira",
      "offset": 10
    },
    {
      "val": "https://anotherjira.unitedsofthouse.com/",
      "offset": 56
    }
  ],
  "location": "TTSSettings.iChangeTTSPathValueTo(String,String)"
});
formatter.result({
  "duration": 334742614,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "duration": 15470756381,
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertTrue(Assert.java:52)\r\n\tat arp.ReportService.ReportAction(ReportService.java:332)\r\n\tat stepdefinition.CommonStepDefinition.iSeeSuccessNotification(CommonStepDefinition.java:503)\r\n\tat ✽.And I see success notification \"\"(ProjectDataEditing/ProjectSettings/TTSSettings.feature:101)\r\n",
  "status": "failed"
});
formatter.after({
  "duration": 84594,
  "status": "passed"
});
formatter.before({
  "duration": 1866198122,
  "status": "passed"
});
formatter.scenario({
  "line": 104,
  "name": "Check that when all TTS are added button \u0027Add TTS\u0027 is disabled",
  "description": "",
  "id": "tts-settings;check-that-when-all-tts-are-added-button-\u0027add-tts\u0027-is-disabled",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 103,
      "name": "@REL-620"
    }
  ]
});
formatter.step({
  "line": 105,
  "name": "I am in page \"Dashboard\"",
  "keyword": "Given "
});
formatter.step({
  "line": 106,
  "name": "I check that project \"F002\" is absent in DB",
  "keyword": "And "
});
formatter.step({
  "line": 107,
  "name": "I click button \u0027Plus\u0027 in panel \u0027Projects\u0027 in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 108,
  "name": "I see pop up \"New Project\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 109,
  "name": "I type \"project_name2\" into textbox \u0027Name\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 110,
  "name": "I type \"F002\" into textbox \u0027Project key\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 111,
  "name": "I type \"test\" into textbox \u0027Description\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 112,
  "name": "I select item \"Cucumber-JVM\" in drop-down \u0027BDD-framework\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 113,
  "name": "I click on button \"Create\" on pop-up \"New Project\"",
  "keyword": "And "
});
formatter.step({
  "line": 114,
  "name": "I see success notification \"\"",
  "keyword": "Then "
});
formatter.step({
  "line": 115,
  "name": "I see that project is added to panel \u0027Projects\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 116,
  "name": "I select project in panel \u0027Projects\u0027 in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 117,
  "name": "I see that all the entered data matches data in page \u0027Project Settings\u0027 for the new project",
  "keyword": "And "
});
formatter.step({
  "line": 118,
  "name": "I click button \u0027Plus\u0027 near setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 119,
  "name": "I see pop up \"Add task tracking system\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 120,
  "name": "I select item \"Jira\" under drop-down \u0027Name\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "When "
});
formatter.step({
  "line": 121,
  "name": "I type \"https://jira.unitedsofthouse.com\" into textbox \u0027URL\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 122,
  "name": "I click on button \"Add\" on pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 123,
  "name": "I see success notification \"\"",
  "keyword": "And "
});
formatter.step({
  "line": 124,
  "name": "I see that \"Jira\" is added to setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 125,
  "name": "I click button \u0027Plus\u0027 near setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 126,
  "name": "I see pop up \"Add task tracking system\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 127,
  "name": "I select item \"Redmine\" under drop-down \u0027Name\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "When "
});
formatter.step({
  "line": 128,
  "name": "I type \"https://redmine.unitedsofthouse.com\" into textbox \u0027URL\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 129,
  "name": "I click on button \"Add\" on pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 130,
  "name": "I see success notification \"\"",
  "keyword": "And "
});
formatter.step({
  "line": 131,
  "name": "I see that \"Redmine\" is added to setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 132,
  "name": "I click button \u0027Plus\u0027 near setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 133,
  "name": "I see pop up \"Add task tracking system\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 134,
  "name": "I select item \"QC\" under drop-down \u0027Name\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "When "
});
formatter.step({
  "line": 135,
  "name": "I type \"https://qc.unitedsofthouse.com\" into textbox \u0027URL\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 136,
  "name": "I type \"dom\" into textbox \"DOMAIN\" in pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 137,
  "name": "I type \"proj\" into textbox \"PROJECT\" in pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 138,
  "name": "I click on button \"Add\" on pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 139,
  "name": "I see success notification \"\"",
  "keyword": "And "
});
formatter.step({
  "line": 140,
  "name": "I see that \"QC\" is added to setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 141,
  "name": "I see that button \u0027Add TTS\u0027 is disabled",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Dashboard",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.iAmInPage(String)"
});
formatter.result({
  "duration": 6302126580,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "F002",
      "offset": 22
    }
  ],
  "location": "CreateNewProject.iCheckThatProjectIsAbsentInDB(String)"
});
formatter.result({
  "duration": 370618325,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Dashboard",
      "offset": 51
    }
  ],
  "location": "CreateNewProject.iClickbuttonPlusInPanelProjects(String)"
});
formatter.result({
  "duration": 54990264,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "New Project",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "duration": 1089170358,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "project_name2",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxNameInPopupNewProject(String)"
});
formatter.result({
  "duration": 263865127,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "F002",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxKeyInPopupNewProject(String)"
});
formatter.result({
  "duration": 178373683,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxDescriptionInPopupNewProject(String)"
});
formatter.result({
  "duration": 177235083,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Cucumber-JVM",
      "offset": 15
    }
  ],
  "location": "CreateNewProject.iSelectItemInDropdownBDDInPopupNewProject(String)"
});
formatter.result({
  "duration": 192226914,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Create",
      "offset": 19
    },
    {
      "val": "New Project",
      "offset": 38
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "duration": 2104936288,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "duration": 612176711,
  "status": "passed"
});
formatter.match({
  "location": "CreateNewProject.iSeeProjectDataIsCorrect()"
});
formatter.result({
  "duration": 1569678396,
  "status": "passed"
});
formatter.match({
  "location": "CreateNewProject.iSelectProjectInPanelProjects()"
});
formatter.result({
  "duration": 821776223,
  "status": "passed"
});
formatter.match({
  "location": "TTSSettings.iSeeThatEnteredDataIsCorrect()"
});
formatter.result({
  "duration": 1180981486,
  "status": "passed"
});
formatter.match({
  "location": "TTSSettings.iClickButtonPlusNearTTSInPageProjectSettings()"
});
formatter.result({
  "duration": 57905653,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Add task tracking system",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "duration": 1087565867,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Jira",
      "offset": 15
    },
    {
      "val": "Add task tracking system",
      "offset": 55
    }
  ],
  "location": "CreateNewProject.iSelectItemUnderDropdownNameInPopup(String,String)"
});
formatter.result({
  "duration": 186434389,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "https://jira.unitedsofthouse.com",
      "offset": 8
    },
    {
      "val": "Add task tracking system",
      "offset": 72
    }
  ],
  "location": "TTSSettings.iTypeURLIntoURLTextboxInPopup(String,String)"
});
formatter.result({
  "duration": 178188322,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Add",
      "offset": 19
    },
    {
      "val": "Add task tracking system",
      "offset": 35
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "duration": 2086668610,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "duration": 15450596218,
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertTrue(Assert.java:52)\r\n\tat arp.ReportService.ReportAction(ReportService.java:332)\r\n\tat stepdefinition.CommonStepDefinition.iSeeSuccessNotification(CommonStepDefinition.java:503)\r\n\tat ✽.And I see success notification \"\"(ProjectDataEditing/ProjectSettings/TTSSettings.feature:123)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "Jira",
      "offset": 12
    }
  ],
  "location": "TTSSettings.iSeeThatJiraIsAddedToSettingTTS(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TTSSettings.iClickButtonPlusNearTTSInPageProjectSettings()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Add task tracking system",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Redmine",
      "offset": 15
    },
    {
      "val": "Add task tracking system",
      "offset": 58
    }
  ],
  "location": "CreateNewProject.iSelectItemUnderDropdownNameInPopup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "https://redmine.unitedsofthouse.com",
      "offset": 8
    },
    {
      "val": "Add task tracking system",
      "offset": 75
    }
  ],
  "location": "TTSSettings.iTypeURLIntoURLTextboxInPopup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Add",
      "offset": 19
    },
    {
      "val": "Add task tracking system",
      "offset": 35
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Redmine",
      "offset": 12
    }
  ],
  "location": "TTSSettings.iSeeThatJiraIsAddedToSettingTTS(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TTSSettings.iClickButtonPlusNearTTSInPageProjectSettings()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Add task tracking system",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "QC",
      "offset": 15
    },
    {
      "val": "Add task tracking system",
      "offset": 53
    }
  ],
  "location": "CreateNewProject.iSelectItemUnderDropdownNameInPopup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "https://qc.unitedsofthouse.com",
      "offset": 8
    },
    {
      "val": "Add task tracking system",
      "offset": 70
    }
  ],
  "location": "TTSSettings.iTypeURLIntoURLTextboxInPopup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "dom",
      "offset": 8
    },
    {
      "val": "DOMAIN",
      "offset": 27
    },
    {
      "val": "Add task tracking system",
      "offset": 46
    }
  ],
  "location": "CommonStepDefinition.iTypeTextIntoTextboxInPoUp(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "proj",
      "offset": 8
    },
    {
      "val": "PROJECT",
      "offset": 28
    },
    {
      "val": "Add task tracking system",
      "offset": 48
    }
  ],
  "location": "CommonStepDefinition.iTypeTextIntoTextboxInPoUp(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Add",
      "offset": 19
    },
    {
      "val": "Add task tracking system",
      "offset": 35
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "QC",
      "offset": 12
    }
  ],
  "location": "TTSSettings.iSeeThatJiraIsAddedToSettingTTS(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TTSSettings.iSeeThatButtonAddTTSIsDisabled()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 72154,
  "status": "passed"
});
formatter.before({
  "duration": 1825698035,
  "status": "passed"
});
formatter.scenario({
  "line": 144,
  "name": "Check that alredy connected TTS are not displayed in dropdown list",
  "description": "",
  "id": "tts-settings;check-that-alredy-connected-tts-are-not-displayed-in-dropdown-list",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 143,
      "name": "@REL-623"
    }
  ]
});
formatter.step({
  "line": 145,
  "name": "I am in page \"Dashboard\"",
  "keyword": "Given "
});
formatter.step({
  "line": 146,
  "name": "I check that project \"F002\" is absent in DB",
  "keyword": "And "
});
formatter.step({
  "line": 147,
  "name": "I click button \u0027Plus\u0027 in panel \u0027Projects\u0027 in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 148,
  "name": "I see pop up \"New Project\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 149,
  "name": "I type \"project_name2\" into textbox \u0027Name\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 150,
  "name": "I type \"F002\" into textbox \u0027Project key\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 151,
  "name": "I type \"test\" into textbox \u0027Description\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 152,
  "name": "I select item \"Cucumber-JVM\" in drop-down \u0027BDD-framework\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 153,
  "name": "I click on button \"Create\" on pop-up \"New Project\"",
  "keyword": "And "
});
formatter.step({
  "line": 154,
  "name": "I see success notification \"\"",
  "keyword": "Then "
});
formatter.step({
  "line": 155,
  "name": "I see that project is added to panel \u0027Projects\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 156,
  "name": "I select project in panel \u0027Projects\u0027 in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 157,
  "name": "I see that all the entered data matches data in page \u0027Project Settings\u0027 for the new project",
  "keyword": "And "
});
formatter.step({
  "line": 158,
  "name": "I click button \u0027Plus\u0027 near setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 159,
  "name": "I see pop up \"Add task tracking system\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 160,
  "name": "I select item \"Jira\" under drop-down \u0027Name\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "When "
});
formatter.step({
  "line": 161,
  "name": "I type \"https://jira.unitedsofthouse.com\" into textbox \u0027URL\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 162,
  "name": "I click on button \"Add\" on pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 163,
  "name": "I see success notification \"\"",
  "keyword": "And "
});
formatter.step({
  "line": 164,
  "name": "I see that \"Jira\" is added to setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 165,
  "name": "I click button \u0027Plus\u0027 near setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 166,
  "name": "I see pop up \"Add task tracking system\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 167,
  "name": "I see item \"Jira\" is absent under drop-down \u0027Name\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "Then "
});
formatter.step({
  "line": 168,
  "name": "I select item \"Redmine\" under drop-down \u0027Name\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "When "
});
formatter.step({
  "line": 169,
  "name": "I type \"https://redmine.unitedsofthouse.com\" into textbox \u0027URL\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 170,
  "name": "I click on button \"Add\" on pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 171,
  "name": "I see success notification \"\"",
  "keyword": "And "
});
formatter.step({
  "line": 172,
  "name": "I see that \"Redmine\" is added to setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 173,
  "name": "I click button \u0027Plus\u0027 near setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 174,
  "name": "I see pop up \"Add task tracking system\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 175,
  "name": "I see item \"Jira\" is absent under drop-down \u0027Name\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "Then "
});
formatter.step({
  "line": 176,
  "name": "I see item \"Redmine\" is absent under drop-down \u0027Name\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "Then "
});
formatter.step({
  "line": 177,
  "name": "I click on button \"Cancel\" on pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Dashboard",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.iAmInPage(String)"
});
formatter.result({
  "duration": 9191787770,
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertTrue(Assert.java:52)\r\n\tat arp.ReportService.ReportAction(ReportService.java:332)\r\n\tat stepdefinition.CommonStepDefinition.iAmInPage(CommonStepDefinition.java:262)\r\n\tat ✽.Given I am in page \"Dashboard\"(ProjectDataEditing/ProjectSettings/TTSSettings.feature:145)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "F002",
      "offset": 22
    }
  ],
  "location": "CreateNewProject.iCheckThatProjectIsAbsentInDB(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Dashboard",
      "offset": 51
    }
  ],
  "location": "CreateNewProject.iClickbuttonPlusInPanelProjects(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "New Project",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "project_name2",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxNameInPopupNewProject(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "F002",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxKeyInPopupNewProject(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "test",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxDescriptionInPopupNewProject(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Cucumber-JVM",
      "offset": 15
    }
  ],
  "location": "CreateNewProject.iSelectItemInDropdownBDDInPopupNewProject(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Create",
      "offset": 19
    },
    {
      "val": "New Project",
      "offset": 38
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreateNewProject.iSeeProjectDataIsCorrect()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreateNewProject.iSelectProjectInPanelProjects()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TTSSettings.iSeeThatEnteredDataIsCorrect()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TTSSettings.iClickButtonPlusNearTTSInPageProjectSettings()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Add task tracking system",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Jira",
      "offset": 15
    },
    {
      "val": "Add task tracking system",
      "offset": 55
    }
  ],
  "location": "CreateNewProject.iSelectItemUnderDropdownNameInPopup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "https://jira.unitedsofthouse.com",
      "offset": 8
    },
    {
      "val": "Add task tracking system",
      "offset": 72
    }
  ],
  "location": "TTSSettings.iTypeURLIntoURLTextboxInPopup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Add",
      "offset": 19
    },
    {
      "val": "Add task tracking system",
      "offset": 35
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Jira",
      "offset": 12
    }
  ],
  "location": "TTSSettings.iSeeThatJiraIsAddedToSettingTTS(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TTSSettings.iClickButtonPlusNearTTSInPageProjectSettings()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Add task tracking system",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Jira",
      "offset": 12
    },
    {
      "val": "Add task tracking system",
      "offset": 62
    }
  ],
  "location": "CreateNewProject.iSeeItemIsAbsentUnderDropdownNameInPopup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Redmine",
      "offset": 15
    },
    {
      "val": "Add task tracking system",
      "offset": 58
    }
  ],
  "location": "CreateNewProject.iSelectItemUnderDropdownNameInPopup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "https://redmine.unitedsofthouse.com",
      "offset": 8
    },
    {
      "val": "Add task tracking system",
      "offset": 75
    }
  ],
  "location": "TTSSettings.iTypeURLIntoURLTextboxInPopup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Add",
      "offset": 19
    },
    {
      "val": "Add task tracking system",
      "offset": 35
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Redmine",
      "offset": 12
    }
  ],
  "location": "TTSSettings.iSeeThatJiraIsAddedToSettingTTS(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TTSSettings.iClickButtonPlusNearTTSInPageProjectSettings()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Add task tracking system",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Jira",
      "offset": 12
    },
    {
      "val": "Add task tracking system",
      "offset": 62
    }
  ],
  "location": "CreateNewProject.iSeeItemIsAbsentUnderDropdownNameInPopup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Redmine",
      "offset": 12
    },
    {
      "val": "Add task tracking system",
      "offset": 65
    }
  ],
  "location": "CreateNewProject.iSeeItemIsAbsentUnderDropdownNameInPopup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Cancel",
      "offset": 19
    },
    {
      "val": "Add task tracking system",
      "offset": 38
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 97035,
  "status": "passed"
});
formatter.before({
  "duration": 1509404417,
  "status": "passed"
});
formatter.scenario({
  "line": 180,
  "name": "Error when updating task tracker",
  "description": "",
  "id": "tts-settings;error-when-updating-task-tracker",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 179,
      "name": "@REL-828"
    }
  ]
});
formatter.step({
  "line": 181,
  "name": "I am in page \"Dashboard\"",
  "keyword": "Given "
});
formatter.step({
  "line": 182,
  "name": "I check that project \"F002\" is absent in DB",
  "keyword": "And "
});
formatter.step({
  "line": 183,
  "name": "I check that project \"F003\" is absent in DB",
  "keyword": "And "
});
formatter.step({
  "line": 184,
  "name": "I click button \u0027Plus\u0027 in panel \u0027Projects\u0027 in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 185,
  "name": "I see pop up \"New Project\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 186,
  "name": "I type \"project_name2\" into textbox \u0027Name\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 187,
  "name": "I type \"F002\" into textbox \u0027Project key\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 188,
  "name": "I type \"test\" into textbox \u0027Description\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 189,
  "name": "I select item \"Cucumber-JVM\" in drop-down \u0027BDD-framework\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 190,
  "name": "I click on button \"Create\" on pop-up \"New Project\"",
  "keyword": "And "
});
formatter.step({
  "line": 191,
  "name": "I see success notification \"\"",
  "keyword": "Then "
});
formatter.step({
  "line": 192,
  "name": "I see that project is added to panel \u0027Projects\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 193,
  "name": "I select project in panel \u0027Projects\u0027 in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 194,
  "name": "I see that all the entered data matches data in page \u0027Project Settings\u0027 for the new project",
  "keyword": "And "
});
formatter.step({
  "line": 195,
  "name": "I click button \u0027Plus\u0027 near setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 196,
  "name": "I see pop up \"Add task tracking system\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 197,
  "name": "I select item \"Jira\" under drop-down \u0027Name\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "When "
});
formatter.step({
  "line": 198,
  "name": "I type \"https://jira.unitedsofthouse.com\" into textbox \u0027URL\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 199,
  "name": "I click on button \"Add\" on pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 200,
  "name": "I see success notification \"\"",
  "keyword": "And "
});
formatter.step({
  "line": 201,
  "name": "I see that \"Jira\" is added to setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "Then "
});
formatter.step({
  "line": 202,
  "name": "I see that entered data matches data in setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027 for Jira",
  "keyword": "And "
});
formatter.step({
  "line": 203,
  "name": "I am in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 204,
  "name": "I click button \u0027Plus\u0027 in panel \u0027Projects\u0027 in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 205,
  "name": "I see pop up \"New Project\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 206,
  "name": "I type \"project_name3\" into textbox \u0027Name\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 207,
  "name": "I type \"F003\" into textbox \u0027Project key\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 208,
  "name": "I type \"test\" into textbox \u0027Description\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 209,
  "name": "I select item \"Cucumber-JVM\" in drop-down \u0027BDD-framework\u0027 in pop-up \u0027New Project\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 210,
  "name": "I click on button \"Create\" on pop-up \"New Project\"",
  "keyword": "And "
});
formatter.step({
  "line": 211,
  "name": "I see success notification \"\"",
  "keyword": "Then "
});
formatter.step({
  "line": 212,
  "name": "I see that project is added to panel \u0027Projects\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 213,
  "name": "I select project in panel \u0027Projects\u0027 in page \"Dashboard\"",
  "keyword": "And "
});
formatter.step({
  "line": 214,
  "name": "I see that all the entered data matches data in page \u0027Project Settings\u0027 for the new project",
  "keyword": "And "
});
formatter.step({
  "line": 215,
  "name": "I click button \u0027Plus\u0027 near setting \u0027Task tracking system\u0027 in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 216,
  "name": "I see pop up \"Add task tracking system\" is opened",
  "keyword": "Then "
});
formatter.step({
  "line": 217,
  "name": "I select item \"Jira\" under drop-down \u0027Name\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "When "
});
formatter.step({
  "line": 218,
  "name": "I type \"https://jira.loldomain.com\" into textbox \u0027URL\u0027 in pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 219,
  "name": "I click on button \"Add\" on pop-up \"Add task tracking system\"",
  "keyword": "And "
});
formatter.step({
  "line": 220,
  "name": "I see success notification \"\"",
  "keyword": "Then "
});
formatter.step({
  "line": 221,
  "name": "I change \"Jira\" \u0027Task tracking system\u0027 path setting to \"https://jira.unitedsofthouse.com\" in page \u0027Project settings\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 222,
  "name": "I see error notification \"\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Dashboard",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.iAmInPage(String)"
});
formatter.result({
  "duration": 8119839794,
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertTrue(Assert.java:52)\r\n\tat arp.ReportService.ReportAction(ReportService.java:332)\r\n\tat stepdefinition.CommonStepDefinition.iAmInPage(CommonStepDefinition.java:262)\r\n\tat ✽.Given I am in page \"Dashboard\"(ProjectDataEditing/ProjectSettings/TTSSettings.feature:181)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "F002",
      "offset": 22
    }
  ],
  "location": "CreateNewProject.iCheckThatProjectIsAbsentInDB(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "F003",
      "offset": 22
    }
  ],
  "location": "CreateNewProject.iCheckThatProjectIsAbsentInDB(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Dashboard",
      "offset": 51
    }
  ],
  "location": "CreateNewProject.iClickbuttonPlusInPanelProjects(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "New Project",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "project_name2",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxNameInPopupNewProject(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "F002",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxKeyInPopupNewProject(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "test",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxDescriptionInPopupNewProject(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Cucumber-JVM",
      "offset": 15
    }
  ],
  "location": "CreateNewProject.iSelectItemInDropdownBDDInPopupNewProject(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Create",
      "offset": 19
    },
    {
      "val": "New Project",
      "offset": 38
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreateNewProject.iSeeProjectDataIsCorrect()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreateNewProject.iSelectProjectInPanelProjects()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TTSSettings.iSeeThatEnteredDataIsCorrect()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TTSSettings.iClickButtonPlusNearTTSInPageProjectSettings()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Add task tracking system",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Jira",
      "offset": 15
    },
    {
      "val": "Add task tracking system",
      "offset": 55
    }
  ],
  "location": "CreateNewProject.iSelectItemUnderDropdownNameInPopup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "https://jira.unitedsofthouse.com",
      "offset": 8
    },
    {
      "val": "Add task tracking system",
      "offset": 72
    }
  ],
  "location": "TTSSettings.iTypeURLIntoURLTextboxInPopup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Add",
      "offset": 19
    },
    {
      "val": "Add task tracking system",
      "offset": 35
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Jira",
      "offset": 12
    }
  ],
  "location": "TTSSettings.iSeeThatJiraIsAddedToSettingTTS(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TTSSettings.iSeeThatEnteredDataMatchesdataInSettingTTS()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Dashboard",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.iAmInPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Dashboard",
      "offset": 51
    }
  ],
  "location": "CreateNewProject.iClickbuttonPlusInPanelProjects(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "New Project",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "project_name3",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxNameInPopupNewProject(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "F003",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxKeyInPopupNewProject(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "test",
      "offset": 8
    }
  ],
  "location": "CreateNewProject.iTypeIntoTextboxDescriptionInPopupNewProject(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Cucumber-JVM",
      "offset": 15
    }
  ],
  "location": "CreateNewProject.iSelectItemInDropdownBDDInPopupNewProject(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Create",
      "offset": 19
    },
    {
      "val": "New Project",
      "offset": 38
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreateNewProject.iSeeProjectDataIsCorrect()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CreateNewProject.iSelectProjectInPanelProjects()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TTSSettings.iSeeThatEnteredDataIsCorrect()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "TTSSettings.iClickButtonPlusNearTTSInPageProjectSettings()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Add task tracking system",
      "offset": 14
    }
  ],
  "location": "CommonStepDefinition.popUpWindowIsOpened(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Jira",
      "offset": 15
    },
    {
      "val": "Add task tracking system",
      "offset": 55
    }
  ],
  "location": "CreateNewProject.iSelectItemUnderDropdownNameInPopup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "https://jira.loldomain.com",
      "offset": 8
    },
    {
      "val": "Add task tracking system",
      "offset": 66
    }
  ],
  "location": "TTSSettings.iTypeURLIntoURLTextboxInPopup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Add",
      "offset": 19
    },
    {
      "val": "Add task tracking system",
      "offset": 35
    }
  ],
  "location": "CommonStepDefinition.iClickButtonOnPopUp(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "CommonStepDefinition.iSeeSuccessNotification(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Jira",
      "offset": 10
    },
    {
      "val": "https://jira.unitedsofthouse.com",
      "offset": 56
    }
  ],
  "location": "TTSSettings.iChangeTTSPathValueTo(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 26
    }
  ],
  "location": "CommonStepDefinition.iSeeErrorNotification(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 92991,
  "status": "passed"
});
});